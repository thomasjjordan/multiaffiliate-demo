@Location.Selector.Type = React.createClass
  displayName: 'Location.Selector.Type'

  getDefaultProps: ->
    locations: []

  _typeSelected: (type) -> @props.typeSelected(type)

  down: -> @refs.list.down()

  left: -> @props.onBack() if @props.onBack

  select: -> @refs.list.select()

  up: -> @refs.list.up()

  render: ->
    if @props.locationsLoading
      React.createElement("div", {"className": "locs-loading"},
        React.createElement("i", {"className": "fa fa-circle-o-notch fa-spin"}), """
        Loading...
""")
    else
      types = @props.locations.map (type) =>
        onClick = @_typeSelected.bind(this, type)
        selected = type == @props.locType
        React.createElement(SelectorItem, {"key": (type.cat), "onClick": (onClick), "parent": "true",  \
                      "selected": (selected), "className": "loc-type"},
          React.createElement("div", {"className": "type-icon"},
            React.createElement("i", {"className": "fa fa-#{type.icon} fa-fw"})
          ),
          (type.label_without_popup.replace(/(<([^>]+)>)/ig, " "))
        )

      React.createElement(SelectorList, {"key": (@props.locType), "backLabel": (@props.backLabel),  \
                    "onBack": (@props.onBack), "className": "type-selector",  \
                    "head": (@props.region?.description), "ref": "list"},
        (types)
      )
