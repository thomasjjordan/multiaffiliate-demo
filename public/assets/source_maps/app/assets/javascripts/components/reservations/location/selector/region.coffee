@Location.Selector.Region = React.createClass
  displayName: 'Location.Selector.Region'

  _regionSelected: (region) -> @props.onChange(region)

  down: -> @refs.list.down()

  select: -> @refs.list.select()

  up: -> @refs.list.up()

  render: ->
    regions = @props.regions.map (region) =>
      onClick = @_regionSelected.bind(this, region)
      React.createElement(SelectorItem, {"key": (region.provider_site_id), "onClick": (onClick), "parent": "true"},
        (region.description)
      )

    React.createElement(SelectorList, {"ref": "list"},
      React.createElement("li", {"className": "hudson-subsection list-head"}, "Select a region"),
      (regions)
    )
