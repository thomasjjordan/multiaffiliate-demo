@Location.Selector.List = React.createClass
  displayName: 'Location.Selector.List'

  _locSelected: (loc) -> @props.locSelected(loc)

  down: -> @refs.list.down()

  left: -> @props.onBack() if @props.onBack

  select: -> @refs.list.select()

  up: -> @refs.list.up()

  render: ->
    locs = @props.locations.map (loc) =>
      onClick = @_locSelected.bind(this, loc)
      React.createElement(SelectorItem, {"key": (loc.Code), "onClick": (onClick)}, (loc.Name))

    React.createElement("div", {"className": "list-selector"},
      React.createElement("div", {"className": "list-label hudson-subsection"},
        (@props.locType.label_without_popup.replace(/(<([^>]+)>)/ig, " "))
      ),
      React.createElement(SelectorList, {"onBack": (@props.onBack), "ref": "list",  \
                    "paginateOnMobile": "true",  \
                    "className": "optimized"}, (locs))
    )
