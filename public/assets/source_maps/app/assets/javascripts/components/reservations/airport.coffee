@Airport = React.createClass
  displayName: 'Airport'

  getInitialState: ->
    displayPopup: false,
    displayedAirports: @props.airports,
    value: @props.current?.name

  componentDidUpdate: (prevProps, prevState) ->
    if prevProps.current != @props.current
      @setState value: @props.current?.name

  _airportSelected: (airport) ->
    @setState displayedAirports: @props.airports
    @props.onChange(airport)
    @hidePopup()

  _renderInputField: ->
    if @props.type == 'search'
      searchField = React.createElement(InputWithIcon, {"icon": "search",  \
                                   "className": "airport-search-field",  \
                                   "onChange": (@_search),  \
                                   "ref": "field",  \
                                   "placeholder": (@props.placeholder),  \
                                   "value": (@state.value),  \
                                   "tabIndex": (@props.tabIndex),  \
                                   "onKeyDown": (@handleKeyDown),  \
                                   "onFocus": (@showPopup)})

    React.createElement("div", null,
      (searchField),
      React.createElement(FakeField, {"icon": "search", "className": "airport",  \
                 "ref": "airport",  \
                 "placeholder": (@props.placeholder),  \
                 "onClick": (@showPopup),  \
                 "onFocus": (@showPopup),  \
                 "onKeyDown": (@handleKeyDown),  \
                 "tabIndex": (@props.tabIndex),  \
                 "value": (@props.current?.name)})
    )

  _renderPopup: ->
    if @state.displayedAirports.length
      airports = @state.displayedAirports.map (airport) =>
        onClick = @_airportSelected.bind(this, airport)
        React.createElement(SelectorItem, {"key": (airport.code), "onClick": (onClick)},
          (airport.name)
        )
      popupContent = React.createElement(SelectorList, {"ref": "list"},
                       (airports)
                     )
    else
      airports = []
      popupContent = React.createElement("div", {"className": "no-airports"}, """
                       No matching airports were found.
""")

    React.createElement(SelectorPopup, {"onClose": (@hidePopup)}, (popupContent))

  _search: (event) ->
    query = event.target.value
    @setState value: query

    if query.length
      results = []
      for airport in @props.airports
        queryRE = new RegExp(query.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1"), 'i')
        if airport.name.match(queryRE) || airport.code.match(queryRE)
          results.push(airport)

      @setState displayedAirports: results, displayPopup: true

    else
      @setState displayedAirports: @props.airports

  handleKeyDown: (e) ->
    keyCode = e.keyCode || e.which
    if keyCode == 9 # Tab
      @hidePopup()
    else if keyCode == 40 # Down key
      if @state.displayPopup
        @refs.list.down() if @refs.list.down
      else
        @showPopup()
    else if keyCode == 38 # Up
      @refs.list.up()
    else if keyCode == 13 # Enter
      @refs.list.select()
    else if keyCode == 27 # Esc
      @hidePopup()

  hidePopup: ->
    @setState value: @props.current?.name, displayPopup: false
    App.parentScrollToElement($(React.findDOMNode(this.refs.airport)), 50)

  showPopup: -> @setState value: null, displayPopup: true

  render: ->
    if @props.airports.length == 1 && @state.value?
      field = React.createElement("div", {"className": "static-airport"}, (@state.value))
    else
      field = @_renderInputField()

    React.createElement("div", {"className": "airport-container"},
      React.createElement("div", {"dangerouslySetInnerHTML": ({__html: @props.label})}),
      React.createElement("input", {"type": "hidden",  \
             "name": "trip[#{@props.index}][airport]",  \
             "value": (@props.current?.code)}),
      (field),
      (if @state.displayPopup then @_renderPopup())
    )
