@SelectorItem = React.createClass
  render: ->
    if @props.parent?
      forwardIcon = React.createElement("div", {"className": "forward"},
                      React.createElement("i", {"className": 'fa fa-caret-right'})
                    )
    className = "selector-item #{@props.className}"
    className += " selected hudson-primary" if @props.selected

    React.createElement("li", {"className": (className), "key": (@props.key), "onClick": (@props.onClick)},
      React.createElement("div", {"className": "value"}, (@props.children)),
      (forwardIcon)
    )
