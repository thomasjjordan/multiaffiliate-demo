@FakeField = React.createClass
  render: ->
    iconClassName = "fa fa-#{@props.icon} pull-right"

    if @props.placeholder? && !@props.value?
      placeholder = React.createElement("div", {"className": "placeholder"}, (@props.placeholder))

    value = React.createElement("div", {"className": "value"}, (@props.value)) if @props.value?

    props = {
      onClick: @props.onClick,
      tabIndex: @props.tabIndex || 0,
      onKeyDown: @props.onKeyDown,
      onFocus: @props.onFocus,
      className: "fake-field #{@props.className}"
    }
    props.disabled = @props.disabled if @props.disabled?

    React.createElement("div", React.__spread({},  props),
      (placeholder), (value),
      React.createElement("i", {"className": (iconClassName)})
    )
