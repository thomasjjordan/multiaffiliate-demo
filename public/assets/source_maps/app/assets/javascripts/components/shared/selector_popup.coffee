@SelectorPopup = React.createClass
  componentDidMount: ->
    $('body').addClass('modal-is-open')
    if window.matchMedia('screen and (max-width: 767px)').matches
      App.parentScrollToElement($('body'))

  componentWillUnmount: -> $('body').removeClass('modal-is-open')

  render: ->
    React.createElement("div", null,
      React.createElement("div", {"className": "selector-popup-back", "onClick": (@props.onClose)}),
      React.createElement("div", {"className": "selector-popup", "style": (@props.style)},
        React.createElement("div", {"className": "control-bar"},
          React.createElement("span", {"className": "close", "onClick": (@props.onClose)}, "x")
        ),
        (@props.children)
      )
    )
