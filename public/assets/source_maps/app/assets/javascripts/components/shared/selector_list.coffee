@SelectorList = React.createClass
  getInitialState: ->
    height = $(document).height()
    if height < 1200
      pageSize = 4
    else if height < 2200
      pageSize = 6
    else
      pageSize = 8
    page: 0, pageSize: pageSize

  componentDidUpdate: (prevProps, prevState) ->
    if prevProps.children != @props.children
      @setState page: 0

  _handleMouseMove: (e) ->
    @selectedChild().removeClass('hover')
    $(':hover', @_jqList()).addClass('hover')

  _handleMouseLeave: (e) ->
    $(':hover, .hover', @_jqList().children()).removeClass('hover')

  _jqList: -> $(React.findDOMNode(@refs.list))

  _pager: ->
    return if @props.children.length <= @state.pageSize
    backClass = 'selector-list-pageback'
    if @state.page == 0
      backClass += ' disabled'
    else
      goBack = => @setState page: @state.page-1
    back = React.createElement("div", {"className": (backClass), "onClick": (goBack)},
             React.createElement("i", {"className": "fa fa-arrow-left"})
           )

    totalPages = Math.ceil(@props.children.length / @state.pageSize)
    forwardClass = 'selector-list-pageforward'
    if @state.page + 1 == totalPages
      forwardClass += ' disabled'
    else
      goForward = => @setState page: @state.page+1
    forward = React.createElement("div", {"className": (forwardClass), "onClick": (goForward)},
                React.createElement("i", {"className": "fa fa-arrow-right"})
              )

    React.createElement("li", {"className": "selector-list-pagebar"},
      (back),
      React.createElement("div", {"className": "selector-list-pageinfo"}, """
        Page """, (@state.page+1), " of ", (totalPages)
      ),
      (forward)
    )

  down: ->
    $selected = @selectedChild()
    if $selected.length
      if $selected.next('.selector-item').length
        $selected.removeClass('hover')
        $selected.next('.selector-item').addClass('hover')
        @scrollList()
    else
      @_jqList().children('.selector-item').first().addClass('hover')

  scrollList: (up) ->
    $parentPopup = @_jqList().parent('.selector-popup')
    return unless @_jqList().height() > $parentPopup.height()

    if $parentPopup.length && @selectedChild().length
      elemHeight = @selectedChild().outerHeight()
      if up
        newScrollTop = ($parentPopup.scrollTop() - elemHeight)
      else
        newScrollTop = ($parentPopup.scrollTop() + elemHeight)
      $parentPopup.scrollTop newScrollTop

  select: -> @selectedChild().click()

  selectedChild: -> $('.hover', @_jqList())

  up: ->
    $selected = @selectedChild()
    if $selected.length && $selected.prev('.selector-item').length
      $selected.removeClass('hover')
      $selected.prev('.selector-item').addClass('hover')
      @scrollList(true)

  render: ->
    backLabel = if @props.backLabel? then @props.backLabel else 'Back'

    if @props.onBack? && !@props.head?
      back = React.createElement("li", {"key": "back", "className": "back", "onClick": (@props.onBack)},
               React.createElement("i", {"className": 'fa fa-caret-left'}), " ", (backLabel)
             )
    if @props.head? && !@props.onBack?
      head = React.createElement("li", {"key": "head", "className": "head hudson-subsection"}, (@props.head))

    if @props.head? && @props.onBack?
      head = React.createElement("li", {"key": "back", "className": "backHead hudson-subsection",  \
                 "onClick": (@props.onBack)},
               React.createElement("div", {"className": "title"}, (@props.head)),
               React.createElement("div", {"className": "backTitle"},
                 React.createElement("i", {"className": 'fa fa-caret-left'}), (backLabel)
               )
             )

    paginate = @props.paginateOnMobile? &&
               window.matchMedia('screen and (max-width: 767px)').matches

    if paginate
      startIndex = @state.page * @state.pageSize
      childrenToDisplay = @props.children.slice(startIndex, startIndex + @state.pageSize )
    else
      childrenToDisplay = @props.children

    React.createElement("ul", {"className": "selector-list #{@props.className || ''}",  \
        "ref": "list",  \
        "onMouseMove": (@_handleMouseMove), "onMouseLeave": (@_handleMouseLeave)},
      (head),
      (back),
      (@_pager() if paginate && childrenToDisplay.length),
      (childrenToDisplay)
    )
