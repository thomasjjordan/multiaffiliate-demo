class App
  constructor: ->
    @_noticeBoxId = '#notice_box'
    @_heightAdjust  = 0

    $('document').ready =>
      # Don't allow "enter" to submit forms.
      $(window).not('textarea').keydown (event) =>
        if event.keyCode is 13
          event.preventDefault()
          return false

      $('input[readonly]').focus -> this.blur()


      # @checkForAdblockers()
      @_initParent()
      $.ajaxSetup(
        data: { site_id: @config('site') },
        global: true,
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')) )
      $(document).ajaxComplete (event,xhr) ->
        data = $.parseJSON(xhr.responseText) if xhr.responseText?.charAt(0) is '{'
        window.location.href = data.redirect_to if (data?.redirect_to?)
      @scrollToTheTop()

      $(document).keypress (ev) =>
        if ev.which == 96 && !$(document.activeElement).is('input')
          $('body').chardinJs('toggle')

    $(window).on 'page:restore', =>
      @autoResizeParent()
      @scrollToTheTop()
    $(window).resize @autoResizeParent
    $(document).on 'page:load', =>
      @autoResizeParent()
      @scrollToTheTop()
    $(document).ready =>
      @autoResizeParent()
      @scrollToTheTop()

  _initParent: ->
    if @parentDocument()?.getElementById('loading')
      @parentDocument()?.getElementById('loading').style.visibility = 'hidden'

  autoResizeParent: =>
    return unless @myFrame()
    height = @myHeight() + @_heightAdjust
    @myFrame().style.height = "#{height}px"

  checkForAdblockers: =>
    setTimeout (=>
      if _gaq?.I? then blocked = false else blocked = true
      $.get @url('block_check'), { blocked }
    ), 2000

  config: (key) -> $('body').data(key)

  devMode: -> @config("env") is 'development'

  doneLoading: (options = {}) =>
    @hideLoader(options.loader) if options.loader
    $(options.button).button('reset') if button = $(options.button)

  ensureSSL: ->
    if !@devMode() && window.location.protocol != "https:"
      window.location.href = "https:" + window.location.href.substring(window.location.protocol.length)

  hideLoader: (selector) => $(selector).siblings().remove('.loader')

  hideNoticeBox: => $(@_noticeBoxId).fadeOut()

  isIE: => !!navigator.userAgent.match(/Trident/)

  isIE11CompatMode: => @isIE() && document.documentMode == 7

  myHeight: ->
    padding = $('body').data('height-padding')
    padding = 100 if padding == '' || padding == null
    $('body').innerHeight() + padding

  myFrame: -> window.frameElement

  parent: ->
    try
      return parent if parent.document
    catch
      null

  parentDocument: -> try parent.document catch e

  parentFrameId: ->
    try
      return window.frameElement?.getAttribute('frame_id')
    catch
      null

  parentGoto: (url) -> @parent()?.goto url

  parentGotoInNewWindow: (url) -> @parent()?.gotoInNewWindow "/#{url}"

  parentScrollToTheBottom: ->
    iframe = @parentDocument()?.getElementById( @parentFrameId() )
    return unless iframe

    spaceAboveIframe = $(iframe).offset().top
    iframeHeight = $(iframe).height()
    iframeBottomPos = spaceAboveIframe + iframeHeight
    viewportHeight = parent.window.innerHeight
    pointToScrollTo = iframeBottomPos - viewportHeight
    parent.window.scrollTo(0, pointToScrollTo)

  parentScrollToElement: ($elem, topOffset = 0) ->
    iframe = @parentDocument()?.getElementById( @parentFrameId() )
    return unless iframe

    spaceAboveIframe = $(iframe).offset().top
    elemTop = $elem.offset().top
    pointToScrollTo = spaceAboveIframe + elemTop - topOffset
    parent.window.scrollTo(0, pointToScrollTo)

  showModal: (message) ->
    modal = "<div id='modal' class='modal fade'><div class='modal-dialog'><div class='modal-content'>" +
      "<div class='modal-body'><p>#{message}</p></div>" +
      "</div></div></div>"
    $('body').append(modal)
    $('#modal').modal('show')

  scrollTo: (vertical_pos) ->
    $("html, body").animate scrollTop: vertical_pos, 1000

  scrollToTheBottom: ->
    $("html, body").animate scrollTop: $(document).height(), 1000

  scrollToTheTop: ->
    $("html, body").animate scrollTop: 0
    if @parentDocument()?
      iframeElem = @parentDocument()?.getElementById(@parentFrameId())
      if iframeElem
        rect = iframeElem.getBoundingClientRect()
        @parent()?.scrollBy(0, rect.top)

  # An adjustment that will be applied to any auto-reizing functions.
  setHeightAdjustment: (adjustment) -> @_heightAdjust = adjustment

  showError: (error) =>
    $(@_noticeBoxId)
      .removeAttr('class').addClass('alert alert-danger')
      .html(error)
      .fadeIn()

  showLoader: (element, options) =>
    className = "loader"
    className += " centered" if options['centered']
    loaderElem = $("<span class='#{className}'/>")
    placementMethod = options['placement'] || 'after'
    element[placementMethod](loaderElem)

  # Supported options:
  # hideErrors -> Whether to hide the page's notice-box.
  # button -> Accepts a selector (ex. #load-services'). The associated button(s)
  #     will have their text changed to their data-loading attribute or "Loading..."
  # loader -> Accepts a selector. Equivalent to calling showLoader.
  startLoading: (options = {}) =>
    $.extend(options, hideErrors: true) # Defaults
    @hideNoticeBox() if options.hideErrors
    @showLoader(options.loader) if options.loader
    $(options.button).button('loading') if options.button

  supportsLocalStorage: ->
      try
        return window['localStorage']?
      catch
        return false

  url: (urlName, paramString) ->
    url = @config('urls')[urlName] || throw("No URL found for #{urlName}")
    if paramString
      delimiter = if (url.indexOf('?') >= 0) then '&' else '?'
      return "#{url}#{delimiter}#{paramString}"
    url

root = exports ? this
root.App = new App()
