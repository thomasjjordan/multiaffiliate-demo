class CruiseResPage extends ResPageClass
  constructor: (@app) ->
    cruiseReady = =>
      @$bookingContainer = $('#booking-container')
      @$cruiseSite = $('#cruise_site_id')

      if $('div[data-type="cruise"]').length > 0
        @_initSiteSelector()

    $(document).ready cruiseReady
    $(document).on 'page:load', cruiseReady
    $(window).on 'page:restore', cruiseReady

  _defaultLeg0Location: =>
    initialLoc = @$leg0Location.data('initial-val')
    return unless initialLoc
    @$leg0Location.val(initialLoc)
    @$leg0Location.removeData('initial-val')
    @leg0LocationChanged()

  _defaultLeg1Hotel: =>
    initialLoc = @$leg1Location.data('initial-val')
    return unless initialLoc
    @$leg1Location.val(initialLoc)
    @$leg1Location.removeData('initial-val')

  _defaultLeg2Location: =>
    initialLoc = @$leg2Location.data('initial-val')
    return unless initialLoc
    @$leg2Location.val(initialLoc)
    @$leg2Location.removeData('initial-val')
    @leg2LocationChanged()

  _initCategoryAutoCompleteWidget: =>
    $.widget( "custom.locCatComplete", $.ui.autocomplete, {
        _renderMenu: ((ul, locList) ->
          if not locList[0].locations?
            ul.append( "<li class='ui-autocomplete-category'>" + locList[0].label + "</li>")
            this._renderItemData(ul, { label: '', value: '' })
          else
            currentCategory = ""
            for cat in locList
              ul.append( "<li class='ui-autocomplete-category'>" + cat.label + "</li>" )
              for loc in cat.locations
                this._renderItemData(ul, {
                    label: loc.Name, value: loc.Code, address: loc.Address,
                    state: loc.State, city: loc.City, catInfo: cat.catInfo }) ),
        _renderItem: (ul, loc) ->
          if not loc.label
            displayStr = "<a style='display:none;'></a>"
          else
            displayStr = "<a class='needsclick'>#{loc.label}"
            displayStr += "<br><span class='loc-address'>#{loc.address}</span>" if loc.address
            if loc.city || loc.state
              displayStr += "<br><span class='loc-address'>#{loc.city}, #{loc.state}</span></a>"
          $("<li>")
            .append(displayStr)
            .appendTo(ul) })

  _initBookingContainer: =>
    @_initSubmit()
    @_initCategoryAutoCompleteWidget()
    @_initDateTimePickers()
    @_initPassengerPanels()
    @_initServiceExtraPanels()

    @_setElementVars()
    @_initLeg0()
    @_initLeg1()
    @_initLeg2()
    @_initLeg3()
    @toggleLeg2()
    @toggleLeg3()
    @_loadServiceExtras()
    @app.autoResizeParent()

  _initLeg0: =>
    @$leg0Location.change @leg0LocationChanged
    @$leg0Airport.change @leg0AirportChanged
    @$leg0Disable.change =>
      @toggleLeg0Fields()
      @toggleLeg1LocationField()
      @_loadLeg1FieldsWhenLeg0Disabled()
    @toggleLeg0Fields()
    @_loadLeg1FieldsWhenLeg0Disabled()

    # Handle existing data.
    @leg0AirportChanged() if @$leg0Airport.val()

  _initLeg1: =>
    @$leg1Airport.change @leg1AirportChanged
    @$leg1Disable.change @toggleLeg1Fields
    @toggleLeg1()
    @toggleLeg1Fields()

  _initLeg2: =>
    @$leg2Airport.change @leg2AirportChanged
    @$leg2Location.change @leg2LocationChanged
    @$leg2Disable.change @toggleLeg2Fields
    @leg2AirportChanged()
    @toggleLeg2Fields()

  _initLeg3: =>
    @$leg3Disable.change @toggleLeg3Fields
    @toggleLeg3Fields()

  _initSiteSelector: =>
    @$cruiseSite.change @cruiseSiteChanged
    @cruiseSiteChanged(noAnimation: true)

  _loadLeg1FieldsWhenLeg0Disabled: =>
    return unless @leg0Disabled()

    # If leg 0 is disabled set the airport to the first airport.
    airport = $('#trip_0_airport option').eq(1).val()
    @loadLeg1Hotels(airport)

  _loadBookingContainer: (options) =>
    options ||= { noAnimation: false }

    @$bookingContainer.hide() if @$bookingContainer.is(':visible')

    $loader = $("<div class='cruise-loader'><span class='loader'></span> Loading...</div>")
    @$bookingContainer.before($loader)

    $.get @app.url('cruise_booking_container'),
        { cruise_site_id: @$cruiseSite.val() },
        (data) =>
          $('.cruise-loader').velocity('fadeOut', { duration: 200 })
          @$bookingContainer
            .hide()
            .html(data.html)

          if options.noAnimation
            @$bookingContainer.show().css('opacity', 1)
            @_initBookingContainer()
          else
            @$bookingContainer.velocity('fadeIn',
                { duration: 200, complete: @_initBookingContainer })


  _loadServiceExtras:  =>
    siteId = @$cruiseSite.val()
    @clearServiceExtras(0)
    @app.showLoader($("#svc-extras-form-0 .panel-body"),
        placement: 'append', centered: true)

    $.get @app.url('svc_extras'),
        { leg: 0, provider_site: siteId },
        (data) =>
          if data.error
            alert data.error
          else
            $("#svc-extras-form-0 .panel-body").html(data.extras_html)
            $("#svc-extras-form-0 label").popover()

  _setElementVars: =>
    @$leg0Location = $('#trip_0_location')
    @$leg0Airport = $('#trip_0_airport')
    @$leg0Disable = $('#trip_disable_leg0')
    @$leg1Airport = $('#trip_1_airport')
    @$leg1Location = $('#trip_1_location')
    @$leg1Disable = $('#trip_disable_leg1')
    @$leg2Airport = $('#trip_2_airport')
    @$leg2Disable = $('#trip_disable_leg2')
    @$leg2Location = $('#trip_2_location')
    @$leg3Airport = $('#trip_3_airport')
    @$leg3Disable = $('#trip_disable_leg3')

  cruiseSiteChanged: (options) =>
    options ||= { noAnimation: false }

    if @$cruiseSite.val()
      @_loadBookingContainer(options)
    else
      if options.noAnimation
        @$bookingContainer.hide()
      else
        @$bookingContainer.velocity('fadeOut', { duration: 200 })

  leg0AirportChanged: =>
    airport = @$leg0Airport.val()
    @loadLeg0Destinations(airport)
    @$leg3Airport.val(airport)
    @toggleLeg1()

  leg0Disabled: => @$leg0Disable.is(':checked')

  leg0LocationChanged: =>
    if @leg0LocationIsPort()
      @$leg2Airport.val(@$leg0Location.val())
      @leg2AirportChanged()

    $('#trip_0_DropOffType').val(@leg0LocationType())

    @toggleLeg1()
    @toggleLeg2()
    @toggleLeg3()

  leg0LocationType: => $('#trip_0_location :selected').data('type')

  leg0LocationIsPort: => @leg0LocationType() == 'Y'

  leg0LocationIsHotel: => @leg0LocationType() && @leg0LocationType() != 'Y'

  leg1AirportChanged: =>
    @$leg2Airport.val(@$leg1Airport.val())
    @leg2AirportChanged()
    @toggleLeg2()

  leg1Disabled: => @$leg1Disable.is(':checked')

  leg2AirportChanged: =>
    airport = @$leg2Airport.val()
    @loadDepartingDestinations(airport)
    @leg2LocationChanged()

  leg2Disabled: => @$leg2Disable.is(':checked')

  leg2LocationChanged: =>
    $('#trip_2_DropOffType').val(@leg2LocationType())
    @toggleLeg2FlightFields()
    @toggleLeg3()

  leg2LocationType: => $('#trip_2_location :selected').data('type')

  leg2LocationIsAirport: => @leg2LocationType() == 'A'

  leg2LocationIsHotel: => @leg2LocationType() && @leg2LocationType() != 'A'

  leg3Disabled: => @$leg3Disable.is(':checked')

  loadLeg0Destinations: (airport) =>
    @$leg0Location.empty().html("<option>Loading...</option>")
    $.get @app.url('cruise_locations'), { airport },
      (data) =>
        if data.error
          alert data.error
        else
          options = '<option></option>'
          @$leg0Location.empty()
          for category, locations of data
            options += "<optgroup label='#{category}'>"
            for location in locations
              options += "<option data-type='#{location.FareType}' " +
                  "value='#{location.Code}'>#{location.Name}</option>"
            options += "</optgroup>"
          @$leg0Location.html(options)
          @_defaultLeg0Location()

  loadLeg1Hotels: (airport) =>
    @$leg1Location.empty().html("<option>Loading...</option>")
    $.get @app.url('cruise_arr_hotel_hotels'), { airport },
      (data) =>
        if data.error
          alert data.error
        else
          options = '<option></option>'
          @$leg1Location.empty()
          for category, locations of data
            options += "<optgroup label='#{category}'>"
            for location in locations
              options += "<option data-type='#{location.FareType}' " +
                  "value='#{location.Code}'>#{location.Name}</option>"
            options += "</optgroup>"
          @$leg1Location.html(options)
          @_defaultLeg1Hotel()

  loadDepartingDestinations: (airport) =>
    @$leg2Location.empty().html("<option>Loading...</option>")
    $.get @app.url('cruise_departing_locations'), { airport },
      (data) =>
        if data.error
          alert data.error
        else
          options = '<option></option>'
          @$leg2Location.empty()
          for category, locations of data
            options += "<optgroup label='#{category}'>"
            for location in locations
              options += "<option data-type='#{location.FareType}' " +
                  "value='#{location.Code}'>#{location.Name}</option>"
            options += "</optgroup>"
          @$leg2Location.html(options)
          @_defaultLeg2Location()

  toggleLeg0Fields: =>
    if @leg0Disabled()
      $('.leg0 .row:not(:first-child), .leg0 p').velocity('fadeOut', {
          duration: 200, complete: @app.autoResizeParent })
    else
      $('.leg0 .row:not(:first-child), .leg0 p').velocity('fadeIn', {
          duration: 200, complete: @app.autoResizeParent })
    @toggleLeg1()

  toggleLeg1: =>
    $leg1 = $('.leg1')
    if @leg0LocationIsHotel() || @leg0Disabled()
      if $leg1.is(":hidden")
        $leg1.velocity('slideDown', {
            duration: 200, complete: @app.autoResizeParent })
    else if $leg1.is(":visible")
      $leg1.velocity('slideUp', {
          duration: 200, complete: @app.autoResizeParent })

  toggleLeg1Fields: =>
    if @leg1Disabled()
      $('.leg1 .row:not(:first-child), .leg1 p').velocity('fadeOut', {
          duration: 200, complete: @app.autoResizeParent })
    else
      $('.leg1 .row:not(:first-child), .leg1 p').velocity('fadeIn', {
          duration: 200, complete: @app.autoResizeParent })
    @toggleLeg1LocationField()
    @toggleLeg2()
    @app.autoResizeParent()

  toggleLeg1LocationField: =>
    $leg1 = $('.leg1')
    if @leg0Disabled()
      $leg1.find('.location-container').show()
    else
      $leg1.find('.location-container').hide()
    @app.autoResizeParent()

  toggleLeg2Fields: =>
    if @leg2Disabled()
      $('.leg2 .row:not(:first-child), .leg2 p').velocity('fadeOut', {
          duration: 200, complete: @app.autoResizeParent })
    else
      $('.leg2 .row:not(:first-child), .leg2 p').velocity('fadeIn', {
          duration: 200, complete: @app.autoResizeParent })
    @toggleLeg3()

  toggleLeg2FlightFields: =>
    if @leg2LocationIsAirport()
      $('.leg2 .flight-fields').velocity('fadeIn', {
          duration: 200, complete: @app.autoResizeParent })
    else
      $('.leg2 .flight-fields').velocity('fadeOut', {
          duration: 200, complete: @app.autoResizeParent })

  toggleLeg2: =>
    $leg2 = $('.leg2')
    leg1AirportSelected = @$leg1Airport.val()
    if (!@leg0Disabled() && @leg0LocationIsPort()) ||
        leg1AirportSelected || @leg1Disabled()
      unless $leg2.is(":visible")
        $leg2.velocity('slideDown', {
            duration: 200, complete: @app.autoResizeParent })
    else if $leg2.is(":visible")
      $leg2.velocity('slideUp', {
          duration: 200, complete: @app.autoResizeParent })

  toggleLeg3: =>
    $leg3 = $('.leg3')
    returnDisabled = @$leg2Disable.is(':checked')

    if !returnDisabled && @leg2LocationIsHotel()
      if $leg3.is(":hidden")
        $leg3.velocity('slideDown', {
            duration: 200, complete: @app.autoResizeParent })
    else if $leg3.is(":visible")
      $leg3.velocity('slideUp', {
          duration: 200, complete: @app.autoResizeParent })

  toggleLeg3Fields: =>
    if @leg3Disabled()
      $('.leg3 .row:not(:first-child), .leg3 p').velocity('fadeOut', {
          duration: 200, complete: @app.autoResizeParent })
    else
      $('.leg3 .row:not(:first-child), .leg3 p').velocity('fadeIn', {
          duration: 200, complete: @app.autoResizeParent })


root = exports ? this
root.CruiseResPage = new CruiseResPage(root.App)
