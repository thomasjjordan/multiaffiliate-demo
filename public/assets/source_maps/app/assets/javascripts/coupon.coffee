
class Coupon
  constructor: (@app) ->
    ready = =>
      @_initCouponForm()

    $(document).ready(ready)
    $(document).on('page:load', ready)

  _initCouponForm: =>
    $('.apply-coupon-form button.apply-coupon').click (e) =>
      e.preventDefault()
      $button = $(e.currentTarget)
      $button.button('loading')

      @applyCoupon( $('input[name=coupon]').val() ).always (data) ->
        $button.button('reset') unless data.redirect_to

    $('.remove-coupon').click (e) =>
      e.preventDefault()
      $button = $(e.currentTarget)
      $button.button('loading')
      @removeCoupon().always (data) ->
        $button.button('reset') unless data.redirect_to

  _issueText: (issue) =>
    text = ''
    if issue.leg.attraction
      text = "Your <strong>#{issue.leg.dropoff}</strong> on " +
             "<strong>#{issue.leg.pickup_date}</strong> "
    else
      text = "Your reservation on <strong>#{issue.leg.pickup_date}</strong> " +
             "(<strong>#{issue.leg.pickup}</strong> -> " +
             "<strong>#{issue.leg.dropoff}</strong>) "

    if issue.status.hasOwnProperty('fare_increase')
      text += "will cost an additional " +
              "<strong>$#{issue.status.fare_increase.toFixed(2)}</strong>."
    else if issue.status.hasOwnProperty("service_no_longer_available")
      text += "will no longer be available and will be " +
              "<strong>removed from your cart</strong>."
    else if issue.status.hasOwnProperty('fare_decrease')
      text += "will cost less by " +
              "<strong>$#{issue.status.fare_decrease.toFixed(2)}</strong>."

    text

  applyCoupon: (coupon, approved) =>
    deferred = $.Deferred()
    $issueList = $('#coupon-issue-modal .issues')

    $.post @app.url('apply_coupon'), { coupon, approved }, (data) =>
      alert(data.error) if data.error
      if data.issues
        $issueList.empty()
        $('#coupon-issue-modal .action')
          .off()
          .click => @applyCoupon(coupon, true)
        $('#coupon-issue-modal .description').html("When this coupon is applied:")
        for issue in data.issues
          $issueList.append "<li>#{@_issueText(issue)}</li>"
        $('#coupon-issue-modal').modal('show')
      deferred.resolve(data)

    deferred.promise()

  removeCoupon: (approved) =>
    deferred = $.Deferred()
    $issueList = $('#coupon-issue-modal .issues')

    $.post @app.url('remove_coupon'), { approved }, (data) =>
      alert(data.error) if data.error
      if data.issues
        $issueList.empty()
        $('#coupon-issue-modal .action')
          .off()
          .click => @removeCoupon(true)
        $('#coupon-issue-modal .description').html("When this coupon is removed:")
        for issue in data.issues
          $issueList.append "<li>#{@_issueText(issue)}</li>"
        $('#coupon-issue-modal').modal('show')
      deferred.resolve(data)

    deferred.promise()



root = exports ? this
root.Coupon = new Coupon(root.App)
