
class Services
  constructor: (@app) ->
    ready = =>
      if $('body[data-controller="services"]').length > 0
        @initLeg(undefined, noAnimations: true)
        @app.autoResizeParent()
        # FastClick support.
        $('button').each -> new FastClick(this)
    $(document).ready(ready)
    $(window).load => @app.autoResizeParent()
    $(document).on('page:load', ready)

  _addAdditionalChargeWarnings: (leg, time) =>
    $servicesTable = $('.services-table', @_legContext(leg))
    $timeList = $('.pickup-time select', @_legContext(leg))
    offPeak = parseFloat(time.components.OffPeakCharge)
    shortNotice = parseFloat(time.components.ShortNoticeCharge)
    $('.time-warn', @_legContext(leg)).remove()
    if offPeak != 0
      warning = $servicesTable.data('offpeak-label')
      $timeList.after("<span class='time-warn time-adtl-charge label label-warning'>#{warning}: #{time.components.OffPeakCharge}</span>")
    if shortNotice != 0
      warning = $servicesTable.data('shortnotice-label')
      $timeList.after("<span class='time-warn time-adtl-charge label label-warning'>#{warning}: #{time.components.ShortNoticeCharge}</span>")
    if offPeak != 0 || shortNotice != 0
      warning = $servicesTable.data('adtlcharges-label')
      $timeList.after("<span class='time-warn label label-warning'>#{warning}</span>")

  _calculateTip: (baseFare, tipPercent) ->
      tip = Math.max(0, (parseFloat(tipPercent) / 100) * baseFare)
      tip = 0 if isNaN(tip)
      tip.toFixed(2)

  _configureGratuity: (leg, service) =>
    includedMessageElem = $('.gratuity-included-message', @_legContext(leg))
    return unless includedMessageElem.length

    includedGratuity = service.data('included-gratuity')
    $('.gratuity-included, .gratuity-not-included', @_legContext(leg)).hide()

    if parseFloat(includedGratuity.percentage) is 0
      $('.gratuity-not-included', @_legContext(leg)).show()
    else
      $('.gratuity-included', @_legContext(leg)).show()

      # Display "gratuity included" message.
      message = includedMessageElem.data('message')
        .replace('%GRAT_PERCENT%', includedGratuity.percentage+'%')
        .replace('%GRAT_AMOUNT%', includedGratuity.amount)
      includedMessageElem.html(message)

  _initAirlinesAndShips: (leg) =>
    $('.airline', @_legContext(leg)).chosen(width: '200px')
    $('.ship', @_legContext(leg)).chosen(width: '200px')

  _initDetailsButtons: (leg) =>
      $('.show-details', @_legContext(leg)).button().click => @showDetails(leg, noAnimations: true)

  _initFlightCityList: (leg) =>
    cityUrl = @app.url('flight_cities', "leg_index=#{leg}")

    $('.flight-city', @_legContext(leg)).autocomplete(
        open: (=> $('.ui-corner-all', @_legContext(leg)).addClass('needsclick') ),
        autoFocus: true, delay: 200, minLength: 1,
        source: cityUrl)
      .on('autocompleteselect', (event, ui) ->
          id = ui.item.id
          value = ui.item.value
          $(this).val(value)
          return false )

  _initPage: (leg, options) =>
    @setService(leg,  $('.service-type', @_legContext(leg)).val(), options)

  _initServiceButtons: (leg) =>
      $('.service-button .btn', @_legContext(leg)).button().click (ev) =>
          @setService(leg, $(ev.delegateTarget).data('type'))
      $('.show-all-services', @_legContext(leg))
        .button()
        .click => @showAllServices(leg)

  _initTipCalculator: (leg) =>
      $('.tip-calculator', @_legContext(leg))
        .popover()
        .on 'shown.bs.popover', =>
            $('.tip-percent', @_legContext(leg)).keyup (ev) =>
                @_updateCalculator(leg)
                ev.preventDefault()
            $('.common-tip', @_legContext(leg)).click (ev) =>
                tipPercent = $(ev.delegateTarget).data('tip')
                $('.tip-percent', @_legContext(leg)).val(tipPercent)
                @_updateCalculator(leg)
                ev.preventDefault()
            $('.update-tip', @_legContext(leg))
              .click (ev) =>
                $('.tip-calculator', @_legContext(leg)).popover('hide')

  _initSubmit: (leg) =>
    $leg = @_leg(leg)
    $submitButton = $leg.find('.submit')
    $backLink = $leg.find('.buttons a')
    $divider = $leg.find('.buttons .divider')
    $form = $leg.find('form')

    # Update submit button's label based on edit state.
    if $form.data('edit')
      $submitButton.html( $submitButton.data('edit-text') )
      $backLink.hide()
      $divider.hide()
    else
      $submitButton.html( $submitButton.data('new-text') )
      $backLink.show()
      $divider.show()

    $form.submit (e) =>
      e.preventDefault()
      $('.submit', $(e.currentTarget)).button('loading')

      formData = $("#form-#{leg}").serialize()
      if $form.data('edit') then formData += "&edit=true"
      $.post @app.url('svc_update'), formData,
        (data) =>
          if data.html
            # Update current leg
            $leg.replaceWith(data.previousHtml)
            $leg = @_leg(leg)
            $leg.find('form').data('edit', 1)

            @initLeg(leg, noAnimations: true)
            @hideDetails(leg)
              .done =>
                # If this leg was the last leg on the page then put the
                # next leg up. Otherwise scooch down to the last leg.
                lastLegIndex = $('.leg-services-section').length - 1
                if leg == lastLegIndex
                  $leg.after(data.html)
                  @initLeg(leg + 1, noAnimations: true)
                  @app.autoResizeParent()
                  elemToScrollTo = $("#leg#{leg+1}")
                  @app.parentScrollToElement(elemToScrollTo, -50)
                else
                  @app.autoResizeParent()
                  elemToScrollTo = $("#leg#{lastLegIndex}")
                  @app.parentScrollToElement(elemToScrollTo, -50)

          else if data.errorHtml
            $leg.replaceWith(data.errorHtml)
            $leg = @_leg(leg)

            # A leg can be considered in "edit" mode if
            # it has been successfully saved at least once.
            lastLegIndex = $('.leg-services-section').length - 1
            $leg.find('form').data('edit', 1) if leg != lastLegIndex

            @initLeg(leg, noAnimations: true)
            @showDetails(leg, noAnimations: true)




  _initTimeSelector: (leg) =>
    $('.pickup-tod', @_legContext(leg))
      .change (ev) =>
        serviceType = $(".service-type", @_legContext(leg)).val()
        times = $(".service-#{serviceType}", @_legContext(leg)).data('times')
        time = (t for t in times when t.time is $(ev.delegateTarget).val())[0]
        @_addAdditionalChargeWarnings(leg, time)

  # Creates a jQuery object around @_legContext to use with .find
  _leg: (index) => $(@_legContext(index))

  _legContext: (index) =>
    if index
      "#leg#{index}"
    else
      defaultIndex = $('.leg-services-section').first().data('index')
      "#leg#{defaultIndex}"

  _loadTimes: (leg, times) =>
    $selectList = $('.pickup-tod', @_legContext(leg))
    pickupStr = $('.services-table', @_legContext(leg)).data('pickup-prefix')
    dropoffStr = $('.services-table', @_legContext(leg)).data('dropoff-prefix')
    selectedTime = $selectList.data('selected')?.replace(/"/g,'')

    $('option', $selectList).remove()
    for time in times
      suggestedTime = time if time.suggested
      timeFormat = $(".pickup-tod").data("time-format")
      pickupTime = $.format.date(time.time, timeFormat)
      dropoffTime = $.format.date(time.dropoff_time, timeFormat)
      timeStr = "#{pickupTime} (#{dropoffStr} #{dropoffTime})"
      $option = $("<option value='#{time.time}'>#{timeStr}</option>")
      if selectedTime == time.time then $option.attr('selected', 'selected')
      $selectList.append($option)
    @_addAdditionalChargeWarnings(leg, suggestedTime)

  _updateCalculator: (leg) =>
    includedGrat = @currentService(leg).data('included-gratuity')
    baseFare = includedGrat.fare_without_gratuity.replace(/[^\d.]/g, '')
    tip = @_calculateTip(baseFare, $('.tip-percent', @_legContext(leg)).val())
    $('.tip', @_legContext(leg)).html(tip)
    $('.user-gratuity', @_legContext(leg)).val(tip)

  _updatePickupExtras: (leg, extras, options) =>
    options ||= { noAnimations: false }
    selectList = $('.extras', @_legContext(leg))
    if extras.length > 0
      if options.noAnimations
        $('.pickup-extras', @_legContext(leg)).show()
      else
        $('.pickup-extras', @_legContext(leg))
          .velocity('fadeIn', { duration: 200 })

      $('option', selectList).remove()
      for extra in extras
        label = "#{extra.name}: $#{extra.charge}"
        selectList.append("<option value='#{extra.type}'>#{label}</option>")
    else
      if options.noAnimations
        $('.pickup-extras').hide()
      else
        $('.pickup-extras').velocity('fadeOut', { duration: 200 })


  currentService: (leg) =>
    $('.services-table .list-group-item:visible', @_legContext(leg))

  disableSubmit: (leg) => $('.submit', @_legContext(leg)).attr('disabled', true)

  hideButtons: (leg) => $('.buttons', @_legContext(leg)).hide()

  hideDetails: (leg) =>
    $leg = @_leg(leg)
    deferred = $.Deferred()

    # Update details summary
    pickupInfo = $leg.find('.pickup-tod option:selected').text()
    $leg.find('.summary-times').html(pickupInfo)

    $leg.find('.buttons, .show-all-services').hide()
    $leg.find('.details-all')
      .velocity('slideUp', { duration: 200, complete: =>
        $leg.find('.details-summary')
          .velocity('slideDown', { duration: 200, complete: =>
            App.autoResizeParent()
            deferred.resolve()
        })
    })

    deferred.promise()

  initLeg: (leg, options) =>
    leg ||= $('.leg-services-section').first().data('index')
    @hideButtons(leg)
    @_initServiceButtons(leg)
    @_initDetailsButtons(leg)
    @_initTimeSelector(leg)
    @_initFlightCityList(leg)
    @_initAirlinesAndShips(leg)
    @_initTipCalculator(leg)
    @_initSubmit(leg)
    @_initPage(leg, noAnimations: options?.noAnimations)

  setService: (leg, serviceType, options) =>
    $leg = @_leg(leg)
    options ||= { noAnimations: false }
    thisService = $leg.find(".service-#{serviceType}")
    return unless thisService.length

    # Hide/Show various parts of the page. Ordering of operations below has
    # been optimized to reduce layout/repainting and improve feel
    # whenever possible.

    # Hide all legs but this one and ones before this one.
    $("#leg#{leg}").nextAll('.leg-services-section').css('visibility', 'hidden')

    $leg.find('.services-table li')
      .not("[data-type='#{serviceType}']")
      .hide()
    $leg.find('.services-table .service-button').hide()
    $('.service-fare', thisService).addClass('selected')

    $leg.find(".services-table li[data-type='#{serviceType}']")
      .addClass('selected-service')
    $leg.find('.show-all-services').show().css('opacity', '1')

    if options.noAnimations
      $leg.find('.service-header-content, h1').hide()
      $leg.find('.details').show()
      # Re-show legs
      $("#leg#{leg}").nextAll('.leg-services-section').css('visibility', 'visible')
      @showButtons(leg)
      App.autoResizeParent()
    else
      $leg.find('.service-header-content').velocity('slideUp', { duration: 200 })
      $leg.find('h1').velocity('slideUp', {
          duration: 200,
          complete: =>
            $leg.find('.details').velocity('fadeIn', {
                duration: 200,
                complete: =>
                  # Re-show legs
                  $("#leg#{leg}").nextAll('.leg-services-section').css('visibility', 'visible')

                  @showButtons(leg)
                  $leg.find('.details-all').velocity("scroll")
                  App.autoResizeParent()
            })
      })

    $leg.find('.service-type').val(serviceType)
    @_configureGratuity(leg, thisService)
    @_loadTimes(leg, thisService.data('times'))
    @_updatePickupExtras(leg, thisService.data('pickup-extras'), options)

  showAllServices: (leg) =>
    @hideButtons(leg)

    # Hide all legs but this one and ones before it.
    $("#leg#{leg}").nextAll('.leg-services-section').css('visibility', 'hidden')

    $('.show-all-services', @_legContext(leg)).velocity('fadeOut', {
        duration: 200,
        complete: =>
          # Ordered this way to optimize animations, reduce layout/repainting
          # when possible.
          $('.details', @_legContext(leg)).hide()
          $('.show-all-services', @_legContext(leg)).hide()

          $('.service-fare.selected', @_legContext(leg))
            .removeClass('selected')
          $('.services-table li.selected-service', @_legContext(leg))
            .removeClass('selected-service')

          $('.services-table li, .service-button', @_legContext(leg)).show()
          $('h1, .service-header-content', @_legContext(leg))
            .velocity('slideDown', { duration: 200 })

          # Re-show legs
          $("#leg#{leg}").nextAll('.leg-services-section').css('visibility', 'visible')

          $('.service-type', @_legContext(leg)).val('')
          $('form', @_legContext(leg)).velocity("scroll")
          App.autoResizeParent() })

  showButtons: (leg) => $('.buttons', @_legContext(leg)).show()

  showDetails: (leg, options) =>
    options ||= { noAnimations: false }
    $leg = @_leg(leg)

    if options.noAnimations
      $leg.find('.details-all, .buttons, .show-all-services').show()
      $leg.find('.details-summary').hide()
      App.autoResizeParent()
    else
      $leg.find('.details-all, .buttons, .show-all-services')
        .velocity('slideDown', complete:
          $leg.find('.details-summary').velocity('slideUp', complete:
            App.autoResizeParent()
          )
        )

root = exports ? this
root.Services = new Services(root.App)
