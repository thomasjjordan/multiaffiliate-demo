
class Receipt
  constructor: (@app) ->
    ready = =>
      if $('body[data-action="receipt"]').length > 0
        @_initPreferredRider()
        FastClick.attach(document.body)
    $(document).ready(ready)
    $(document).on('page:load', ready)


  _initPreferredRider: ->
    $('#join-preferred-rider').click =>
        @app.parentGotoInNewWindow 'join_preferred_rider'

root = exports ? this
root.Receipt = new Receipt(root.App)
