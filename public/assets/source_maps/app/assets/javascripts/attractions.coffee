
class Attractions
  constructor: (@app) ->
    ready = =>
      if $('body[data-controller="attractions"]').length > 0
        @_initFields()
        @_initPriceCheck()
        @_initTimesButtons()
        @_initTimeSelector()
        FastClick.attach(document.body)
    $(document).ready(ready)
    $(document).on('page:load', ready)

  _initFields: ->
    $('.datepicker').datepicker(
        todayHighlight: true, autoclose: true, startDate: '+0d',
        orientation: 'auto')
      .focus -> $(this).blur()

  _initPriceCheck: =>
    updater =  (ev) =>
      key = $(ev.delegateTarget).parents('.attract-container').data('global-key')
      @updateInfo(key)
    $('.passenger-count, select[name=PUDAltLocation]').change(updater)
    $('.datepicker').on('changeDate', updater)

  _initTimesButtons: =>
    $attractionsWithTimes = $('.attract-container[data-display-times=true]')
    $attractionsNoTimes = $('.attract-container[data-display-times=false]')
    $('.times').hide()
    $('.button-update', $attractionsNoTimes).hide()
    $('.button-add', $attractionsWithTimes).hide()

    $('.button-update').click (ev) =>
        key = $(ev.delegateTarget).parents('.attract-container').data('global-key')
        @updateInfo(key)
        ev.preventDefault()

  _initTimeSelector: =>
    $('select[name=PickupTOD]').change (e) =>
      $timeSelector = $(e.currentTarget).parent().first()
      @_updateYardTimes($timeSelector)

  _updateYardTimes: ($timeSelector) =>
    $timeOption = $('option:selected', $timeSelector)
    $parent = $timeOption.parents('.attract-container').first()
    locationDisplayed = $('select[name=PUDAltLocation]:visible', $parent).length
    location = $('#PUDAltLocation', $parent).val()

    unless $timeOption.val() && locationDisplayed &&
           location != '' && location != 'NONE'
      $('.yard-departure', $parent).hide()
      return

    departure = $.format.date($timeOption.data('yard-departure'), "h:mm a")
    $('.yard-time', $parent).html(departure)
    $('.yard-departure').show()

  updateInfo: (key) =>
    attraction = $(".attract-container[data-global-key='#{key}']")
    originalFare = $('.original-fare', attraction).val()
    priceLabel = $('#attractions').data('price-label')
    price = $('.price', attraction)
    timesSelect = $('select[name=PickupTOD]', attraction)
    displayTimes = attraction.data('display-times')

    $('.times, .button-update, .button-add, .yard-departure', attraction).hide()
    timesSelect.empty()

    $('.price-label', attraction).html(priceLabel)
    @app.showLoader(price, placement: 'html')

    $('form', attraction).serialize()
    $.get @app.url('attraction_info'), $('form', attraction).serialize(),
        (data) =>
          if data.error
            price.html('N/A')
            if displayTimes
              $('.button-update', attraction).show()
            else
              $('.button-add', attraction).show()
              @app.autoResizeParent()
            alert data.error
          else
            price.html(data.fare)
            if data.times && displayTimes
              for time in data.times
                pickupTime = $.format.date(time.time, "h:mm a")
                timeOpt = $("<option value='#{time.time}'>#{pickupTime}</option>")
                timeOpt.data('yard-arrival', time.yard_arrival)
                timeOpt.data('yard-departure', time.yard_departure)
                timesSelect.append timeOpt
              $('.times', attraction).show()
              @_updateYardTimes(timesSelect)
            $('.button-add').show()
            @app.autoResizeParent()

root = exports ? this
root.Checkout = new Attractions(root.App)
