Rails.application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    mount HudsonBase::Engine, :at  => 'hudson_base', :as => 'hudson_base'

    # Testing
    get ':site_id/iframe_test' => 'reservations#iframe_test'

    # Configuration (old URL format)
    get 'configuration', to: redirect { |params, request|
        site_id = request.query_parameters['network_site_id'] ||
            request.session[:site_id]

        request.protocol + request.host_with_port +
        request.fullpath.sub(/\/configuration.*/, '') + '/' +
            site_id + '/configuration' },
        as: 'configuration'

    # Res - first page
    get ':site_id', to: redirect { |params, request|
        request.protocol + request.host_with_port + request.fullpath + "/res/one_way" }, as: 'root'
    get ':site_id/res/one_way' => 'reservations#one_way', as: 'oneway'
    get ':site_id/res/round_trip' => 'reservations#round_trip', as: 'roundtrip'
    get ':site_id/res/multi_leg' => 'reservations#multi_leg', as: 'multi'
    get ':site_id/res/cruise_redirect' => 'reservations#cruise'
    post ':site_id/res/update_legs' => 'reservations#update_legs', as: 'update_legs'
    get ':site_id/res/initial_locations' => 'reservations#initial_locations',
        as: 'initial_locations'
    get ':site_id/res/locations' => 'reservations#locations', as: 'locations'
    get ':site_id/res/svc_extras' => 'reservations#svc_extras', as: 'svc_extras'
    get ':site_id/res/preload' => 'reservations#preload_data', as: 'preload'

    # Res - Cruise first page
    get ':site_id/res/cruise' => 'cruise_reservations#index', as: 'cruise'
    get ':site_id/res/cruise/arriving_locations' =>
          'cruise_reservations#arriving_locations',
        as: 'cruise_arriving_locations'
    get ':site_id/res/cruise/arriving_hot_dest' =>
          'cruise_reservations#arriving_hotel_destinations',
        as: 'cruise_arr_hotel_dests'
    get ':site_id/res/cruise/arriving_hot_hotels' =>
          'cruise_reservations#arriving_hotel_hotels',
        as: 'cruise_arr_hotel_hotels'
    get ':site_id/res/cruise/booking_container' =>
          'cruise_reservations#booking_container',
        as: 'cruise_booking_container'
    get ':site_id/res/cruise/departing_locations' =>
          'cruise_reservations#departing_locations',
        as: 'cruise_dep_locations'
    get ':site_id/res/cruise/departing_hot_dest' =>
          'cruise_reservations#departing_hotel_destinations',
        as: 'cruise_dep_hotel_dests'
    post ':site_id/res/cruise/update_legs' =>
          'cruise_reservations#update_legs',
        as: 'update_cruise_legs'

    # Res - services
    get ':site_id/services/flight_cities' => 'services#flight_cities',
            as: 'flight_cities'
    get ':site_id/services' => 'services#index', as: 'services'
    post ':site_id/services/update' => 'services#update', as: 'services_update'

    # Res - cart
    get ':site_id/cart' => 'cart#index', as: 'cart'
    get ':site_id/cart/new_trip' => 'cart#new_trip', as: 'new_trip'
    post ':site_id/cart/delete' => 'cart#delete', as: 'delete_trip'
    post ':site_id/cart/empty' => 'cart#empty', as: 'empty_cart'
    post ':site_id/cart/apply_coupon' => 'cart#apply_coupon', as: 'apply_coupon'
    post ':site_id/cart/remove_coupon' => 'cart#remove_coupon',
         as: 'remove_coupon'

    # Res - checkout
    get ':site_id/checkout' => 'cart#checkout', as: 'checkout'
    post ':site_id/checkout/validate' =>
        'cart#validate_checkout', as: 'validate_checkout'
    get ':site_id/checkout/book' => 'cart#book', as: 'book'
    get ':site_id/receipt' => 'cart#receipt', :as => :receipt

    # Attractions
    get ':site_id/attractions' => 'attractions#index'
    post ':site_id/attractions' => 'attractions#index'
    match ':site_id/attractions/widget' => 'attractions#widget',
          as: 'attractions_widget', via: :all
    get ':site_id/attractions/update_info' => 'attractions#update_info',
        as: 'attraction_update_info'
    post ':site_id/attractions/validate' => 'attractions#validate',
         as: 'attraction_validate'
    get ':site_id/attractions/widget_test' => 'attractions#test_iframe'
    get ':site_id/attractions' => 'attractions#index', as: 'attractions'
    get ':site_id/attractions/details' => 'attractions#details',
        as: 'attraction_details'

    # Profiles
    match ':site_id/group' => 'profile#group_login', as: :group, via: :all
    match ':site_id/profile/group_login_window' => 'profile#group_login_window',
            as: :group_login_window, via: :all
    match ':site_id/profile/login_window' => 'profile#login_window',
            as: :login_window, via: :all
    match ':site_id/profile/login_modal' => 'profile#login_modal',
            as: :login_modal, via: :all
    get ':site_id/join_preferred_rider' => 'profile#join', as: :join_preferred_rider
    get ':site_id/reload_profile' => 'profile#reload_profile', as: :reload_profile
    get ':site_id/preferred_rider_home' => 'profile#home', as: :preferred_rider_home
    get ':site_id/profile/pr_redirect' => 'profile#pr_redirect', as: :pr_redirect
    get ':site_id/logout' => 'profile#logout', as: :logout_profiles
    get ':site_id/logout_group' => 'profile#logout_group', as: :logout_group_profile

    # Misc.
    match ':site_id/join_mailing_list_window' => 'info#join_mailing_list_window',
            as: :join_mailing_list_window, via: :all
    match ':site_id/note' => 'contact_us#process_form', as: :notes, via: :all
    get ':site_id/error' => 'info#error', as: :error_page
    get ':site_id/block_check' => 'info#block_check', as: :block_check
    get ':site_id/cookies_test' => 'application#cookies_test', as: :cookies_test

    # Configuration
    get ':site_id/configuration' => 'configuration#index',
      as: 'config_home'
    match ':site_id/config/add_site' => 'configuration#add_site', as: 'add_site', via: :all
    match 'config/advanced' => 'configuration#advanced', via: :all
    match 'config/basic' => 'configuration#basic', via: :all
    get ':site_id/config/logout_user' => 'configuration#logout_user',
      as: 'config_logout'
    post 'config/update_site_details' => 'configuration#update_site_details', as: 'update_site_details'
    match ':site/:controller/:action', via: :all
    match 'config/delete_site' => 'configuration#delete_site', via: :all
    get 'config/activate_site' => 'configuration#activate_site'
    get 'config/disable_site' => 'configuration#disable_site'
    post 'config/add_airport' => 'configuration#add_airport', as: 'add_airport'
    post 'config/delete_airport' => 'configuration#delete_airport',
         as: 'delete_airport'

# Old below - can probably be deleted
    post 'profile/login' => 'profile#login'
    match ':site_id/login' => 'hudson_base/login#index', as: :login, via: :all
    match 'groups' => 'profile#groups', :as => :groups, via: :all

    match 'update_price' => 'attractions#update_price', as: :update_price, via: :all
    match 'logout' => 'configuration#logout', :as => :logout, via: :all
    match '/' => 'reservations#index', via: :all
  end
end
