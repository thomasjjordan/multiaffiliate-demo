# Be sure to restart your server when you modify this file.

# Be sure to restart your server when you modify this file.
if Rails.env.development?
  Rails.application.config.session_store :mem_cache_store, :key => "_multiaff#{RELEASE_VERSION}_session", :expire_after => 24.hours, cookie_only: false
else
  Rails.application.config.session_store :active_record_store, :key => "_multiaff#{RELEASE_VERSION}a_session", :expire_after => 24.hours, cookie_only: false
end
