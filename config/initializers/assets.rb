# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif
    attractions/index.* cart/checkout.* cart/index.* contact_us/index.*
    details/index.* info/index.* profile/join reservations/index.*
    services/index.* configuration.* network.*
    attractions/detail.* cart/print_index.* cart/receipt.* configuration/all.*
    configuration/configuration.* configuration/login.* configuration/map_regions.*
    layouts/main.* profile/join.* shared/hudson-progress-bar.*
    shared/styled_form.* respond/respond.js]
