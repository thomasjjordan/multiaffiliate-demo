Date::DATE_FORMATS[:verbosedate] = "%A, %B %e"
Time::DATE_FORMATS[:verbosedate] = "%A, %B %e"
Date::DATE_FORMATS[:verbosetime] = "at %l:%M%P"
Time::DATE_FORMATS[:verbosetime] = "at %l:%M%P"