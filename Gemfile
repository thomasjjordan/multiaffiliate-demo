source 'https://rubygems.org'
ruby '2.1.1'

gem 'rails', '~> 4.2.0'
gem 'bundler'
gem 'exception_notification',
    git: 'https://github.com/smartinez87/exception_notification.git',
    branch: 'master'
gem 'mysql2', '0.3.15'

# Rails 4 compatibility gems
gem 'activerecord-session_store' #, github: 'thisisadam/activerecord-session_store'

gem 'hudson_base',
    git: "https://stash:QaWsEdRf@stash.hudsonltd.net/scm/rhb/hudson_base.git",
    branch: 'rails4' #,
    # tag: 'multiaff5.5'

gem 'sass-rails', '~> 5.0.0'
gem 'coffee-rails', " ~> 4.0.0"
gem 'uglifier', '>= 1.3.0'
gem 'compass-rails', '2.0.2'
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 4.2.0'
gem 'velocityjs-rails'
gem 'bootstrap-sass', '~> 3.3.1'
gem 'autoprefixer-rails' # required by bootstrap-sass
gem 'font-awesome-rails', '~> 4.2'
gem 'chardinjs-rails'
gem 'browser_details'
gem 'sanitize'
gem 'whenever', require: false

gem 'paleta' # For color manipulation

gem 'newrelic_rpm'

# React-related gems
gem 'react-rails', '~> 1.0'
gem 'sprockets-coffee-react'

group :development do
  gem 'capistrano'
  gem 'capistrano_colors'
  gem 'coffee-rails-source-maps'
  gem 'rvm-capistrano', require: false
  gem 'byebug'
  gem 'quiet_assets'  # Don't show asset-related log messages.

  # For testing emails in development. To use:
  # > bundle exec mailcatcher
  # Go to http://localhost:1080 for web interface.
  gem 'mailcatcher'

  # Ran into routing issues with web-console in 4.2.0, maybe try again after
  # a couple rails upgrades.
  # gem 'web-console', '~> 2.0'

  gem 'better_errors'
  gem 'binding_of_caller'
end

# Support sessions through get params
# (when loaded as a third-party iframe some browsers won't allow us to set cookies).
gem 'cookieless_sessions'

# Add additional security to our sessions (primarily because we sometimes need to
# support session keys through get params :( )
gem 'frikandel'
