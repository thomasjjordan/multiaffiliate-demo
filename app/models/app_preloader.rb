class AppPreloader
  def self.preload_data(current_site_id)
    PassengerType.all(current_site_id)

    # Trigger bulk token loads
#    airport_placeholder && airport_label_a && current_theme.app_color

    NetworkAirport.all(current_site_id).each do |air|
      air.service_areas.each do |sa|
        site_id, airport, service_area = sa.provider_site_id, air.code, sa.service_area_id

        Airports.airport_for(site_id, airport, service_area)
        Airports.for(site_id)
        Airlines.for(site_id, airport)
        Locations.by_type(site_id, {
                  airport_code: airport, service_area: service_area },
                  minimal_cols: true)
        Locations.for(site_id, airport_code: airport, service_area: service_area)
      end
    end
    true
  end

  def self.test(site_id)
    Rails.logger.info "\n>>>>>AppPreloader Go for #{site_id}!\n\n"
  end

end
