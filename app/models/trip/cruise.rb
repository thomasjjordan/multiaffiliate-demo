class Trip
  class Cruise < Trip
    self.max_legs = 4
    attr_accessor :cruise_site_id
    attr_reader :active_legs

    tokens 'MA_GLOBAL_' => {
        leg_header_cruise_0: {'LEGHEAD_CRUISE0' => 'Arriving at the airport'},
        leg_header_cruise_1: {'LEGHEAD_CRUISE1' => 'Hotel to cruise terminal'},
        leg_header_cruise_2: {'LEGHEAD_CRUISE2' => 'Cruise ship return'},
        leg_header_cruise_3: {'LEGHEAD_CRUISE3' => 'Hotel back to airport'}
    }

    def initialize(site_id, legs = [])
      @active_legs = [0,1,2,3]

      # Don't try to convert legs from other trips into cruise legs.
      super(site_id, [])
    end

    def cruise?; true end

    def dup_ship
      dep_ship_leg = [@legs[0], @legs[1]].find {|leg| leg.capture_ship?}
      arr_ship_leg = [@legs[2], @legs[3]].find {|leg| leg.capture_ship?}

      unless arr_ship_leg.Ship.present?
        arr_ship_leg.Ship = dep_ship_leg.Ship
      end
    end

    def is_ready_for_booking!
      super
    end

    def leg0_enabled?
      @active_legs.include? 0
    end

    def leg1_enabled?
      @active_legs.include? 1
    end

    def leg2_enabled?
      @active_legs.include? 2
    end

    def leg3_enabled?
      @active_legs.include? 3
    end

    def leg_errors
      error = ''
      legs.each.with_index do |leg, i|
        next if leg.errors[:base].empty?
        label = send("leg_header_cruise_#{leg.trip_index}")
        error << "<h3>#{label}</h3>"
        error << leg.errors[:base].join("<br/>")
      end
      error
    end

    def leg_index(leg)
      @legs.index(leg)
    end

    def legs
      actual_legs = []
      @active_legs.each { |index| actual_legs << @legs[index] }
      actual_legs
    end

    def massage_initial_legs
      @legs.each { |leg| leg.FlightType = 'D' }
      @legs[0].Direction = 'A'
      @legs[1].Direction = 'D'
      @legs[2].Direction = 'A'
      @legs[2].PickupTOD = nil
      @legs[3].Direction = 'D'

      @legs.each { |leg| leg.PrimarySite = cruise_site_id }
    end

    def replace_base_info(params = {})
      set_active_legs(params)
      original_params = params.dup
      adjust_param_indices(params)

      super

      # Dupe passenger and service extra stuff to all legs.
      network = NetworkObj.new(@site_id)

      network.service_extras(cruise_site_id).each do |extra|
        @legs[1..3].each do |l|
          value = original_params['0'][extra['FieldName']]
          l.send("#{extra['FieldName']}=", value)
        end
      end if cruise_site_id
      network.passenger_types.each do |type|
        @legs[1..3].each do |l|
          value = original_params['0'][type.field_name]
          l.send("#{type.field_name}=", value)
        end
      end

      if leg2_enabled?
        @legs[2].additional_required_base_fields = [:PickupTOD]
        if @legs[2].DropOffType == 'A' # Dropping off at airport
          @legs[2].service_adtl_params = {
            FlightType: 'R', FlightTime: @legs[2].PickupTOD
          }

          if @legs[2].PickupTOD
            @legs[2].FlightTOD = @legs[2].PickupTOD
            @legs[2].FlightTOD = @legs[2].FlightTOD.change(
                hour: original_params['2']['FlightTOD(4i)'],
                min: original_params['2']['FlightTOD(5i)'])
            @legs[2].FlightTOD += 1.day if @legs[2].FlightTOD < @legs[2].PickupTOD
          end
        else # Dropping off at hotel
          @legs[2].service_adtl_params = {}
          @legs[2].FlightTOD = @legs[2].PickupTOD
        end
      end
      legs.each { |leg| leg.FlightType ||= 'D' }
      if leg1_enabled? && leg0_enabled? && @legs[1].airport_key
        @legs[1].pickup = @legs[0].DropOffKey
      end
      @legs[3].pickup = @legs[2].DropOffKey if leg3_enabled? && @legs[3].airport_key
    end

    def trip_type
      :cruise
    end

    private

    # Multiaffiliate (outside of cruise) expects a linear match between incoming
    # params indices and the legs array. Because we allow nonlinear disabling of
    # legs for a cruise trips (ex. 0 and 2 enabled, 1 and 3 disabled) we need
    # to massage the params hash so the other Multiaffiliate methods work
    # correctly. (Using the previous example the params 1 index should represent
    # leg 2 instead of leg 1).
    #
    # The only times we need to worry about it is if a middle leg is disabled.
    # We're fine if trailing legs are disabled (it works itself out). Because
    # leg 3 is automatically disabled if leg 2 is disabled then the only cases we
    # need to handle are where leg 1 or leg 0 are disabled.
    def adjust_param_indices(params)
      # If leg 1 is disabled push up legs 2 and 3 in params.
      if !leg1_enabled?
        params['1'] = params['2']
        params['2'] = params['3']
        params.delete('3')
      end

      # If leg 0 is disabled push up legs 1, 2, and 3 in params.
      if !leg0_enabled?
        params['0'] = params['1']
        params['1'] = params['2']
        params['2'] = params['3']
        params.delete('3')
      end
    end

    def set_active_legs(params)
      @active_legs = []
      @active_legs << 0 if !params[:disable_leg0]
      if !params[:disable_leg1] &&
          (params['0'][:DropOffType] == 'H' || params[:disable_leg0])
        @active_legs << 1
      end
      @active_legs << 2 unless params[:disable_leg2]
      if !params[:disable_leg3] && !params[:disable_leg2] &&
          params['2'][:DropOffType] == 'H'
        @active_legs << 3
      end
    end
  end
end
