# A session has one to many trips
# A trip is either “one-way”, “round-trip”, or “multi-leg”
# A trip has 1 to a configurable amount of legs (or reservations).
#   one-way = 1 reservation
#   round-trip = 2 reservations centered around a single location (airport, port, etc.), both using the same provider.
#   multi-leg = multiple reservations with potentially different locations potentially using different providers, all one-way.

class Trip::MultiLeg < Trip
  tokens 'MA_GLOBAL_' => { leg_name: {'LEGNAME_MULTI' => 'Leg'} }

  def initialize(site_id, legs = [])
    super
    @leg_count = [initial_legs, max_legs].min
  end

  # If the max_legs are changed from a lower number to a higher number
  # anyone with a MultiLeg trip in their session may get an error. This
  # method prevents that.
  def ensure_legs_are_initialized
    if @legs.length < self.max_legs
      populate_legs
    elsif @legs.length > self.max_legs
      @legs.slice!(self.max_legs, @legs.length)
    end
  end

  def initial_legs
    Network::Config.multi_leg_initial_legs(@site_id)
  end

  def leg_count
    @leg_count
  end

  def leg_count=(count)
    @leg_count = count.to_i
  end

  def leg_errors
    error = ''
    legs.each.with_index do |leg, i|
      next if leg.errors[:base].empty?
      label = (i+1).ordinalize + ' ' + leg_name
      error << "<h3>#{label}</h3>"
      error << leg.errors[:base].join("<br/>")
    end
    error
  end

  def max_legs
    Network::Config.multi_leg_max_legs(@site_id)
  end

  def set_return_trip_statuses
    legs.each { |leg| leg.is_oneway! }
  end

  def trip_type; :multi end
end