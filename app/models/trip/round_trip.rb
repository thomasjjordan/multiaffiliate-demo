# A session has one to many trips
# A trip is either “one-way”, “round-trip”, or “multi-leg”
# A trip has 1 to 4 legs (or reservations).
#   one-way = 1 reservation
#   round-trip = 2 reservations centered around a single location (airport, port, etc.), both using the same provider.
#   multi-leg = multiple reservations with potentially different locations potentially using different providers, all one-way.

class Trip::RoundTrip < Trip
  self.max_legs = 2

  tokens 'MA_GLOBAL_' => {
    leg_name_a: {'LEGNAME_RTA' => 'Arriving'},
    leg_name_d: {'LEGNAME_RTD' => 'Departing'},
    leg_name_second_leg: {'LEGNAME_RTSECOND' => 'Return Trip'}
  }

  def dup_service_info_for_second_leg
    %i( location_addr location_zip location_details
        Airline FlightCity PUDDirections ).each do |attr|
      legs[1].send("#{attr}=", legs[0].send(attr))
    end

    first_leg_service_type = legs[0].ServiceType
    if legs[1].has_service?(first_leg_service_type)
      legs[1].service = legs[1].service_by_type(first_leg_service_type)
    end
  end

  def leg_errors
    error = ''
    legs.each.with_index do |leg, i|
      next if leg.errors[:base].empty?
      if i == 0
        label = leg.arrival? ? leg_name_a : leg_name_d
      else
        label = leg_name_second_leg
      end
      error << "<h3>#{label}</h3>"
      error << leg.errors[:base].join("<br/>")
    end
    error
  end

  # Second leg originally only consists of a return flight date.
  # Create a full set of params based on the first leg.
  def replace_base_info(params = {})
    params['1'] ||= {}
    params['1'] = params['1'].merge(params['0'].except(:FlightTOD, "FlightTOD(5i)", "FlightTOD(4i)"))
    params['1'][:Direction] = params['0'][:Direction] == 'A' ? 'D' : 'A'

    params['1'][:direction_type] = params['0'][:direction_type] == 'AtH' ?
        'HtA' : 'AtH'
    super(params)
  end

  def return_trip_status(leg)
    leg == legs[1] ? 'S' : 'F'
  end

  def roundtrip?; true end

  def set_return_trip_statuses
    legs[0].is_first_leg_of_roundtrip!
    legs[1].is_second_leg_of_roundtrip!
  end

  def trip_type; :roundtrip end

  private
    def validate_initial_legs
      # Second leg should be the opposite direction of the first.
      @legs[1].Direction = @legs[0].departure? ? 'A' : 'D'
    end
end