# A session has one to many trips
# A trip is either "one-way", "round-trip", or "multi-leg"
# A trip has 1 to 4 legs (or reservations).
#   one-way = 1 reservation
#   round-trip = 2 reservations centered around a single location
#               (airport, port, etc.), both using the same provider.
#   multi-leg = multiple reservations with potentially different locations
#               potentially using different providers, all one-way.

class Trip::OneWay < Trip
  self.max_legs = 1

  def leg_errors
    legs.first.errors[:base].join '<br/>'
  end

  def set_attraction_info(attraction, params = {})
    legs.first.set_attraction_info(attraction, params)
    is_attraction!
  end

  def set_return_trip_statuses
    legs.first.is_oneway!
  end

  def trip_type
    :oneway
  end
end
