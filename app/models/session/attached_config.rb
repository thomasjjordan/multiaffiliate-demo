class Session
  class AttachedConfig
    attr_accessor :new_url, :app_bgcolor, :app_color, :button_bgcolor,
                  :button_color, :subsection_bgcolor, :subsection_color,
                  :primary_bgcolor, :primary_color, :cart_page

    # Generate CSS based off override colors.
    def css_overrides
      {
        button: {
          background_color: @button_bgcolor,
          mouseover_color: @button_bgcolor,
          color: @button_color
        },
        app: {
          background_color: @app_bgcolor,
          color: @app_color
        },
        primary: {
          background_color: @primary_bgcolor,
          color: @primary_color
        },
        subsection: {
          background_color: @subsection_bgcolor,
          color: @subsection_color
        }
      }
    end
  end
end
