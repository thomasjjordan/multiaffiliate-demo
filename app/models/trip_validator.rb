class TripValidator < ActiveModel::Validator
  def validate(record)
    legs = record.active_legs

    if legs.any? {|leg| leg.PickupKey.blank? || leg.PickupKey == '--' }
      record.errors[:base] << "Please select a pickup location."
    end

    if legs.any? {|leg| leg.DropOffKey.blank? || leg.DropOffKey == '--' }
      record.errors[:base] << "Please select a drop-off location."
    end

    if legs.any? {|leg| leg.FlightTOD.blank? }
      record.errors[:base] << "Please select a pickup date."
    end
  end
end