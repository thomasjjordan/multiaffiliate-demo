class ServiceInfoValidator < ActiveModel::Validator

  def validate(res)
    site_id = res.home_site_id
    error_required = token(site_id, 'MA_GLOBAL_ERR_GEN_REQUIRED',
        "This field is required")
    error_max_gratuity = token(site_id, 'MA_GLOBAL_ERR_MAX_GRAT',
        'Gratuity must be less than %MAXGRAT%')
    error_positve_gratuity = token(site_id, 'MA_GLOBAL_ERR_POS_GRAT',
        'Gratuity must be at least 0.')

    if res.capture_flight_info?
      if res.flight_number_required?
        res.validates_presence_of :FlightNumber, message: error_required
      end

      if res.flight_city_required?
        res.validates_presence_of :FlightCity, message: error_required
      end

      if res.airline_required?
        res.validates_presence_of :Airline, message: error_required
      end
    end

    if !res.location.readonly_address?
      res.validates_presence_of :location_addr, message: error_required
    end

    if res.UserGratuity.present? && res.max_gratuity.present? &&
        res.max_gratuity.to_f != 0 &&
        res.UserGratuity.to_f > res.max_gratuity
      grat_error = error_max_gratuity.gsub(/%MAXGRAT%/, res.max_gratuity_percent.to_s)
      res.errors.add :UserGratuity, grat_error
    end

    if res.UserGratuity.present? && res.UserGratuity.to_f < 0
      res.errors.add :UserGratuity, error_positve_gratuity
    end
  end

  private
    def token(site_id, token, default)
      val = Tokens.for(site_id, token)
      val.blank? ? default : val
    end
end
