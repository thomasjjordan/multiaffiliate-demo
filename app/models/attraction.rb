# An abstraction class representing an attraction.
class Attraction
  attr_reader :location
  delegate :Code, :description, :fare, :Name, :passenger_type_alias,
           :passenger_type_filter, :service_extra_group, :site_id,
           :thumbnail_url, to: :@location

  alias_method :code, :Code
  alias_method :name, :Name

  def self.all(attraction_site_id, options = {})
    filters = options[:filters] || {}

    # Make sure we don't pass global code to the web service.
    code = filters[:fare_code]
    if code && code.include?(Attraction.global_code_separator)
      filters[:fare_code] = code.split(Attraction.global_code_separator)[1]
    end
    locs = Array(attraction_site_id).collect do |site_id|
      Locations.for(site_id,
                    { airport_code: pickup_key(site_id) }.merge(filters),
                    quick_price_service: 1,
                    include_cols: ['FareOpts'])
    end.flatten
    locs.collect { |loc| new(loc) }
  end

  def self.find(attraction_site_id, code)
    if code.include?(Attraction.global_code_separator)
      code = code.split(Attraction.global_code_separator)[1]
    end
    loc = Locations.for_key(attraction_site_id, code,
        { airport_code: pickup_key(attraction_site_id) },
        {
          quick_price_service: 1,
          include_cols: ['FareOpts']
         })
    new(loc) if loc
  end

  def self.global_code_separator
    '_'
  end

  def self.pickup_key(attraction_site_id)
    key = Tokens.for(attraction_site_id, 'NETWORK_ATT_PICKUPKEY')
    key.blank? ? 'xx1' : key
  end

  def self.service_type(attraction_site_id)
    type = Tokens.for(attraction_site_id, 'NETWORK_ATT_SERVICE')
    type.blank? ? 1 : type.to_i
  end

  def initialize(location)
    @location = location
  end

  def alternate_locations
    return unless has_alternate_location_list?

    if matches = /ALL=(\w+):(\w+)/.match(@location.options_string)
      airport_key, loc_group = matches[1], matches[2]
      return Locations.for(site_id,
          airport_code: airport_key, loc_group: loc_group)
    end
  end

  def default_date(additional_dates)
    additional_dates = [] if additional_dates.blank?
    global_date = Tokens.for(site_id, "ATT_DEFAULT_DATE")
    default_dates = additional_dates +
                    [@location.default_date, global_date, 7.days.from_now]

    default_date = default_dates.find { |date| Chronic.parse(date.to_s) }
    Chronic.parse(default_date.to_s)
  end

  def details
    location_details.Details if location_details
  end

  def display_times?
    # The FareOpts DTM flag is for backwards compatibility.
    !!(@location.display_times.to_i == 1 || @location.options_string =~ /DTM/)
  end

  def global_code
    site_id + Attraction.global_code_separator + code
  end

  # ex: ALL=xx5:SG
  def has_alternate_location_list?
    @location.options_string =~ /ALL=\w+:\w+/
  end

  def has_details?
    @location.details_available?
  end

  def instructions
    location_details.Instructions if location_details
  end

  def location_details
    @location.details
  end

  def passenger_types
    return nil if passenger_type_filter.blank? && passenger_type_alias.blank?
    PassengerType.all(site_id,
                      filter: passenger_type_filter,
                      psgr_alias: passenger_type_alias)
  end

  def pickup_key
    Attraction.pickup_key(site_id)
  end

  def service_extras
    if service_extra_group.present?
      ServiceExtras.all_for_alias(site_id, service_extra_group)
    else
      []
    end
  end

  def service_type
    Attraction.service_type(site_id)
  end

  def special_info
    location_details.SpecialInfo if location_details
  end

  def summary_description
    location_details.SummaryDescr if location_details
  end

  def terms_and_conditions
    location_details.TermsAndConditions if location_details
  end

  def type_label
    LocationType.label(site_id, @location.FareType)
  end

  def as_json(options = {})
    json = {}
    methods = [:code, :description, :details, :fare, :instructions,
      :has_details?, :name, :special_info,
      :summary_description, :terms_and_conditions, :thumbnail_url]
    methods.each do |method|
      if method == :code
        json[method] = global_code
      else
        json[method] = send(method)
      end
    end
    return json
  end
end
