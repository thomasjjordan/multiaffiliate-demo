class CheckoutValidator < ActiveModel::Validator
  def validate(record)
    token_prefix = "MA_CHECKOUT_"

    # Payment
    if (record.payment_type.to_i != 3 && record.cc_required?) ||
        record.payment_type.to_i == 1

      # CC expiration must be in the future.
      if record.CCExp.year < Time.now.year ||
          (record.CCExp.year == Time.now.year && record.CCExp.month < Time.now.month)
        error = token(record, "#{token_prefix}ERR_CCEXP",
            "Expiration date must be in the future")
        record.errors[:CCExp] << error
      end

      # CC number required.
      if record.CCNumber.blank?
        error = token(record, "#{token_prefix}ERR_CCNUM",
            "Card number is required")
        record.errors[:CCNumber] << error
      else
        record.CCNumber = record.CCNumber.to_s.gsub(/[^\d]/, "")
        unless record.CCNumber.creditcard?
          error = token(record, "#{token_prefix}ERR_CCNUM_VALID",
              "Card number is invalid")
          record.errors[:CCNumber] << error
        end
      end

      # CC name required.
      if record.CCDetails.blank?
        error = token(record, "#{token_prefix}ERR_CCNAME",
            "Cardholder's name is required")
        record.errors[:CCDetails] << error
      end

      # CC zip required.
      if record.cczip_displayed? && record.CCZip.blank?
        error = token(record, "#{token_prefix}ERR_CCZIP",
            "Cardholder's postal code is required")
        record.errors[:CCZip] << error
      end

      # CC ccvv required.
      if record.ccvv_displayed? && record.CCVV.blank?
        error = token(record, "#{token_prefix}ERR_CCVV", "Security code is required")
        record.errors[:CCVV] << error
      end
    end


    # Contact Info
    if record.Name.blank?
      error = token(record, "#{token_prefix}ERR_NAME", "Name is required")
      record.errors[:Name] << error
    end

    if record.telephone_displayed? && record.Telephone.blank?
      error = token(record, "#{token_prefix}ERR_TELEPHONE",
        "Telephone number is required")
      record.errors[:Telephone] << error
    end

    if record.EmailAddr.blank?
      error = token(record, "#{token_prefix}ERR_EMAIL", "Email address is required")
      record.errors[:EmailAddr] << error
    else
      record.EmailAddr.strip!
      if record.EmailAddr !~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
        error = token(record, "#{token_prefix}ERR_EMAIL_FORMAT",
            "Email address must be a valid email.")
        record.errors[:EmailAddr] << error
      end

      if record.email_validation? && record.EmailAddr != record.EmailAddr_confirmation
        error = token(record, "#{token_prefix}ERR_EMAILCONF",
            "Your email address confirmation must be the same as your email address")
        record.errors[:EmailAddr_confirmation] << error
      end
    end

    # Terms and Conditions
    if record.terms_and_conditions? && record.terms_conditions.to_i != 1
      error = token(record, 'TERMCONDERR', "Please accept the terms and conditions.")
      record.errors[:terms_conditions] << error
    end
  end

  private

  def token(record, name, default = '')
    value = Tokens.for(record.site_id, name)
    value.blank? ? default : value
  end
end
