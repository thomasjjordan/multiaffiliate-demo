require 'chronic'

class MultiaffiliateReservation < Reservation
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  validates_with BaseInfoValidator, if: Proc.new { |res| res.validation_step == :base }
  validates_with ServiceInfoValidator, if: Proc.new { |res| res.validation_step == :service }

  attr_accessor :direction_type, :services_cache, :attraction_display_time,
                :additional_required_base_fields, :service_adtl_params,
                :parent_trip, :incomplete_FlightTOD_time, :incomplete_FlightTOD_date,
                :incomplete_PickupTOD_date, :incomplete_PickupTOD_time
  attr_reader :validation_step

  class Error < RuntimeError; end
  class AttractionServiceError < Error; end

  def airport
    airport_key
  end

  def available_ships
    return [] unless @parent_trip.cruise?

    if trip_index == 0
      Airlines.for(home_site_id, location_key)
    else
      Airlines.for(home_site_id, airport_key)
    end
  end

  def capture_flight_info?
    if @parent_trip.cruise?
      trip_index == 0 ||  trip_index == 3 || trip_index == 2 && self.DropOffType == 'A'
    else
      true
    end
  end

  def capture_ship?
    return false unless @parent_trip.cruise?

    (trip_index == 0 && self.DropOffType == 'A') ||
        trip_index == 1 || trip_index == 2
  end

  def clear_base_info
    self.FlightTOD = nil
    self.airport = nil
    self.ServiceArea = nil
    self.PrimarySite = nil
    self.FlightType = nil
    self.dropoff = nil
    self.pickup = nil
    self.AltPassengers = 0
    self.BagCount = 0
    self.Passengers = 1
  end

  def clear_service_info
    %i(service PickupTOD DropOffTOD location_addr location_details
        location_zip Airline FlightCity FlightNumber PickupExtras
        OffPeakCharge ShortNoticeCharge PUDDirections UserGratuity
        Ship).each do |attr|
      self.send("#{attr}=", nil)
    end
  end

  def fare_without_discount
    self.Fare + self.GroupDisc
  end

  # Given a params hash and a date fieldname this method:
  #   1) Updates params[field] with a Time object if it can parse both date
  #      and time from values in the params hash.
  #   2) Updates the "incomplete_<field>_date" and "incomplete_<field>_time"
  #      instance variables with Time objects if only the date portion or
  #      only the time portion can be parsed.
  # No useful return value is generated.

  def generate_date_times(field, params = {})
    # If we already have a time then we're done here.
    return if params[field].is_a?(Time) || params[field].blank?

    begin
    parsed_date = Date.strptime(params[field], SiteInfo.configured_date_format(@SiteID))
    rescue ArgumentError => e
    # If we got bad month/day/year data
      if e.to_s =~ /invalid date/
        @flight_date = ""
      end
    end

    date, hour, min = parsed_date, params["#{field}(4i)"], params["#{field}(5i)"]
    instance_variable_set("@incomplete_#{field}_date".to_sym, nil)
    instance_variable_set("@incomplete_#{field}_time".to_sym, nil)
    params.delete(field)

    # If the hour is set but the minute is not, let's assume the minute is 00.
    min = '00' if hour.present? && min.blank?

    # If we have all the components create a full-fledged date obj.
    if [date, hour, min].all?(&:present?)
      params[field] = Chronic.parse("#{date} #{hour}:#{min}")

    # If we only have date create an incomplete date var.
    elsif date.present?
      inc_date = Chronic.parse(date)
      instance_variable_set("@incomplete_#{field}_date".to_sym, inc_date)

    # If we only have a time create an incomplete time var.
    elsif hour.present? && min.present?
      inc_time = Time.now
      inc_time = inc_time.change(hour: hour, min: min)
      instance_variable_set("@incomplete_#{field}_time".to_sym, inc_time)
    end
  end

  def has_service?(type)
    service = service_by_type(type)
    service.present? && service.fare_available?
  end

  def load_services
    adtl_params = @service_adtl_params || {}
    @services_cache = svcs = current_network.services_for(self, adtl_params)

    # No selectable services? error.
    selectable_services = svcs.find_all { |s| s.fare_available? }
    if selectable_services.blank?
      errors[:base] << current_network.no_service_msg(self.home_site_id).html_safe
      return []
    end

    if services.none? { |s| s.type.to_i == self.ServiceType.to_i }
      self.service = nil
    end
    svcs
  end

  def recalculate_fare(options = {}, adtl_params = {})
    adtl_params = adtl_params.merge(@service_adtl_params || {})
    super(options, adtl_params)
  end

  # Returns a service object given a service type.
  def service_by_type(type)
    services.find {|s| s.type.to_i == type.to_i}
  end

  # Returns a service time object given a service type and
  # pickup time object to match against.
  def service_time(service_type, pickup_time)
    if svc = service_by_type(service_type)
      return svc.times.find {|t| t.time == pickup_time}
    end
    nil
  end

  def services
    @services_cache ||= load_services
  end

  def set_airport_and_primary_site(airport, primary_site = nil)
    return if airport.blank?

    primary_site = primary_site.blank? ? provider_from_airport(airport) : primary_site
    network_site = Network::Site.for(self.SiteID, primary_site) if primary_site.present?
    self.PrimarySite = primary_site
    self.ServiceArea = network_site.try(:service_area_id)
    self.airport = airport
  end

  def set_attraction_info(attraction, params)
    self.FlightType = 'R'
    self.Direction = 'A'
    set_airport_and_primary_site(attraction.pickup_key, attraction.site_id)
    self.ServiceType = attraction.service_type
    self.location = params[:DropOffKey]
    params.except(:PickupKey, :DropOffKey).each { |k, v| send("#{k}=", v) }
    @attraction_display_time = attraction.display_times?

    # Calculate service / fare
    svc_params = {
      'Direction' => self.Direction,
      'ServiceType' => self.ServiceType,
      'PickupKey' => self.PickupKey,
      'FlightType' => self.FlightType,
      'GroupAlias' => self.GroupAlias
    }
    params.each { |k, v| svc_params[k.to_s] = v }
    service = Services.for(attraction.site_id, svc_params).try(:first)

    if service
      self.service = service
      self.ServiceType = translate_service_type(attraction.site_id,
                                                attraction.service_type)

      if attraction.display_times?
        time = service.time(svc_params['PickupTOD'])
        self.pickup_time = time if time
      elsif service.times.present?
        self.pickup_time = service.times.first.time
      end
    else
      fail AttractionServiceError
    end
  end

  def set_base_info(params = {})
    self.Direction = params[:Direction] if params[:Direction]
    set_airport_and_primary_site(params[:airport], params[:PrimarySite])

    dates_to_parse = [:FlightTOD]
    dates_to_parse << :PickupTOD if params[:PickupTOD]
    dates_to_parse.each { |field| generate_date_times(field, params) }

    begin
      omitted_params = %i(
        airport PrimarySite Direction FlightTOD(4i) FlightTOD(5i) PickupTOD(4i)
        PickupTOD(5i)
      )
      assign_attributes params.except(*omitted_params)

    rescue NoMethodError
      Rails.logger.warn "Unable to default all parameters (bad parameter detected)."
    end
  end

  def set_service_info(params = {})
    self.service = service_by_type(params[:ServiceType])

    pickup_time = Chronic.parse(params[:PickupTOD])
    self.pickup_time = service_time(params[:ServiceType], pickup_time)

    assign_attributes params.except(:ServiceType, :PickupTOD)
  end

  def Ship; @ship end

  # Ship is stored in the port's address field as a special string.
  def Ship=(ship)
    @ship = ship
    return unless ship.present?
    address = "SHIP NAME: #{ship}"

    if trip_index < 2
      self.DropOffAddr = address
    else
      self.PickupAddr = address
    end
  end

  def trip_index
    @parent_trip.base_legs.index(self)
  end

  def valid_base_info?
    @validation_step = :base
    valid?
  end

  def valid_service_info?
    @validation_step = :service
    valid?
  end

  def provider_from_airport(airport)
    Rails.cache.fetch("provider_from_airport:#{self.SiteID}:#{airport}") do
      sites = Network::Airport.active_providers(self.SiteID, airport)
      if sites.blank? || sites.length > 1
        nil
      else
        sites.first.ProviderSiteId
      end
    end
  end

  private

  def current_network
    NetworkObj.new(self.SiteID)
  end

  def translate_service_type(provider_site_id, service_type)
    Network::ServiceTranslation.translate_provider_to_network(self.SiteID,
                                                              provider_site_id,
                                                              service_type)
  end
end
