require 'chronic'

# A session has one to many trips
# A trip is either “one-way”, “round-trip”, or “multi-leg”
# A trip has 1 to 4 legs (or reservations).
#   one-way = 1 reservation
#   round-trip = 2 reservations centered around a single location (airport, port, etc.), both using the same provider.
#   multi-leg = multiple reservations with potentially different locations potentially using different providers, all one-way.

class Trip
  include HasSite
  include UsesTokens
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  class_attribute :max_legs
  validates_with TripValidator

  class Error < RuntimeError; end
  class BookingError < Error; end

  attr_accessor :trip_type, :error_state, :booking_error
  attr_reader :site_id, :booked

  def initialize(site_id, legs = [])
    @site_id = site_id
    @status = :initialized

    @default_attrs ||= {}
    @default_attrs.merge!(
      Passengers: 1,
      SourceType: 18,
      SiteID: @site_id,
      FlightType: nil)

    @ready_for_booking = false
    @legs = [].fill(0, max_legs) { |i| legs.try(:[], i) || new_res }
    massage_initial_legs
    validate_initial_legs
  end

  def self.from_trip(old_trip)
    if old_trip.class == self
      return old_trip
    else
      # Don't adapt cruise reservations as they are special-case enough
      # that we don't want to worry about edge cases.
      legs = old_trip.cruise? ? nil : old_trip.legs

      new_trip = self.new(old_trip.site_id, legs)
      new_trip.clear_errors
      new_trip
    end
  end

  # Pass on incoming values to each leg.
  def assign_attributes(values, options = {})
    values.each_key do |i|
      @legs[i.to_i].assign_attributes(values[i])
    end
  end

  def attach_coupon(coupon)
    @legs.each do |leg|
      leg.GroupAlias = coupon.WebID
      leg.recalculate_fare
    end
  end

  # Coupon codes replace group profile support so we should be able
  # to remove this at some point (AGM 7/22/15)
  def attach_group_profile(profile)
    @legs.each {|leg| leg.GroupAlias = profile.WebID }
  end

  def attach_user_profile(profile)
    @legs.each do |leg|
      leg.ProfileIDWEB = profile.WebID
      if profile.ParentID.present?
        leg.GroupAlias = profile.ParentID
      end
    end
  end

  def attach_itinerary_id(iid)
    legs.each {|leg| leg.ItineraryID = iid}
  end

  # Attractions are available for a given trip if any of the arrival legs
  # has an associated attraction site configured.
  def attractions_available?
    return false if is_attraction?

    legs.find_all(&:arrival?).any? do |leg|
      airport = Network::Airport.where(SiteID: @site_id, Code: leg.airport)
      airport.first.present? && airport.first.attractions.present?
    end
  end

  def available_attractions
    return [] if is_attraction?

    legs.find_all(&:arrival?).collect do |leg|
      airport = Network::Airport.where(SiteID: @site_id, Code: leg.airport)
      if airport.first.present?
        airport.first.attractions.pluck(:ProviderSiteId)
      end
    end.delete_if(&:blank?).compact.uniq
  end

  def base_legs
    @legs
  end

  def booked?
    @booked
  end

  def booked!
    @booked = true
  end

  def clear_errors
    @legs.each {|leg| leg.errors.clear}
  end

  def clear_group_profile!
    @legs.each {|leg| leg.GroupAlias = nil }
  end

  def clear_roundtrip_only_legs!
    roundtrip_only_sites = Network::Site.where(
        SiteID: current_site_id, DisableOneWay: 1).collect(&:ProviderSiteId)
    sites_reset = false
    legs.each do |leg|
      if leg.PrimarySite.present? && roundtrip_only_sites.include?(leg.PrimarySite)
        leg.clear_base_info
        sites_reset = true
      end
    end
    sites_reset
  end

  def clear_user_profile!
    @legs.each do |leg|
      leg.ProfileIDWEB = nil
      leg.GroupAlias = nil
    end
  end

  def collected_payment_types
    legs.collect{|leg| leg.payment_types}
  end

  def cruise?; false end

  def direction_types
    # Cruise direction types that were removed: AtC HtC CtH CtA
    %w{AtH HtA}.collect { |type| DirectionType.new(@site_id, type) }
  end

  def has_leg_errors?
    legs.any? {|leg| leg.errors.any?}
  end

  def is_attraction!
    @is_attraction = true
  end

  def is_attraction?
    !!@is_attraction
  end

  def is_ready_for_booking!
    @ready_for_booking = true
  end

  def leg_count
    @legs.length
  end

  # A concatenation of all errors each leg currently has in a nice
  # format that can be directly shown to users.
  def leg_errors; end

  def legs
    @legs[0, leg_count]
  end

  # Attempt to load services for all legs and store service data locally
  # for use later.
  def load_services
    legs.each.with_index {|leg| leg.load_services }
  end

  # Stub allowing subclasses to update initial leg
  # values as necessary.
  def massage_initial_legs; end

  # Create a new reservation, defaulting certain attributes.
  def new_res(attrs = {})
    direction = direction_types.first
    res_attrs = @default_attrs.merge({
      Direction: direction.direction
    })
    res = MultiaffiliateReservation.new(res_attrs.merge(attrs))
    res.direction_type = direction.type
    res.parent_trip = self
    res
  end

  def payment_type=(type)
    legs.each {|leg| leg.PaymentType = type}
  end

  def points_program=(program_id)
    legs.each {|leg| leg.set_ResFlag('FEI', program_id) }
  end

  def populate_legs
    @legs = @legs.fill(0, max_legs) {|i| @legs.try(:[], i) || new_res}
  end

  # An array of this trip's legs' primary sites.
  def primary_sites
    legs.collect(&:PrimarySite).uniq
  end

  def ready_for_services!
    @ready_for_services = true
  end

  def ready_for_services?
    !!@ready_for_services
  end

  def not_ready_for_services!
    @ready_for_services = false
  end

  def ready_to_book?
    !!@ready_for_booking
  end

  def recalculate_fares(leg = nil)
    if leg.present?
      legs[leg.to_i].try(:recalculate_fare)
    else
      legs.each {|leg| leg.recalculate_fare}
    end
  end

  # Receives parameters coming in from the -first- reservation process page.
  # Ensures that all legs get that data in the format they need.
  def replace_base_info(params = {})
    clear_errors
    legs.each.with_index do |leg, index|
      leg.clear_base_info
      leg.clear_service_info
      leg.set_base_info(params[index.to_s])
    end
    set_return_trip_statuses
  end

  # Receives parameters coming in from the -services- reservation process page.
  # Ensures that the given leg gets that data in the format it needs.
  def replace_service_info(leg_index, params = {})
    clear_errors
    if leg = legs[leg_index.to_i]
      leg.set_service_info(params)
    end
  end

  def roundtrip?; false end

  def service_still_available?(leg = nil)
    if leg.present?
      return legs[leg.to_i].try(:service_still_available?)
    else
      return legs.all? {|leg| leg.service_still_available?}
    end
  end

  # Need to temporarily change primary site in order to support
  # duplicate sites with singular back-ends
  def set_primary_site_to_original_value
    legs.each.with_index {|leg, i| leg.PrimarySite = @original_primary_sites[i]}
  end

  # Need to temporarily change primary site in order to support
  # duplicate sites with singular back-ends
  def set_primary_site_to_res_site
    @original_primary_sites = []
    legs.each.with_index do |leg, i|
      @original_primary_sites[i] = leg.PrimarySite
      res_siteid = Tokens.for(@original_primary_sites[i], 'GEN_RESSITEID')
      leg.PrimarySite = res_siteid if res_siteid.present?
    end
  end

  def set_return_trip_statuses; false end

  def total_cost
    legs.inject(BigDecimal('0.0')) do |sum, item|
      sum + BigDecimal(item.Fare.to_s)
    end.to_f
  end

  def total_discount
    legs.inject(BigDecimal('0.0')) do |sum, item|
      sum + BigDecimal(item.GroupDisc.to_s)
    end.to_f
  end

  def total_tax
    legs.inject(BigDecimal('0.0')) do |total, leg|
      total + BigDecimal(leg.SalesTax.to_s) + BigDecimal(leg.SalesTax2.to_s)
    end.to_f
  end

  # Default reservation attributes. There is some conversion to support
  # fields that we supported in earlier versions of Multiaffiliate.
  # This class's method is intended to be run before a more specific
  # subclass's is run. It cleans up the data as much as possible.
  def update_from_default_values(params)
    # Convert "legacy" leg keys to new ones.
    if params[:starting_direction] # "Legacy mode"
      if params[:starting_direction] == 'to'
        params['0'] = params.delete('departure') || {}
        params['1'] = params.delete('arrival') || {}
        params['0'][:direction_type] = 'HtA'
        params['1'][:direction_type] = 'AtH'
      else
        params['0'] = params.delete('arrival') || {}
        params['1'] = params.delete('departure') || {}
        params['0'][:direction_type] = 'AtH'
        params['1'][:direction_type] = 'HtA'
      end
    end

    # Normalize per-leg fields.
    max_legs.times do |i|
      key = i.to_s
      params[key] ||= {}

      # Direction
      if params[key][:direction_type]
        dir = direction_types.find {|dt| dt.type == params[key][:direction_type]}
        params[key][:Direction] = dir.try(:direction)
      end

      # Airport
      params[key][:airport] = params[:airport] if params[:airport]

      # Site
      params[key][:PrimarySite] = params[:site] if params[:site]
      params[key][:PrimarySite] = params[key].delete(:site) if params[key][:site]

      # Location
      params[key][:location] = params[:location] if params[:location]
      params[key][:location] = params[key].delete(:location) if params[key][:location]

      # Flight type
      params[key][:FlightType] = params[key].delete(:flight_type) if params[key][:flight_type]

      # Flight date
      if params[key][:flight_date]
        unless params[key][:flight_date].blank? || params[key][:flight_time].blank?
          date = "#{params[key][:flight_date]} #{params[key][:flight_time]}#{params[key][:flight_ampm]}"
          site = Site.new( params[key][:PrimarySite] || @site_id )
          Time.use_zone(site.time_zone) do
            params[key][:FlightTOD] = Chronic.parse(date)
          end
        end
      end
    end

    replace_base_info(params)
  end

  def valid_base_info?
    legs.all? &:valid_base_info?
  end

  def valid_service_info?(leg_index)
    legs[leg_index.to_i].try(:valid_service_info?)
  end

  private
    def validate_initial_legs; end
end
