class LocationDialog
  include HasSite
  include UsesTokens

  attr_reader :site_id, :leg

  tokens 'MA_HOME_' => {
    type_display_P: { 'TYPEDISP_P' => 2 },
    type_display_S: { 'TYPEDISP_S' => 3 },
    type_display_H: { 'TYPEDISP_H' => 3 },
    type_display_A: { 'TYPEDISP_A' => 1 },
    type_display_C: { 'TYPEDISP_C' => 0 },
    type_display_O: { 'TYPEDISP_O' => 0 },
    type_display_R: { 'TYPEDISP_R' => 1 },
    type_display_Y: { 'TYPEDISP_Y' => 1 },
    type_display_D: { 'TYPEDISP_D' => 2 },
    type_display_G: { 'TYPEDISP_G' => 3 },
    type_icon_P: { 'TYPEICON_P' => 'building' },
    type_icon_S: { 'TYPEICON_S' => 'university' },
    type_icon_H: { 'TYPEICON_H' => 'hotel' },
    type_icon_A: { 'TYPEICON_A' => 'plane' },
    type_icon_C: { 'TYPEICON_C' => 'building' },
    type_icon_O: { 'TYPEICON_O' => 'square' },
    type_icon_R: { 'TYPEICON_R' => 'train' },
    type_icon_Y: { 'TYPEICON_Y' => 'ship' },
    type_icon_D: { 'TYPEICON_D' => 'home' },
    type_icon_G: { 'TYPEICON_G' => 'building' }
  }

  def initialize(site_id, leg)
    @site_id = site_id
    @leg = leg
  end

  def initial_locations
    loc_list = @leg.available_locations(nil, minimal_cols: true)

    response = []
    loc_list.each do |cat|
      next if cat[:locations].empty?
      locs = []

      # Don't return a loc list if we're going to allow searching.
      if display_type(cat[:type], cat[:locations]) != :search ||
         cat[:locations].length < 5
        cat[:locations].each do |loc|
          # Create an optimized version of the loc object to improve speed.
          stripped_loc = {}
          [:Name, :Code, :State, :City].each do |attr|
            stripped_loc[attr] = loc.send(attr)
          end
          locs.push(stripped_loc)
        end
      end

      if cat[:label] =~ /(.*?)<a[^\/]*?class=.?boxpopup.?.*?>(.*?)<span[^\/]*?><div>(.*?)<\/div><\/span><\/a>(.*)/
        label_without_popup = "#$1<span class='popover-trigger'>#$2</span>#$4"
      end

      response.push(
        label: cat[:label],
        label_without_popup: (label_without_popup || cat[:label]),
        cat: cat[:type],
        catInfo: cat[:info],
        displayType: display_type(cat[:type], cat[:locations]),
        icon: type_icon(cat[:type]),
        locations: locs)
    end

    response
  end

  def display_type(type, locs)
    mode = send("type_display_#{type}").to_i
    mode = %i(auto list search both)[mode]

    if mode == :auto
      locs.length > 15 ? :both : :list
    else
      mode
    end
  end

  def type_icon(type)
    send("type_icon_#{type}") || 'square'
  end
end
