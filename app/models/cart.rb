class Cart
  attr_reader :items, :session

  def <<(item)
    @items << item
  end

  def add_and_make_current_item(item)
    @items << item
    @current_item = @items.length-1
    item
  end

  def booked_items
    @items.find_all {|item| item.booked?}
  end

  def empty?
    @items.empty?
  end

  def empty!
    @items = []
  end

  def freeze_group
    @frozen_group_alias = (@session.group_profile ? @session.group_profile.WebID.dup : nil)
    @frozen_group_name = (@session.group_profile ? @session.group_profile.Name.dup : nil)
  end

  def freeze_total
    @frozen_total = total
    @frozen_discount = total_discount
  end

  def group_alias
    @frozen_group_alias || @session.group_profile.try(:WebID)
  end

  def group_name
    @frozen_group_name || @session.group_profile.try(:Name)
  end

  def initialize(session_obj)
    @session = session_obj
    @items = []
  end

  def items_ready_for_booking
    @items.find_all {|item| item.ready_to_book?}
  end

  def length
    @items.length
  end

  def current_item
    if @current_item && @items[@current_item]
      @items[@current_item]
    else
      @items.first
    end
  end

  def current_item=(index)
    @current_item = index
  end

  def delete_item(index_or_object)
    if index_or_object.is_a?(Integer)
      @items.delete_at(index_or_object)
      if @current_item == index_or_object
        @current_item = @items.length - 1
        @current_item = nil if @current_item == -1
      end
    else
      @items.delete(index_or_object)
      @current_item = @items.length - 1
      @current_item = nil if @current_item == -1
    end
  end

  def empty?
    items_ready_for_booking.empty?
  end

  def empty!
    @items = []
  end

  def remove_unbooked_items!
    items.delete_if {|item| !item.booked?}
  end

  def replace_current_item(new_item)
    delete_item current_item
    add_and_make_current_item new_item
  end

  def negatively_affected_items_if_profile_applied(coupon)
    statuses = items_ready_for_booking.collect do |trip|
      trip.legs.collect do |leg|
        {
          leg: {
            attraction: trip.is_attraction?,
            pickup: leg.PickupLocation,
            pickup_date: leg.PickupTOD.to_s(:verbosedate),
            dropoff: leg.DropOffLocation,
            service: leg.service_name.dehtml
          },
          status: leg.status_when_profile_applied(coupon)
        }
      end
    end.flatten

    statuses.select do |leg|
      leg[:status] &&
        (leg[:status].key?(:fare_increase) ||
          leg[:status].key?(:service_no_longer_available))
    end
  end

  def negatively_affected_items_if_profile_removed
    statuses = items_ready_for_booking.collect do |trip|
      trip.legs.collect do |leg|
        {
          leg: {
            attraction: trip.is_attraction?,
            pickup: leg.PickupLocation,
            pickup_date: leg.PickupTOD.to_s(:verbosedate),
            dropoff: leg.DropOffLocation,
            service: leg.service_name.dehtml
          },
          status: leg.status_when_profile_removed
        }
      end
    end.flatten

    statuses.select do |leg|
      leg[:status] &&
        (leg[:status].key?(:fare_increase) ||
          leg[:status].key?(:service_no_longer_available))
    end
  end

  def to_partial_path() 'shared/cart' end

  def total
    return @frozen_total if @frozen_total

    items_ready_for_booking.inject(BigDecimal('0.0')) do |sum, item|
      sum + BigDecimal(item.total_cost.to_s)
    end.to_f
  end

  def total_discount
    return @frozen_discount if @frozen_discount

    items_ready_for_booking.inject(BigDecimal('0.0')) do |sum, item|
      sum + BigDecimal(item.total_discount.to_s)
    end.to_f
  end

  def total_tax
    items_ready_for_booking.inject(BigDecimal('0.0')) do |sum, item|
      sum + BigDecimal(item.total_tax.to_s)
    end.to_f
  end
end
