# A session has one to many trips

class Session
  include HasSite
  include UsesTokens
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  validates_with CheckoutValidator

  class Error < RuntimeError; end
  class BookingError < Error; end
  class PaymentError < Error; end
  class RecalculationError < Error; end
  class ServiceNoLongerAvailableError < Error; end

  attr_reader :attached_config, :cart, :receipt_cart, :user, :site_id,
              :coupon_code
  attr_accessor :cc_result, :points_program, :Name,
      :payment_type, :CCNumber, :CCDetails, :CCExp, :CCZip,
      :Telephone, :Telephone2, :EmailAddr, :terms_conditions,
      :EmailAddr_confirmation, :CCVV, :group_profile, :user_profile,
      :group_id

  tokens terms_and_conditions: 'TERMCONDDESC',
      ccvv_label: 'CCVVDESC',
      credit_card_zip_label: 'CCZIPDESC',
      cutoff_error: 'ERR_CUTOFF',
      telephone_label: 'TELEPHONEDESC',
      telephone2_label: 'ALTTELEPHONEDESC'

  def initialize(site_id)
    @site_id = site_id
    @cart = Cart.new(self)
    start_a_new_trip!
  end

  def alt_telephone_displayed?
    telephone2_label.present?
  end

  def assign_attributes(values)
    values.each {|k, v| send("#{k}=", v) }
  end

  def attach_coupon(coupon_code)
    coupon_code.upcase!
    @group_profile = Profiles.for(@site_id, coupon_code) if coupon_code
    @coupon_code = coupon_code
    @group_id = @group_profile.try(:WebID) if coupon_code
    @cart.items_ready_for_booking.each do |trip|
      begin
        trip.attach_coupon(@group_profile)
      rescue Services::RecalculateFareError
        @cart.delete_item(trip)
      end
    end
  end

  # Coupon codes replace group profile support so we should be able
  # to remove this at some point (AGM 7/22/15)
  def attach_group_profile(profile)
    return(attach_coupon(profile.WebID)) if profile.coupon?

    @coupon_code = nil
    @group_id = profile.try(:WebID)
    @group_profile = profile
    @cart.items.each {|trip| trip.attach_group_profile(profile)}
  end

  def attach_user_profile(profile)
    @user_profile = profile
    @cart.items.each {|trip| trip.attach_user_profile(profile)}
  end

  def attach_itinerary_id
    @itinerary_id = Network::ItineraryID.get(@site_id)
    bookable_trips.each {|item| item.attach_itinerary_id(@itinerary_id)}
    @itinerary_id
  end

  def attached_config=(values)
    @attached_config ||= AttachedConfig.new

    if values.is_a?(AttachedConfig)
      @attached_config = values
    elsif values.is_a?(Hash)
      values.each { |k, v| @attached_config.send("#{k}=", v) }
    end

    @attached_config
  end

  def attached_config?
    @attached_config.present?
  end

  def attraction_service(attraction, params = {})
    params.merge!(
      'Direction' => 'A',
      'ServiceType' => attraction.service_type,
      'PickupKey' => attraction.pickup_key,
      'FlightType' => 'R')
    params[:GroupAlias] = @group_id if @group_id.present?

    Services.for(attraction.site_id, params).try(:first)
  end

  def book!(ccv)
    # Ensure there are no last-minute lead-time issues.
    bookable_trips.each do |trip|
      trip.booking_error = nil

      # I don't check for recalc errors on attractions because the
      # services call wasn't as straightforward.
      # I don't think there's a reason we can't check attractions this
      # way but needed to get this released as a fix.
      if !trip.is_attraction? && !trip.service_still_available?
        trip.booking_error = cutoff_error
        fail XmlCart::BookingError
      end
    end

    # Need to temporarily change primary site in order to
    # support duplicate sites with singular back-ends
    bookable_trips.each { |trip| trip.set_primary_site_to_res_site }
    sites_changed = true

    iid = attach_itinerary_id
    bookable_trips.each { |trip| trip.payment_type = @payment_type }

    # Adam, 2/15/15
    # Currently we don't have a way to send more than 2 legs to the XML Cart
    # inside of one TripID. This affects both multi-leg and cruise reservations
    # in Multiaffiliate. The solution for now is to split legs with more than
    # 3 legs into one-way reservations.
    legs = []
    bookable_trips.each do |trip|
      if trip.legs.length < 3
        legs << trip.legs
      else
        trip.legs.each { |leg| legs << leg }
      end
    end

    # Original code (before installing the multi-leg fix above):
    # legs = bookable_trips.collect { |trip| trip.legs }

    if @payment_type.to_i == 3 # Direct Bill
      success = XmlCart.book(@site_id, legs, nil, nil, nil, nil, iid)
    else
      success = XmlCart.book(
        @site_id, legs, self.CCNumber, self.CCExp,
        self.CCDetails, ccv, iid, self.CCZip
      )
    end
    fail(BookingError) unless success

    bookable_trips.each { |trip| trip.booked! }

  rescue XmlCart::BookingError => e
    raise BookingError, e.to_s

  ensure
    if sites_changed
      bookable_trips.each { |trip| trip.set_primary_site_to_original_value }
    end
  end

  def bookable_trips
    @cart.items_ready_for_booking
  end

  # Yes if any of the cash payment types require a credit card
  def cc_required?
    collected_payment_types.find_all do |pt|
      pt.payment_type.zero?
    end.any?{|pt| pt.cc_required?}
  end

  def ccvv_displayed?
    ccvv_label.present?
  end

  def cczip_displayed?
    credit_card_zip_label.present?
  end

  def CCDetails
    @CCDetails || @user_profile.try(:CCDetails)
  end

  def CCExp
    @CCExp || @user_profile.try(:CCExp)
  end

  def CCZip
    @CCZip || @user_profile.try(:CCZipCode)
  end

  def CCNumber
    @CCNumber || @user_profile.try(:CCNumber)
  end

  def clear_ccvv!
    @CCVV = nil
  end

  def clear_group_profile!
    return(detach_coupon) if @group_profile.try(:coupon?)

    @group_id = nil
    @group_profile = nil
    @cart.items.each {|trip| trip.clear_group_profile! }
  end

  def clear_user_profile!
    @user_profile = nil
    @cart.items.each {|trip| trip.clear_user_profile! }
    clear_contact_and_payment_info!
  end

  def clear_contact_and_payment_info!
    @Name, @CCNumber, @CCDetails, @CCExp, @CCZip, @Telephone, @Telephone2, @EmailAddr, @EmailAddr_confirmation = nil
  end

  def collected_payment_types
    all_payment_types = bookable_trips.collect do |trip|
      trip.collected_payment_types
    end
    all_payment_types.flatten
  end

  def copy_cart_for_receipt
    @receipt_cart = @cart.dup
    @receipt_cart.freeze_total
    @receipt_cart.freeze_group
    @receipt_cart.remove_unbooked_items!
  end

  def coupon_applied?
    @coupon_code.present?
  end

  def coupon_type
    @group_profile.try(:coupon_type)
  end

  def css_overrides
    return {} unless attached_config?
    attached_config.css_overrides
  end

  def current_trip
    @cart.current_item
  end

  def detach_coupon
    @group_profile = nil
    @coupon_code = nil
    @group_id = nil
    @cart.items.each(&:clear_group_profile!)
    @cart.items_ready_for_booking.each(&:recalculate_fares)
  end

  def email_validation?
    reservations = bookable_trips.collect(&:legs).flatten
    reservations.any? {|res| Tokens.for(res.home_site_id, 'PICKUPEMAILCDESC').present?}
  end

  def EmailAddr
    @EmailAddr || @user_profile.try(:EmailAddress)
  end

  # If the current_trip is empty, create a new <new_trip_class>.
  # Otherwise, convert the current_trip (if necessary) to a <new_trip_class>.
  def ensure_current_trip_is_a(new_trip_class)
    if current_trip
      trip = @cart.replace_current_item(new_trip_class.from_trip(current_trip))
    else
      trip = @cart.add_and_make_current_item(new_trip_class.new(@site_id))
    end
    trip.attach_user_profile(@user_profile) if @user_profile.present?
    trip.attach_group_profile(@group_profile) if @group_profile.present?
  end

  def logout_profiles!
    clear_user_profile!
    clear_group_profile!
  end

  def Name
    @Name || @user_profile.try(:Name)
  end

  def points_program=(program_id)
    @points_program = program_id
    bookable_trips.each {|trip| trip.points_program = program_id }
  end

  def replace_checkout_info(params = {})
    errors.clear
    assign_attributes params

    # Add the contact information fields to each res.
    contact_fields = params.find_all do |k,v|
      %w(Name EmailAddr Telephone Telephone2).include?(k)
    end
    bookable_trips.each do |trip|
      trip.legs.each do |leg|
        leg.assign_attributes contact_fields
      end
    end
  end

  def start_a_new_trip!
    trip = @cart.add_and_make_current_item(Trip::OneWay.new(@site_id))
    trip.attach_user_profile(@user_profile) if @user_profile.present?
    trip.attach_group_profile(@group_profile) if @group_profile.present?
  end

  def telephone_displayed?
    telephone_label.present?
  end

  def Telephone
    @Telephone || @user_profile.try(:Telephone)
  end

  def Telephone2
    @Telephone2 || @user_profile.try(:Telephone2)
  end

  def terms_and_conditions?
    terms_and_conditions.present?
  end

  def valid_coupon?(code)
    profile = Profile.for(@site_id, code)
    return false unless profile

    profile.coupon?
  end
end
