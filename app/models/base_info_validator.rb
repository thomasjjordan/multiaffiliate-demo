class BaseInfoValidator < ActiveModel::Validator

  def validate(res)
    res.validates_presence_of :airport, message: "This field is required"
    res.validates_presence_of :FlightType, message: "This field is required"
    res.validates_presence_of :location_key, message: "This field is required"


    if res.FlightTOD.blank? && res.incomplete_FlightTOD_date.blank?
      res.errors.add :FlightTOD_date, "This field is required"
    end

    if res.FlightTOD.blank? && res.incomplete_FlightTOD_time.blank?
      res.errors.add :FlightTOD_time, "This field is required"
    end

    if res.additional_required_base_fields &&
       res.additional_required_base_fields.is_a?(Array)
      res.additional_required_base_fields.each do |field|
        res.validates_presence_of field.to_sym, message: "This field is required"
      end
    end

    if res.total_passengers <= 0
      res.errors.add :passengers, "There are no passengers selected."
    end
  end
end
