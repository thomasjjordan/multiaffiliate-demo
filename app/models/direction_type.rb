# Represents things like "Airport -> Cruise terminal" or "Hotel or Address -> Airport"
# A type is represented with the characters A, C, H, and t (ex. AtC):
#   A - Airport
#   C - Cruise terminal
#   H - Hotel or address
#   t - delimiter ('to')
#
# ex. DirectionType.new('AtC')
class DirectionType
  include HasSite
  include UsesTokens

  attr_reader :type

  tokens 'MA_HOME_' => {
            A: {'DT_AIRPORT' => 'the airport'},
            C: {'DT_CRUISE' => 'a cruise terminal'},
            H: {'DT_LOC' => 'a hotel or address'},
            t: {'DT_TO' => ' -> '} }

  def initialize(site_id, type)
    @site_id, @type = site_id, type
  end

  def arrival?
    direction == 'A'
  end

  def departure?
    direction == 'D'
  end

  def direction
    @type.first.in?(%w{A C}) ? 'A' : 'D'
  end

  def dropoff_label
    send(@type[2])
  end

  def label
    @type.gsub(/[AtCH]/) {|match| send(match) }
  end

  def pickup_label
    send(@type[0])
  end

  def to_operator_label
    send(@type[1])
  end
end