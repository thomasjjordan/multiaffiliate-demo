module ProfileHelper
	def iframe_tag(src)
	  tag :iframe, scrolling: 'no', allowtransparency: 'true', marginwidth: '0',
	      marginheight: '0', vspace: '0', hspace: '0', width: '820',
	      src: src, frameborder: '0', height: '600', frame_id: 'profile_iframe'
	end

	def profile_url(type, options = {})
		host = Rails.env.development? ?
		    "#{request.protocol}#{SiteServer.server_for(current_site_id)}" : ''
		url = "#{host}/a/profiles/#{type.to_s}?site_id=#{current_site_id}"

		if type == :new
			first_name, last_name = current_session.Name.to_s.split(' ', 2)
			url << "&defaults[FirstName]=#{first_name}&defaults[LastName]=#{last_name}&" +
					"defaults[Telephone]=#{current_session.Telephone}&" +
					"defaults[Telephone2]=#{current_session.Telephone2}&" +
					"defaults[EmailAddress]=#{current_session.EmailAddr}&" +
					"defaults[CCNumber]=#{current_session.CCNumber}&" +
					"defaults[CCDetails]=#{current_session.CCDetails}&" +
					"defaults[CCZipCode]=#{current_session.CCZip}"
			if current_session.CCExp.is_a?(Time)
				url << "&defaults[CCExp]=#{current_session.CCExp.strftime('%m-%d-%Y')}"
			end
		end
		options.each {|k,v| url << "&#{k}=#{v}" }
		URI.escape(url)
	end
end
