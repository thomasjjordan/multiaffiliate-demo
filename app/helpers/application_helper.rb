require 'sanitize'

module ApplicationHelper
  def current_theme_css
    current_theme.to_css(current_session.css_overrides)
  end

  def darker_primary_css
    bgcolor = Paleta::Color.new(:hex,
        current_theme.primary_background_color.gsub(/#/,''))
    color = Paleta::Color.new(:hex,
        current_theme.primary_color.gsub(/#/,''))

    <<-eos
      .darker-primary {
        background-color: ##{bgcolor.darken!(15).hex};
        color: ##{color.lighten!(20).hex};
      }
    eos
  end

  def cart_link
    unless cart.empty?
      link_to fa_icon('shopping-cart') + cart_label, cart_url, class: 'cart-link'
    end
  end

  def dehtml(string)
    string.dehtml rescue nil
  end

  # type = DirectionType
  def direction_type_icon(dirtype, options = {})
    icons = {
      'AA' => 'plane rotate-90',
      'AD' => 'plane',
      'C' => 'anchor fw',
      'H' => 'building fw' }

    type = dirtype.arrival? ? dirtype.type[0] : dirtype.type[2]
    type += (dirtype.arrival? ? 'A' : 'D') if type == 'A'

    class_name = icons[type]
    class_name << " fw" if options[:fixed_width]
    fa_icon(class_name)
  end

  def date_format(incoming_date)
    SiteInfo.date_format(current_site_id, incoming_date)
  end

  def time_format
    SiteInfo.time_format(current_site_id)
  end

  def general_analytics
    Tokens.for(current_site_id, 'MA_GLOBAL_ANALYTICS', noreplace: true, no_imagefix: true).html_safe
  end

  def google_analytics_page_name
    name = "/reservations"
    name += "/#{current_group_id}" if current_group_id.present?

    clean_url = url_for().gsub(/\/a\/r\/multiaff[\d.]+(_demo)?/, '')
    clean_url = clean_url.gsub(/^\/[\w\d]+/, '') # Remove site id.
    name += {
      '/reservations' => '/welcome'
    }[clean_url] || clean_url
  end

  def leg_summary_flighttod_prefix(leg_index)
    if current_trip.cruise?
      if leg_index == 2
        type = current_trip.base_legs[leg_index].DropOffType
        send("leg_summary_flighttod_cruise_2_#{type.downcase}")
      else
        send("leg_summary_flighttod_cruise_#{leg_index}")
      end
    else
      leg_summary_flighttod_default
    end
  end

  def nice_date_display(time, only_date = false)
    ret_str = time.strftime("%A %b ") + time.strftime("%d").to_i.to_s
    ret_str += time.strftime(" at ") + nice_time_display(time) unless only_date
    ret_str
  end

  def nice_time_display(time)
    if time_format == :"24_hour"
      time.strftime("%H").to_i.to_s +
      time.strftime(":%M")
    else
      time.strftime("%I").to_i.to_s +
      time.strftime(":%M%p").downcase
    end
  end

  def notice_box
    notice_class = 'alert'
    notice_class << ' alert-danger' if flash[:error]
    notice_class << ' alert-info' if flash.notice
    notice_class << ' hide' unless flash[:error] || flash.notice

    content_tag(:div, id: 'notice_box', class: notice_class) do
      (flash[:error] || flash.notice).try(:html_safe)
    end.html_safe
  end

  def remove_crap_html!(text)
    text.gsub!(/(<br\/?>)*\s*$/, '')
    text.gsub!(/^\s*(<br\/?>)*/, '')
    text.gsub!(/<\/?center>/i, '')
    text
  end

  def site_body_class
    params[:custom_body_class]
  end

  def site_stylesheet
    unless custom_stylesheet.blank? || custom_stylesheet == 'DEFAULT'
      stylesheet_link_tag custom_stylesheet
    end
  end

  # The problem is that our customers are configuring labels based on
  # other systems and so we have a mismatch of gray and black labels.
  # This will likely be an ever-changing method in order to best
  # support what our customers want.
  def strip_font_tags(text)
    whitelist = Sanitize::Config::RELAXED.deep_dup
    whitelist[:elements].push 'font'
    whitelist[:attributes]["font"] = ["size"]
    whitelist[:attributes]['a'] << 'onclick'
    whitelist[:protocols]['a'] = {'href' => ['javascript', 'http', 'https']}
    Sanitize.clean(text, whitelist).html_safe
  end

  def summary_icon(res, options = {})
    if current_trip.cruise?
      presenter = CruiseReservationsPresenter.new(current_session, self)
      presenter.summary_icon(res)
    else
      dir_type = DirectionType.new(current_site_id, res.direction_type)
      direction_type_icon(dir_type, options)
    end
  end

  def tripit_enabled?
    tripit_enabled.present?
  end

  def universal_google_analytics?
    Network::Config.universal_google_analytics?(current_site_id)
  end
end
