module ServicesHelper
  # A warning message to the user alerting them that adding their items to
  # the cart means they can't edit them anymore.
  def add_to_cart_warning
    if add_to_cart_msg(no_span: true).present?
      content_tag :div, add_to_cart_msg, class: 'add-to-cart-warning'
    end
  end

  def additional_charges_label(site_id)
    label = dehtml(site_token(site_id, 'MA_SVCS_CAP_ADTLCHARGES'))
    label.blank? ? 'This time incurs additional charges:' : label
  end

  def address_label(res)
    site_token(res.home_site_id, 'ROADDRESSDESC', 'Address').dehtml
  end

  def address3_label(res)
    site_token(res.home_site_id, 'ADDRESSDESC3').dehtml
  end

  def adtl_gratuity_label(res)
    site_token(res.home_site_id, 'ADTLUSERGRATDESC', 'Additional Gratuity').dehtml
  end

  def airline_label(res)
    dehtml(site_token(res.home_site_id, 'AIRLINESDESC'))
  end

  def available_airlines(res)
    airlines = [' ']

    if current_trip.cruise?
      presenter = CruiseReservationsPresenter.new(current_session, self)
      airlines + presenter.available_airlines(res)
    else
      airlines + res.available_airlines.collect(&:name)
    end

  end

  def cruise_label(res)
    dehtml(site_token(res.home_site_id, 'MA_SVCS_CAP_SHIP', 'Ship name'))
  end

  def dropoff_time_label(site_id)
    dehtml(site_token(site_id, "MAX_SVCS_DO_TIME_PREFIX", 'Dropoff:'))
  end

	def fare_specific_message(res)
		message = res.location.try(:LocationStr)
    message.present? ? content_tag(:p, message).html_safe : ''
  end

  def flight_city_label(res)
    label = res.arrival? ?
      [raw_network_flight_city_a_label, raw_flight_city_a_label].find(&:present?) :
      [raw_network_flight_city_d_label, raw_flight_city_d_label].find(&:present?)
    label.html_safe
  end

  def flight_num_label(res)
    site_token(res.home_site_id, "#{res.Direction.upcase}FLIGHTNUMBERDESC").dehtml
  end

  def gratuity_enabled?(res)
    !gratuity_label(res).blank?
  end

  def gratuity_included_message(res)
    site_token(res.home_site_id, 'MA_SVCS_MSG_GRAT_INCLUDED',
        'A(n) %GRAT_PERCENT% gratuity of %GRAT_AMOUNT% is included in your fare.',
        noreplace: true)
  end

  def gratuity_label(res)
    dehtml(site_token(res.home_site_id, 'ALTUSERGRATDESC'))
  end

  def location_address(f, res)
    if res.location.readonly_address?
      res.location_addr
    else
      class_name = "form-control #{error_class(res, :location_addr)}"
      (field_error_label(res, :location_addr).to_s +
        f.text_area(res.location_addr_field,
            size: '25x3', class: class_name,
            placeholder: address_label(res))).html_safe
    end
  end

  def leg_header(leg)
    label = send("leg_header_#{current_trip.trip_type}_#{leg.Direction.downcase}")
    label = (current_trip.legs.index(leg)+1).ordinalize + ' ' + label if current_trip.trip_type == :multi
    content_tag :h3, label
  end

  def location_details(f, res)
    return nil unless address3_label(res).present?

    if res.location.readonly_address?
      res.location_details
    else
      f.text_field res.location_details_field, maxlength: 10,
          class: 'form-control loc-details',
          placeholder: address3_label(res)
    end
  end

  def location_header(res)
    res.arrival? ? location_header_a : location_header_d
  end

  def location_zip(f, res)
    if res.location.readonly_zip?
      res.location_zip
    else
      f.text_field res.location_zip_field, maxlength: 10,
          class: 'form-control loc-zip',
            placeholder: zip_label(res)
    end
  end

  def off_peak_label(site_id)
    dehtml(site_token(site_id, 'OFFSURCHARGECOLTEXT'))
  end

  def pickup_extras_header(site_id)
    site_token(site_id, 'PECOLTEXT', 'Extras').dehtml.html_safe
  end

  def pickup_extra_selected?(extras_list, this_extra, set_extra)
    # If this extra is the set extra, it's selected.
    if this_extra.type == set_extra
      return true

    # If the set_extra is not in the extras_list, select the first extra
    elsif !extras_list.find{|extra| extra.type == set_extra} && this_extra.type == extras_list[0].type
      return true

    else
      return false
    end
  end

  def pickup_time_label(site_id)
    site_token(site_id, "PUTIMECOLTEXT").html_safe
  end

  def pickup_time_message(site_id, direction)
    remove_crap_html!(site_token(site_id, "TIMESEL#{direction}")).html_safe
  end

  def pud_directions_label(res)
    dehtml(site_token(res.home_site_id, 'ADDRNOTESDESC'))
  end

  def service_description(service, res)
    remove_crap_html!(service.description.fix_image_urls(res.home_site_id)).html_safe
    #desc.gsub(/<br\/?>/i, '').html_safe
  end

  def service_fare(service)
    if service.fare_available?
      fare = number_to_currency(service.fare)
      parts = fare.split('.')

      html = ''
      if service.group_discount > 0.0
        original_fare = number_to_currency(service.fare.to_f + service.group_discount)
        html = "<span class='original-fare'>#{original_fare}</span>"
      end
      html += "<span class='fare-major'>#{parts[0]}</span><span class='fare-minor'>.#{parts[1]}</span>"
      html.html_safe
    else
      no_fare
    end
  end

  def service_group_discount_message(service)
    msg = service_group_discount_label(nil, noreplace: true).gsub(/%GROUPDISC%/, number_to_currency(service.group_discount))
    msg.gsub(/%GROUPALIAS%/, current_group_alias.to_s).html_safe
  end

  #TODO: Modify for multi-leg
  def services_header(leg)
    if current_trip.roundtrip?
      header = leg.departure? ? roundtrip_d_services_h1 : roundtrip_a_services_h1
    else
      header = oneway_services_h1
    end
    content_tag(:h1, header.html_safe).html_safe
  end

  def service_table_header(leg)
    header = site_token(leg.home_site_id, "SERVICESEL#{leg.Direction}")
    remove_crap_html!(header)
    content_tag(:p, header.html_safe)
  end

  def short_notice_label(site_id)
    dehtml(site_token(site_id, 'SNSURCHARGECOLTEXT'))
  end

  def show_airline?(res)
    airline_label(res).present? && flight_num_label(res).present?
  end

  def show_flight_city?(res)
    flight_city_label(res).present?
  end

  def show_flight_info?(res)
    res.capture_flight_info? &&
      !(airline_label(res).blank? && flight_num_label(res).blank? &&
        flight_city_label(res).blank? && res.departure?)
  end

  def show_flight_number?(res)
    !flight_num_label(res).blank? &&
          !Airports.airport_for(res.home_site_id, res.airport_key).cruise_terminal?
  end

  def show_pud_directions?(res)
    pud_directions_label(res).present?
  end

  def show_ship?(res)
    res.capture_ship?
  end

  def zip_label(res)
    dehtml(site_token(res.home_site_id, "PICKUPZIPDESC", 'Zip code'))
  end
end
