module ColorsHelper
  include Sass::Script::Functions

  def subsection_alt_bgcolor
    color = current_theme.subsection_background_color.gsub(/#/,'')
    color = Paleta::Color.new(:hex, color).darken!(20)
    "##{color}"
  end

  def button_dark_bgcolor
    color = current_theme.button_background_color.gsub(/#/,'')
    color = Paleta::Color.new(:hex, color).darken!(20)
    "##{color}"
  end

  def button_light_bgcolor
    color = current_theme.button_background_color.gsub(/#/,'')
    color = Paleta::Color.new(:hex, color).lighten!(20)
    "##{color}"
  end

  def themed_button
    bgcolordark = button_dark_bgcolor
    bgcolorlight = button_light_bgcolor
    bgcolor = current_theme.button_background_color
    color = current_theme.button_color

    %{
    .btn-hudson.active {
      color: rgba(255, 255, 255, 0.75);
    }
    .btn-hudson {
      color: #{color };
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
      background-color: #{bgcolor };
      background-image: -moz-linear-gradient(top, #{bgcolorlight }, #{bgcolordark });
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#{bgcolorlight }), to(#{bgcolordark }));
      background-image: -webkit-linear-gradient(top, #{bgcolorlight }, #{bgcolordark });
      background-image: -o-linear-gradient(top, #{bgcolorlight }, #{bgcolordark });
      background-image: linear-gradient(to bottom, #{bgcolorlight }, #{bgcolordark });
      background-repeat: repeat-x;
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{bgcolorlight },', endColorstr='#{bgcolordark }', GradientType=0);
      border-color: #{bgcolordark } #{bgcolordark } #{color };
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      *background-color: #{bgcolordark };
      /* Darken IE7 buttons by default so they stand out more given they won't have borders */

      filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
    }
    .btn-hudson:hover,
    .btn-hudson:focus,
    .btn-hudson:active,
    .btn-hudson.active,
    .btn-hudson.disabled,
    .btn-hudson[disabled] {
      color: #ffffff;
      background-color: #{bgcolor };
      *background-color: #{bgcolor };
    }
    .btn-hudson:active,
    .btn-hudson.active {
      background-color: #{bgcolordark } ;
    } }.html_safe
  end
end
