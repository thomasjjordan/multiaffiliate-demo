module ConfigurationHelper
  def attraction_airport_options(attraction)
    options_from_collection_for_select(attraction.unattached_airports,
                                       :Code,
                                       :Description)
  end

  def logo_image
    if config_logo(no_span: true).present?
      image_tag(config_logo(no_span: true), id: 'branding')
    elsif site_logo(no_span: true).present?
      logo_url = site_logo.extract_image_url(current_network.site_id)
      image_tag(logo_url, id: 'branding')
    end
  end

  def site_icon(site)
    if site.is_a? Network::Site
      content_tag :div, fa_icon('car') + " transportation", class: 'site-type transpo'
    else
      content_tag :div, fa_icon('ticket') + 'attraction', class: 'site-type attract'
    end
  end
end
