require 'ostruct'

module ReservationsHelper
  def leg(index, leg)
    if leg.airport.present?
      airport = NetworkAirport.find(current_site_id, leg.airport)
    end
    airports = NetworkAirport.all(current_site_id, current_group_profile)
    airport = airports.first if airports.length == 1

    if airport && leg.PrimarySite.present?
      region = airport.service_areas.find do |sa|
        sa.provider_site_id == leg.PrimarySite
      end
    elsif airport && airport.service_areas.length == 1
      region = airport.service_areas.first
    end

    react_component 'Leg',
                    direction: leg.Direction,
                    region: region,
                    index: index,
                    airport: {
                      current: airport,
                      field_type: airport_autocomplete_field_type,
                      label: airport_labels(index, leg.Direction) +
                             field_error_label(leg, :airport),
                      airports: airports,
                      placeholder: airport_placeholder,
                      tabindex: 0 #airport_trip_index_indicator(index)
                    },
                    location: {
                      current: leg.location,
                      label: location_labels(index, leg.Direction) +
                             field_error_label(leg, :location_key),
                      placeholder: loc_placeholder
                    },
                    urls: {
                      initial_locations: initial_locations_path,
                      location_search: locations_path,
                      roundtrip: roundtrip_path
                    }
  end

  def airport_autocomplete_field_type
    config = airport_autocomplete_type(no_span: true)
    config.to_i == 0 ? :search : :list
  end

  def airport_labels(leg_index, current_direction)
    labels = { 'D' => airport_label_d, 'A' => airport_label_a }
    directional_elements(leg_index, :label, labels, current_direction)
  end

  def bag_count_label(site_id)
    site_token(site_id, 'BAGCOUNTDESC')
  end

  # Provides inline styles for the direction button if the current
  # theme colors necessitate it.
  def direction_contrast_border
    if current_theme.button_background_color == current_theme.subsection_background_color
      return "style='border: 1px solid #{current_theme.subsection_color}'".html_safe
    end
  end

  def direction_label_with_icons(dir_type)
    dir = current_trip.direction_types.find {|dt| dt.type == dir_type}

    header = content_tag :div, class: 'direction-type-diagram pull-left hudson-subsection ' do
        direction_type_icon(dir, fixed_width: true)
    end.html_safe
    pickup_phrase = ("from " + dir.pickup_label).html_safe
    dropoff_phrase = ("to " + dir.dropoff_label).html_safe
    description = content_tag :div, class: 'direction-type-desc' do
      (dir.arrival? ?
          "#{pickup_phrase}<br/>#{dropoff_phrase}" :
          "#{dropoff_phrase}<br/>#{pickup_phrase}").html_safe
    end.html_safe

    "#{header}#{description}".html_safe
  end

  # Options:
  #   except - An array of direction types to not include.
  def direction_types(options = {})
    types = current_trip.direction_types
    if options[:except]
      types.delete_if do |dir|
        dir.type[0].in?(options[:except]) || dir.type[2].in?(options[:except])
      end
    end
    types
  end

  def error_class(leg, attribute)
    'has-error' if leg.errors[attribute].any?
  end

  def field_error_label(leg, attribute)
    if leg.errors[attribute].any?
      content_tag :label, leg.errors[attribute].join('<br/>').html_safe, class: 'error-label'
    end
  end

  # Produce two labels, one for ports and the other for airports.
  def flight_date_labels(leg_index, leg)
    current_type = 'A'

    labels = {
        'A' => flight_date_label_a,
        'CA' => flight_date_label_c_a,
        'CD' => flight_date_label_c_d }
    %w(A CA CD).collect do |type|
      content_tag('label', labels[type].html_safe,
          data: {leg: leg_index, type: type}, class: 'flight-date-label',
          style: "display: " + (current_type == type ? 'block' : 'none'))
    end.join.html_safe
  end

  # Produce two labels, one for ports and the other for airports.
  def flight_time_labels(leg_index, leg)
    current_type = 'A'

    labels = {
        'A' => flight_time_label_a,
        'CA' => flight_time_label_c_a,
        'CD' => flight_time_label_c_d }
    %w(A CA CD).collect do |type|
      content_tag('label', labels[type].html_safe,
          data: {leg: leg_index, type: type},
          style: "display: " + (current_type == type ? 'block' : 'none'))
    end.join.html_safe
  end

  def direction_trip_index_indicator(leg_index)
    if leg_index == 0
      tabindex = 4
    else
      tabindex = 12
    end
    tabindex
  end

  def airport_trip_index_indicator(leg_index)
    if leg_index == 0
      tabindex = 5
    else
      tabindex = 13
    end
    tabindex
  end

  def region_trip_index_indicator(leg_index)
    if leg_index == 0
      tabindex = 6
    else
      tabindex = 14
    end
    tabindex
  end

  def location_trip_index_indicator(leg_index)
    if leg_index == 0
      tabindex = 7
    else
      tabindex = 15
    end
    tabindex
  end

  def flightTOD_trip_index_indicator(leg_index)
    if leg_index == 0
      tabindex = 8
    else
      tabindex = 16
    end
    tabindex
  end

  def trip_time_index_indicator(index)
    if index == 0
      tabindex = 9
    else
      tabindex = 17
    end
    tabindex
  end

  def flight_type_trip_index_indicator(rt_leg,leg_index)
    if !rt_leg.present? && leg_index == 0
      tabindex = 10
    else
      tabindex = 18
    end
    tabindex
  end

  def group_profile_logo
    if current_group_profile.Logo.present?
      current_group_profile.Logo
        .fix_image_urls(current_site_id)
        .gsub(/<img /, '<img class="img-responsive" ')
        .html_safe
     end
  end

  def home_header
    content_tag(:h1, home_h1) + content_tag(:h2, home_h2)
  end

  def icon_cruise
    fa_icon 'anchor'
  end

  def icon_oneway
    fa_icon 'long-arrow-right'
  end

  def icon_multi
    fa_icon 'list'
  end

  def icon_roundtrip
    fa_icon 'exchange'
  end

  def leg_container_class(leg, hide_leg = false)
    className = 'leg-container'
    className << ' hide' if hide_leg
    className
  end

  def leg_headers(leg_index, current_direction, trip_type)
    return if trip_type == :roundtrip

    labels = {}
    %w(D A).each do |dir|
      label = send("leg_header_#{trip_type}_#{dir.downcase}")
      label = (leg_index+1).ordinalize + ' ' + label if trip_type == :multi
      labels[dir] = label
    end
    directional_elements(leg_index, :h3, labels, current_direction)
  end

  def location_labels(leg_index, current_direction)
    labels = { 'D' => loc_label_d, 'A' => loc_label_a }
    directional_elements(leg_index, :label, labels, current_direction)
  end

  def profile_welcome_message(profile)
    message = profile.WEBLogonStr.blank? ? default_profile_message :
        profile.WEBLogonStr
    message.fix_image_urls(current_site_id).html_safe
  end

  def reservation_page(type, data = {}, &block)
    content_tag :div, class: "page",
        data: data.merge({page: 'reservations', type: type}) do
      login_form = render(partial: 'reservations/login_modal')
      login_form + home_header + notice_box + capture(&block)
    end
  end

  def service_extra_label(f, extra)
    popover_content = extract_popup_text(extra.label)
    if popover_content
      data_hash = {
          content: popover_content,
          toggle: 'popover', placement: 'auto bottom',
          container: 'body', trigger: 'hover click',
          html: true }
    end

    f.label extra.field_name,
        label_without_popup_text(extra.label)
          .fix_image_urls(f.object.home_site_id).html_safe,
        data: data_hash
  end

  def date_selector(leg, index, field = :FlightTOD)
    # Where should the date come from?
    if [:FlightTOD, :PickupTOD].include?(field.to_sym) &&
       leg.send("incomplete_#{field}_date").present?
      date = leg.send("incomplete_#{field}_date")
    elsif leg.send(field).present?
      date = leg.send(field)
    end

    error_classname = error_class(leg, "#{field}_date")
    field_options = {
      data: { 'date-format' => custom_date_format.blank? ? "mm/dd/yyyy" : custom_date_format.downcase},
      # readonly: true,
      class: "input-small form-control datepicker #{error_classname}"
    }
    value = date_format(date) if date

    text_field_tag "trip[#{index}][#{field}]", value, field_options
  end

  def time_selector(leg, index, field = :FlightTOD)
    # Where should the time come from?
    if [:FlightTOD, :PickupTOD].include?(field.to_sym) &&
       leg.send("incomplete_#{field}_time").present?
      time = leg.send("incomplete_#{field}_time")
    elsif leg.send(field).present?
      time = leg.send(field)
    end

    field_options = {
      ampm:   time_format == :"12_hour" ? true : false,
      minute_step: 5,
      ignore_date: true
    }
    field_options[:include_blank] = time.blank?
    field_options[:default] = { hour: time.hour, minute: time.min } if time

    error_classname = error_class(leg, "#{field}_time")
    time_select("trip[#{index}]",
                field,
                field_options,
                class: "form-control time #{error_classname}")
  end

  def trip_type_selector(current_trip_type)
    trip_types = []
    trip_types << :oneway if oneway_enabled?
    trip_types << :roundtrip if roundtrip_enabled?
    trip_types << :multi if multi_leg_enabled?
    trip_types << :cruise if cruise_enabled?(current_trip_type)

    if trip_types.length == 1 ||
       !Network::Config.show_trip_types_bar?(current_site_id, current_trip_type)
      return nil
    end

    selector = content_tag :ul, id: 'trip-type-selector', class: %w(nav nav-pills) do

      trip_types.collect do |type|
        is_active = (current_trip_type == type)

        content_tag :li, class: (is_active ? 'active' : '') do
          value = send("icon_#{type}") + send("trip_label_#{type}")
          content_tag :a, value,
              href: url_for(type),
              id: type,
              class: (is_active ? 'hudson-button' : ''),
              tabindex: -1
        end
      end.join.html_safe
    end.html_safe
    selector + tag(:hr)
  end

  private
    # Produces two similar HTML elements where only one is intended to be
    # visible at a time, based on the current leg's direction.
    # Usage example:
    #   directional_elements(0, :h3,
    #       {'D' => 'Departing', 'A' => 'Arriving'}, 'D')
    def directional_elements(leg_index, element_type, labels_hsh, current_dir)
      %w(D A).collect do |dir|
        content_tag(element_type, labels_hsh[dir].html_safe,
            data: {leg: leg_index, direction: dir}, class: 'directional',
            style: "display: " + (current_dir == dir ? 'block' : 'none'))
      end.join.html_safe
    end

    # See text for #remove_popup_text_and_html for description of res popups.
    # This method extracts the hover text.
    def extract_popup_text(label)
      matches = label.match(/<div><center>(.*?)<\/center><\/div>/)
      if !matches
        matches = label.match(/<div>(.*?)<\/div>/)
      end
      matches ? matches[1] : nil
    end

    # Sometimes a location category is configured with certain HTML to allow it
    # to have custom hover text in the main res system. We need to extract the
    # actual category label from it. Also, remove any other html.
    # An example of the popup html is:
    # <a class=boxpopup href=\"javascript: void(0)\">Select Street<span><div><center>House/Building number not required at this time.</center></div></span></a>"
    def remove_popup_text_and_html(label)
      if label =~ /boxpopup/
        text = label.match(/">(.*?)<span.*?><div>/)[1] rescue nil
      end
      dehtml(text || label)
    end

    def label_without_popup_text(label)
      # If I find an anchor with "boxpopup" class:
      if label =~ /(.*?)<a[^\/]*?class=.?boxpopup.?.*?>(.*?)<span[^\/]*?><div>(.*?)<\/div><\/span><\/a>(.*)/
        "#$1<span class='popover-trigger'>#$2</span>#$4"
      else
        label
      end
    end
end
