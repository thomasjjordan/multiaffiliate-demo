module CartHelper

  def add_res_link
    # If we have attached config for the most recent item in the cart.
    if current_attached_config && current_attached_config.new_url.present?
      url = current_attached_config.new_url
      return link_to cart_add_res_label, '#',
                     class: 'add-res btn btn-link',
                     onclick: "window.top.location.href = '#{url}';"
    end

    # If the customer has defined a custom link, we want to update the parent.
    if cart_add_res_destination.present?
      return link_to cart_add_res_label, '#',
                     class: 'add-res btn btn-link',
                     onclick: "window.top.location.href = '#{cart_add_res_destination}';"
    end

    # Otherwise
    link_to cart_add_res_label, add_res_url,
            class: 'add-res btn btn-link'
  end

  def add_res_url
    # URL for booking a new trip of the most recently added type.
    if cart.items_ready_for_booking.length > 0
      trip_type = cart.items_ready_for_booking.last.trip_type
      return send("#{trip_type}_url")
    end

    root_url
  end

  def allow_coupon_removal?
    current_session.coupon_applied? &&
      current_session.coupon_type != :immutable_coupon
  end

  # Coupons are allowed if the coupon label is set and we don't have a profile
  # loaded that has disabled coupons.
  def allow_coupons?
    apply_coupon_label(no_span: true).present? &&
      (current_session.group_profile.blank? ||
        current_session.group_profile.coupon_type == :coupon)
  end

  def attractions
    cart.items_ready_for_booking.collect(&:available_attractions).flatten.uniq
  end

  def attractions_available?
    cart.items_ready_for_booking.any? &:attractions_available?
  end

  # Checks if any of a leg's FlightCity can be associated with
  # a valid network airport (so we can try to sell service to the customer
  # in that region)
  def can_book_travel_to_flight_city?(leg)
    airports = current_network.airports.collect &:Code

    if (airport = airport_from_flight_city(leg.FlightCity)) && airports.include?(airport)
      return true
    end

    false
  end

  def cart_flight_city_caption(leg)
    label = send("cart_flight_city_cap_#{leg.Direction.downcase}", {}, noreplace: true)
    label.gsub(/%FLIGHTCITY%/, leg.FlightCity).html_safe
  end

  def credit_card_message
    # Any of the cash payment types have a message?
    payment = collected_payment_types.find{|pt| pt.payment_type == 0}
    payment ?  content_tag(:p, payment.message.html_safe) : nil
  end

  # Some analytics will send duplicated data if the user reloads the receipt
  # page. This method helps ensure they are only displayed once.
  def display_receipt_analytics?(itinerary_id)
    @display_receipt_analytics ||= begin
      times_displayed = session[itinerary_id].to_i
      session[itinerary_id] = times_displayed + 1
      times_displayed == 0
    end
  end

  def email_sub_checked?(key)
    # An email subscription should be checked off by default or if the user
    # explicitly selected it. We need to catch the case where the user
    # unselected it and make sure we don't re-select it on them.
    flash[:mailing_list] == nil || flash[:mailing_list][key] == '1'
  end

  def exclusive_programs
    current_user_profile.enrolled_programs.find_all {|p| p.earning_type == :exclusive}
  end

  def mask_number(number)
    number.to_s.size < 5 ? number.to_s : (('*' * number.to_s[0..-5].length) + number.to_s[-4..-1])
  end

  def multiple_exclusive_programs?
    exclusive_programs.length > 1
  end

  def offer_multiple_payment_types?
    [offer_cc?, offer_cash?, offer_db?].find_all{|a|a}.length > 1
  end

  def payment_type(type, label, is_default = false)
    active = (current_session.payment_type == type ||
        (current_session.payment_type.blank? && is_default))
    content_tag :li, class: "#{'active' if active}" do
      content_tag :a, label, id: "pt-#{type}",
          class: "#{'hudson-button' if active}", data: {type: type}
    end.html_safe
  end

  def programs_earning_points
    programs = current_user_profile.enrolled_programs
        .find_all {|p| p.earning_type == :always}
        .collect{|p| content_tag :p, p.name}
        .join.html_safe
  end

  def receipt?
    controller_name == 'cart' && action_name == 'receipt'
  end

  def receipt_analytics(cart)
    content = Tokens.for(current_site_id, 'MA_RECEIPT_ANALYTICS',
        :noreplace => true, :no_imagefix => true)
    unless content.blank?
      content = content.gsub(/%TOTALFARE%/, number_with_precision(cart.total.to_s, precision: 2))
      content = content.gsub(/%(IID|ID)%/, cart.items[0].legs[0].ItineraryID.to_s)
      content = content.gsub(/%OP_BREAKDOWN%/, revenue_breakdown(cart))
    end
    content.html_safe
  end

  def resid_list(cart)
    resids = []
    cart.items.each do |trip|
      trip.legs.each {|leg| resids << leg.WResID }
    end
    resids.join(",")
  end

  def resid_list_for_email(cart, email, options = {})
    resids = []
    cart.items.each do |trip|
      trip.legs.each {|leg| resids << leg.WResID if leg.EmailAddr == email }
    end
    if options[:nice]
      return resids.join(", ")
    else
      return resids.join(",")
    end
  end

  def revenue_breakdown(cart)
    legs = cart.items.collect(&:legs).flatten.compact
    legs.collect {|leg| "#{leg.home_site_id}:#{number_with_precision(leg.Fare, precision: 2)}"}.join('|')
  end

  def cart_service_group_discount_message(leg, cart)
    msg = service_group_discount_label(nil, noreplace: true).gsub(/%GROUPDISC%/, number_to_currency(leg.GroupDisc))
    msg.gsub(/%GROUPALIAS%/, (cart.group_alias || '')).html_safe
  end

  def hide_attraction_attract?
    cart_hide_attractions_attract.to_i == 1
  end

  def show_book_another_link?
    hide_book_another_link.to_i != 1
  end

  def show_email_validation_field?
    current_session.email_validation?
  end

  def show_flight_city_marketing?(leg)
    show_provider_links? && can_book_travel_to_flight_city?(leg)
  end

  def show_points_info?
    current_user_profile &&
        !(current_user_profile.enrolled_programs.nil? ||
            current_user_profile.enrolled_programs.empty?)
  end

  def show_preferred_rider_button?
    show_preferred_rider_button.to_i == 1
  end

  # Show provider links if there is only one primary site represented in the cart.
  # If there are more than one site represented the customer may have already
  # added travel to/from their flight city.
  def show_provider_links?
    bookable_trips.collect(&:primary_sites).flatten.uniq.length == 1
  end

  def show_service_discount_row?(leg)
    leg.GroupDisc.to_f > 0.0
  end

  def show_time_in_cart?(trip)
    !trip.is_attraction? ||
      (trip.is_attraction? && trip.legs[0].attraction_display_time)
  end

  def trips_have_multiple_email_addresses?(trips)
    unique_email_addresses(trips).length > 1
  end

  # The number of rows a trip will generate in the cart table.
  def trip_table_row_count(trip)
    trip.legs.reduce(0) do |row_count, leg|
      row_count + 1 + [
        show_service_discount_row?(leg),
        show_flight_city_marketing?(leg) ].delete_if(&:!).size
    end
  end
end
