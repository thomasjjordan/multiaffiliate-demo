module CruiseReservationsHelper
  def cruise_h3(index)
    content_name = "leg_header_cruise_#{index}"
    return if send(content_name, no_span: true).blank?

    icon = summary_icon(current_trip.base_legs[index], fixed_width: true)
    icon_tag = content_tag :div,
                           icon,
                           class: 'circle-icon hudson-primary'
    content_tag :h3, icon_tag + send(content_name)
  end

  def cruise_section_header(index)
    content_name = "cruise_sect_head_#{index}"
    return if send(content_name, no_span: true).blank?

    content_tag :p, send(content_name)
  end
end
