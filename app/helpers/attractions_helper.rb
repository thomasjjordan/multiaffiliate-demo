module AttractionsHelper
  def attraction_title(attraction)
    content_tag(:h3, class: 'hudson-primary') do
      url = attraction_details_url(key: attraction.code,
                                   attraction_site_id: attraction.site_id)
      link_to_unless(!attraction.has_details?,
                     attraction.name,
                     url,
                     class: 'hudson-primary')
    end
  end

  def passenger_types(attraction)
    attraction.passenger_types || current_network.passenger_types
  end
end
