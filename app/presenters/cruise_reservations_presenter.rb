class CruiseReservationsPresenter < BasePresenter

  def airports
    Airports
      .airports_only_for(site_id, group_id)
      .collect { |a| [a.name, a.code] }
  end

  def arriving_destinations(airport = nil)
    airport ||= legs[0].airport
    return {} if airport.blank?

    ports = arriving_ports(airport)
    locations = arriving_non_ports(airport)

    return_hash = {}
    return_hash['Ports'] = ports unless ports.empty?
    return_hash.merge! locations

    return_hash
  end

  def arriving_non_ports(airport = nil)
    airport ||= legs[1].airport
    return {} if airport.blank?

    locations = Locations.by_type(
      site_id,
      airport_code: airport,
      group_id: group_id,
      fare_type: location_types_to_display) || []

    return_hash = {}
    locations.each do |loc_type|
      return_hash[loc_type[:label]] = loc_type[:locations]
    end

    return_hash
  end

  def arriving_ports(airport = nil)
    airport ||= legs[0].airport
    return {} if airport.blank?

    ports = Locations.by_type(
      site_id,
      airport_code: airport,
      group_id: group_id,
      fare_type: 'Y').first
    return [] unless ports

    ports[:locations]
  end

  def available_airlines(leg)
    leg_index = trip.leg_index(leg)
    key = leg_index == 2 ? leg.location_key : leg.airport_key
    leg.available_airlines(key).collect(&:name)
  end

  def departing_airports(airport = nil)
    airport ||= legs[2].airport
    return {} if airport.blank?

    airports = Locations.by_type(
      site_id,
      airport_code: airport,
      group_id: group_id,
      fare_type: 'A').first
    return [] unless airports

    airports[:locations]
  end

  def departing_destinations(airport = nil)
    airport ||= legs[2].airport
    return {} if airport.blank?

    airports = departing_airports(airport)
    locations = Locations.by_type(
      site_id,
      airport_code: airport,
      group_id: group_id,
      fare_type: location_types_to_display) || []

    return_hash = {}
    return_hash['Airports'] = airports unless airports.empty?
    locations.each do |loc_type|
      return_hash[loc_type[:label]] = loc_type[:locations]
    end
    return_hash
  end

  def ports
    Airports.ports(site_id, group_id).collect { |a| [a.name, a.code] }
  end

  def summary_icon(leg)
    # Determine which leg this is
    leg_index = trip.leg_index(leg)

    # Leg 0 is always airport arrival.
    if leg_index == 0
      h.fa_icon('plane rotate-90')

    # Leg 1 is cruise departure (from hotel)
    elsif leg_index == 1
      h.fa_icon('anchor')

    # Leg 2 is from cruise to airport or hotel
    elsif leg_index == 2
      h.fa_icon('anchor')

    # Leg 3 is from hotel to airport
    elsif leg_index == 3
      h.fa_icon('plane')
    end
  end

  private

  def group_id
    @model.group_id
  end

  def legs
    trip.legs
  end

  def location_types_to_display
    types = Tokens.for(site_id, 'MA_HOME_CRS_LOC_TYPES')
    types = 'H' if types.blank?
    types.split('|')
  end

  def site_id
    trip.cruise_site_id
  end

  def trip
    @model.current_trip
  end
end
