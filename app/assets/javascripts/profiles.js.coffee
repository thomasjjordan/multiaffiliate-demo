#= require app

class Profiles
  constructor: (@app) ->
    ready = =>
      if $('body[data-controller="profile"]').length > 0
        @_initForgotPasswordLink()
        FastClick.attach(document.body)

      onIframedPage = $('body[data-controller="profile"]')
        .filter('[data-action="join"], [data-action="home"]')
        .length > 0
      if onIframedPage
        @_showLoader()
        root.profile_window_loaded = @_hideLoader
        root.profileCreated = @_profileCreated
    $(document).ready(ready)
    $(document).on('page:load', ready)

  _initForgotPasswordLink: ->
    $('#forgot-password').click => @app.parentGoto 'forgot_password'

  _hideLoader: ->
    @app.parentDocument()?.getElementById('loading')?.style.visibility = 'hidden'

  _profileCreated: (username, password) =>
    $.get @app.url('reload_profile'), {username, password}

  _showLoader: ->
    @app.parentDocument()?.getElementById('loading')?.style.visibility = 'visible'

root = exports ? this
root.Profiles = new Profiles(root.App)