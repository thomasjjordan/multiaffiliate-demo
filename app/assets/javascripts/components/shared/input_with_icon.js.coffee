@InputWithIcon = React.createClass
  _field: -> React.findDOMNode(@refs.field)

  focus: -> @_field().focus()

  isBlank: -> !$.trim(@_field().value).length

  render: ->
    iconClassName = "fa fa-#{@props.icon} pull-right"

    props = {
      type: 'text',
      className: "form-control",
      autoComplete: 'off', autoCorrect: 'off', autoCapitalize: 'off',
      spellCheck: 'false',
      placeholder: @props.placeholder,
      onChange: @props.onChange,
      onFocus: @props.onFocus,
      onKeyDown: @props.onKeyDown,
      value: @props.value,
      ref: "field"
    }

    props.readOnly = @props.readOnly if @props.readOnly?
    props.value = @props.value if @props.value?
    props.disabled = @props.disabled if @props.disabled?

    <div className="form-group input-with-icon #{@props.className}">
      <input {...props}/>
      {this.props.children}
      <i className={iconClassName}></i>
    </div>
