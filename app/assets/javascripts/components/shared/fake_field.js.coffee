@FakeField = React.createClass
  render: ->
    iconClassName = "fa fa-#{@props.icon} pull-right"

    if @props.placeholder? && !@props.value?
      placeholder = <div className="placeholder">{@props.placeholder}</div>

    value = <div className="value">{@props.value}</div> if @props.value?

    props = {
      onClick: @props.onClick,
      tabIndex: @props.tabIndex || 0,
      onKeyDown: @props.onKeyDown,
      onFocus: @props.onFocus,
      className: "fake-field #{@props.className}"
    }
    props.disabled = @props.disabled if @props.disabled?

    <div {...props}>
      {placeholder}{value}
      <i className={iconClassName}></i>
    </div>
