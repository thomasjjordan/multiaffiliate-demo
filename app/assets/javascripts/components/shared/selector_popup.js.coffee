@SelectorPopup = React.createClass
  componentDidMount: ->
    $('body').addClass('modal-is-open')
    if window.matchMedia('screen and (max-width: 767px)').matches
      App.parentScrollToElement($('body'))

  componentWillUnmount: -> $('body').removeClass('modal-is-open')

  render: ->
    <div>
      <div className="selector-popup-back" onClick={@props.onClose}/>
      <div className="selector-popup" style={@props.style}>
        <div className="control-bar">
          <span className="close" onClick={@props.onClose}>x</span>
        </div>
        {@props.children}
      </div>
    </div>
