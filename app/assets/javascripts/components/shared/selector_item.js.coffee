@SelectorItem = React.createClass
  render: ->
    if @props.parent?
      forwardIcon = <div className="forward">
                      <i className='fa fa-caret-right'></i>
                    </div>
    className = "selector-item #{@props.className}"
    className += " selected hudson-primary" if @props.selected

    <li className={className} key={@props.key} onClick={@props.onClick}>
      <div className="value">{@props.children}</div>
      {forwardIcon}
    </li>
