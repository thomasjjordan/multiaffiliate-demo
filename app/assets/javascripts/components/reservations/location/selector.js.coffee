@Location.Selector = React.createClass
  displayName: 'Location.Selector'

  getInitialState: ->
    if @props.location && @props.region?
      types = @props.locations[@props.region.provider_site_id]
      if types
        locType = (type for type in types when type.cat == @props.location.FareType)?[0]

    locType: locType,
    region: @props.region,

  _backFromTypeSelection: -> @setState region: null, locType: null

  _locSelected: (loc) -> @props.locSelected(@state.region, loc)

  _providerSiteId: -> @state.region?.provider_site_id

  _regionSelected: (region) ->
    @setState region: region
    if @props.locations?[region.provider_site_id]?.length == 1
      @setState locType: @props.locations[region.provider_site_id][0]

  _typeSelected: (locType) -> @setState locType: locType

  clearLocType: ->
    @props.onRefocus()
    @setState locType: null
    @currentSelector().down()

  currentSelector: ->
    if @selectingRegion()
      @refs.regionSelector
    else if !@state.locType?
      @refs.typeSelector
    else if @state.locType.displayType == 'list'
      @refs.locSelector
    else
      @refs.acSelector

  down: -> @currentSelector().down()

  left: -> @currentSelector().left() if @currentSelector().left

  select: -> @currentSelector().select()

  selectingRegion: -> !@_providerSiteId()?

  up: -> @currentSelector().up()

  render: ->
    if @selectingRegion()
      <Location.Selector.Region ref="regionSelector"
                                regions={@props.regions}
                                onChange={@_regionSelected}/>
    else
      locations = @props.locations[@_providerSiteId()]
      if Object.keys(@props.locations).length > 1
        onBack = @_backFromTypeSelection
        region = @state.region

      typeSelector = <Location.Selector.Type ref="typeSelector"
                             providerSiteId={@_providerSiteId()}
                             locations={locations}
                             locationsLoading={@props.locationsLoading}
                             locType={@state.locType}
                             typeSelected={@_typeSelected}
                             region={region}
                             onBack={onBack}
                             backLabel="Select a different region"
                             />
      if @state.locType?
        locs = @props.locations[@_providerSiteId()]
        locs = (loc.locations for loc in locs when loc.cat == @state.locType.cat)?[0]

        if @state.locType.displayType == 'list'
          locSelector = <Location.Selector.List ref="locSelector"
                                 locations={locs}
                                 locType={@state.locType}
                                 locSelected={@_locSelected}/>
        else
          locSelector = <Location.Selector.AutoComplete ref="acSelector"
                           locSelected={@_locSelected}
                           locations={locs}
                           locType={@state.locType}
                           providerSiteId={@_providerSiteId()}
                           locationSearchURL={@props.locationSearchURL}
                           placeholder={@props.placeholder}
                           onBack={@clearLocType}
                           onBlur={@props.onBlur}/>


      <div className="row">
        <div className="col-sm-6">{typeSelector}</div>
        <div className="col-sm-6">{locSelector}</div>
      </div>
