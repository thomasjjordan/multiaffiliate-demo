@Location.Selector.AutoComplete = React.createClass
  displayName: 'Location.Selector.AutoComplete'

  getInitialState: ->
    locations: @props.locations || [],
    noLocationsFound: false

  componentDidMount: ->
    this.refs.field.focus() unless @props.locations.length
    @_scrollToMeOnMobile()

  componentDidUpdate: (prevProps, prevState) ->
    if prevProps.locType != @props.locType
      @setState locations: (@props.locations || []), noLocationsFound: false
    this.refs.field.focus() unless @props.locations.length
    @_scrollToMeOnMobile()

  componentWillUnmount: -> clearTimeout(@_searchDebouncer)

  _locSelected: (loc) ->
    @props.locSelected(loc)

  _scrollToMeOnMobile: ->
    if window.matchMedia('screen and (max-width: 767px)').matches
      App.parentScrollToElement($(React.findDOMNode(this.refs.field)), 150)

  _search: ($searchBox, query) ->
    if query.length
      $.getJSON(@props.locationSearchURL,
                term: query,
                loc_type: @props.locType.cat,
                provider_site: @props.providerSiteId)
        .then (locs) =>
          # Make sure the user hasn't updated their query.
          return unless $searchBox.val() == query

          if locs.length && locs[0].FareType == @props.locType.cat
            @setState locations: locs, noLocationsFound: false
          else
            @setState locations: [], noLocationsFound: true
    else
      @setState locations: (@props.locations || []), noLocationsFound: false

  # Introduces a slight delay to ensure we aren't sending tons of AJAX calls.
  _searchDelayer: (event) ->
    $searchBox = $(event.target)
    query = $searchBox.val()
    clearTimeout(@_searchDebouncer)
    @_searchDebouncer = setTimeout((=> @_search($searchBox, query)), 200)

  handleKeyDown: (e) ->
    keyCode = e.keyCode || e.which
    if keyCode == 9 # Tab
      @props.onBlur()
    else if keyCode == 40 # Down key
      @refs.selector.down() if @refs.selector.down
      e.preventDefault()
    else if keyCode == 38 # Up
      @refs.selector.up() if @refs.selector.up
      e.preventDefault()
    else if keyCode == 37 # Left
      if @refs.field.isBlank()
        @props.onBack()
        e.preventDefault()
    else if keyCode == 13 # Enter
      @refs.selector.select() if @refs.selector.select

  render: ->
    locs = @state.locations.map (loc) =>
      onClick = @_locSelected.bind(this, loc)
      if loc.Address?
        address = <div><span className='loc-address'>{loc.Address}</span></div>
      if loc.City? || loc.State?
        cityState = <div><span className='loc-address'>{loc.City}, {loc.State}</span></div>

      <SelectorItem key={loc.Code} onClick={onClick}>
        {loc.Name}{address}{cityState}
      </SelectorItem>

    if @state.noLocationsFound
      noLocsFoundMessage = <div className="no-locs">
                             No matching locations were found.
                           </div>

    <div className="auto-complete-selector" key={@props.locType.cat}>
      <div className="ac-label hudson-subsection">
        {@props.locType.label_without_popup.replace(/(<([^>]+)>)/ig, " ")}
      </div>
      <InputWithIcon className="hudson-subsection"
                     icon="search"
                     onChange={@_searchDelayer}
                     key="field#{@props.locType.cat}"
                     ref="field"
                     onKeyDown={@handleKeyDown}
                     placeholder={@props.placeholder}/>
      {noLocsFoundMessage}
      <SelectorList ref="selector" paginateOnMobile="true">{locs}</SelectorList>
    </div>

