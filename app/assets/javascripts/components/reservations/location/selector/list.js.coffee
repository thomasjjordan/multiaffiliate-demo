@Location.Selector.List = React.createClass
  displayName: 'Location.Selector.List'

  _locSelected: (loc) -> @props.locSelected(loc)

  down: -> @refs.list.down()

  left: -> @props.onBack() if @props.onBack

  select: -> @refs.list.select()

  up: -> @refs.list.up()

  render: ->
    locs = @props.locations.map (loc) =>
      onClick = @_locSelected.bind(this, loc)
      <SelectorItem key={loc.Code} onClick={onClick}>{loc.Name}</SelectorItem>

    <div className="list-selector">
      <div className="list-label hudson-subsection">
        {@props.locType.label_without_popup.replace(/(<([^>]+)>)/ig, " ")}
      </div>
      <SelectorList onBack={@props.onBack} ref="list"
                    paginateOnMobile="true"
                    className="optimized">{locs}</SelectorList>
    </div>
