@Leg = React.createClass
  displayName: 'Leg'

  getInitialState: ->
    airport: @props.airport.current,
    location: @props.location.current,
    locations: {},
    locationsLoading: false,
    region: @props.region

  componentDidMount: ->
    @_loadLocations(@state.airport) if @state.airport?

    if @state.region?
      ResPage.loadServiceExtras(@props.index, @state.airport.code,
                                @state.region.provider_site_id)

    ResPage.updateSubmitStatus()

  componentDidUpdate: (prevProps, prevState) ->
    # If the selected site only supports roundtrip, redirect them.
    if @state.region? && prevState.region != @state.region &&
       @state.region.roundtrip_only &&
       !($('.page').data('type') is 'roundtrip')
      @_redirectToRoundtrip()

    # If region is deselected, clear the service extras.
    ResPage.clearServiceExtras(@props.index, true) if !@state.region?
    ResPage.updateSubmitStatus()

  _airportSelected: (airport) ->
    return if airport == @state.airport
    @setState airport: airport, location: null, region: null
    @_loadLocations(airport)
    ResPage.updateSubmitStatus()

  _loadLocations: (airport) ->
    siteIds = airport.service_areas.map (region) -> region.provider_site_id
    @setState locationsLoading: true, locations: {}
    $.getJSON(@props.urls.initial_locations,
              leg: @props.index,
              airport: airport.code,
              direction: ResPage.direction(@props.index),
              provider_site: siteIds.join())
      .then (locs) =>
        @setState locationsLoading: false, locations: locs

        # If only one location came back, use it.
        if siteIds.length == 1
          locTypes = locs[siteIds[0]]
          firstLocTypeLocs = locTypes[0].locations
          if locTypes.length == 1 && firstLocTypeLocs.length == 1
            @_locSelected(airport.service_areas[0], firstLocTypeLocs[0])

  _locSelected: (region, loc) ->
    @setState region: region, location: loc
    ResPage.loadServiceExtras(@props.index, @state.airport.code,
                              region.provider_site_id)
    ResPage.updateSubmitStatus()

  _redirectToRoundtrip: ->
    params = $.param({
                      reservation_params: 1,
                      forced_roundtrip: true,
                      "trip[site]": @state.region.provider_site_id,
                      "trip[airport]": @state.airport.code,
                      "trip[location]": @state.location.Code})
    window.location.href = @props.urls.roundtrip + "?" + params

  locationSearchURL: ->
    @props.urls.location_search + '?' +
      $.param({
               leg: @props.index,
               direction: ResPage.direction(@props.index),
               airport: @state.airport?.code })

  region: ->
    if @state.region?
      @state.region
    else if @state.airport?.service_areas.length == 1
      @state.airport.service_areas[0]
    else
      null

  render: ->
    <div className="col-sm-5">
      <i className="fa fa-circle-o-notch fa-spin"
         style={position: 'absolute', opacity: 0}></i>

      <Airport current={@state.airport}
               index={@props.index}
               airports={@props.airport.airports}
               label={@props.airport.label}
               placeholder={@props.airport.placeholder}
               tabIndex={@props.airport.tabindex}
               type={@props.airport.field_type || 'search'}
               onChange={@_airportSelected}/>
      <Location current={@state.location}
                index={@props.index}
                label={@props.location.label}
                placeholder={@props.location.placeholder}
                regions={@state.airport?.service_areas}
                locations={@state.locations}
                locationsLoading={@state.locationsLoading}
                region={@region()}
                locSelected={@_locSelected}
                locationSearchURL={@locationSearchURL()}/>
    </div>
