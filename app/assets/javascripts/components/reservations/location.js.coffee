@Location = React.createClass
  displayName: 'Location'

  getInitialState: -> displayPopup: false

  _locSelected: (region, loc) ->
    @props.locSelected(region, loc)
    @hidePopup()

  _locType: ->
    return unless @props.current? && @props.region? && @props.locations?
    locs = @props.locations[@props.region.provider_site_id] || {}
    loc = (loc for loc in locs when loc.cat == @props.current.FareType)
    if loc then loc[0] else null

  handleKeyDown: (e) ->
    keyCode = e.keyCode || e.which
    if keyCode == 9 # Tab
      @hidePopup()
    else if keyCode == 40 # Down key
      if @state.displayPopup
        @refs.selector.down() if @refs.selector.down
      else
        @showPopup()
      e.preventDefault()
    else if keyCode == 38 # Up
      @refs.selector.up() if @refs.selector.up
      e.preventDefault()
    else if keyCode == 37 # Left
      @refs.selector.left() if @refs.selector.left
      e.preventDefault()
    else if keyCode == 13 || keyCode == 39 # Enter, Right
      @refs.selector.select() if @refs.selector.select
    else if keyCode == 27 # Esc
      @hidePopup()

  hidePopup: ->
    @refocus() if window.matchMedia('screen and (max-width: 767px)').matches
    @setState displayPopup: false

  refocus: -> React.findDOMNode(@refs.field).focus()

  showPopup: -> @setState displayPopup: true

  render: ->
    popup =
      <SelectorPopup onClose={@hidePopup} key="popup" style={width: '600px'}>
        <Location.Selector ref="selector"
                           regions={@props.regions}
                           region={@props.region}
                           locations={@props.locations}
                           location={@props.current}
                           locationsLoading={@props.locationsLoading}
                           locSelected={@_locSelected}
                           locationSearchURL={@props.locationSearchURL}
                           placeholder={@props.placeholder}
                           onBlur={@hidePopup}
                           onRefocus={@refocus}/>
      </SelectorPopup>

    if @_locType() && @_locType().catInfo
      catInfo = <span className="cat-info label label-warning">
                  {@_locType().catInfo}
                </span>

    fakeFieldProps = {
      icon: 'search',
      ref: 'field',
      placeholder: @props.placeholder,
      onClick: @showPopup,
      onFocus: @showPopup,
      onKeyDown: @handleKeyDown,
      value: @props.current?.Name
    }
    unless @props.region? || @props.regions? # Disabled
      fakeFieldProps.className = 'disabled'
      fakeFieldProps.onClick = null
      fakeFieldProps.onFocus = null
      fakeFieldProps.onKeyDown = null

    <div className="location location-container">
      <div dangerouslySetInnerHTML={{__html: @props.label}}/>
      <input type="hidden" name="trip[#{@props.index}][PrimarySite]"
             value={@props.region?.provider_site_id}/>
      <input type="hidden" name="trip[#{@props.index}][location]"
             className="location-field" value={@props.current?.Code}/>

      <FakeField {...fakeFieldProps}/>
      {catInfo}
      {if @state.displayPopup then popup}
    </div>

