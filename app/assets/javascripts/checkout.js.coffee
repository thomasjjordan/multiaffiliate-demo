#= require app

class Checkout
  constructor: (@app) ->
    ready = =>
      if $('body[data-controller="cart"][data-action="index"]').length > 0
        @app.setHeightAdjustment()
        @_initCartSubmit()
        @app.autoResizeParent()
      if $('body[data-action="checkout"]').length > 0
        @_initPaymentTypes()
        @_initCreditCardFields()
        @_initCheckoutSubmit()
        $('button').each -> new FastClick(this)
        @app.parentScrollToElement($('form'))
    $(document).ready ready
    $(document).on 'page:load', ready
    $(window).on 'page:restore', ready

  _initCreditCardFields: =>
    $('#cart_CCDetails').change ->
        $('#cart_Name').val($('#cart_CCDetails').val()) if !$('#cart_Name').val()

  _initPaymentTypes: =>
    $('.payment-types a').on 'click', (ev) =>
        @changePaymentType( $(ev.currentTarget).data('type') )
        ev.preventDefault()
    @changePaymentType($('#payment-type').val())

  _initCartSubmit: =>
    $('#checkout-btn')
      .button('reset')

  _initCheckoutSubmit: =>
    $('.submit').button().click ->
      $(this).button('loading')
      $('#new_cart').submit()

  changePaymentType: (type) ->
    type = parseInt(type)
    $('#payment-type').val(type)

    # Show/Hide credit card fields.
    ccRequired = $('#credit-card').data('required')
    if type == 1 || (type == 0 && ccRequired)
      $('#credit-card').fadeIn(=> @app.autoResizeParent())
    else
      $('#credit-card').fadeOut(=> @app.autoResizeParent())

    $('.payment-types li').removeClass('active')
    $('.payment-types li a').removeClass('hudson-button')
    $("#pt-#{type}").parent().addClass('active')
    $("#pt-#{type}").addClass('hudson-button')


root = exports ? this
root.Checkout = new Checkout(root.App)
