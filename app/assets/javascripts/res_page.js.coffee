#= require app

class ResPage
  constructor: (@app) ->
    ready = =>
      # If a reservations page (but not the cruise page)
      if $('div[data-page="reservations"]').length > 0
        if $('div[data-type="cruise"]').length == 0
          @_initDirectionDropdown()
          @_initMultilegButtons()

        @_initSubmit()
        @_initDateTimePickers()
        @_initLoginLink()
        @_initPassengerPanels()
        @_initServiceExtraPanels()
        @app.autoResizeParent()

        # FastClick support.
        $('.direction-dropdown, #trip-type-selector, button, .selector-popup, .fake-field, .close, .selector-list-pageforward, .selector-list-pageback, .selector-item').each -> new FastClick(this)

        # Alert users who may be running Internet Explorer in compatibility view mode.
        if @app.isIE11CompatMode()
          @app.showModal(@app.config('compat-message'))

    $(document).ready ready
    $(document).on 'page:load', ready
    $(window).on 'page:restore', ready
    @locations = []

  _initDateTimePickers: ->
    $('.datepicker').datepicker(todayHighlight: true, autoclose: true, startDate: '+0d', disableTouchKeyboard: true)

  _initDirectionDropdown: =>
    $('.direction-dropdown li').click (ev) =>
        typeElem = $(ev.delegateTarget)
        if typeElem.attr('class') is 'direction-type-heading'
          ev.preventDefault(); return
        label = $('a', typeElem).html()
        index = typeElem.data('index')
        $("#dropdown-toggle-#{index} .type-label").html(label)
        @changeDirection(index, typeElem.data('type'), typeElem.data('direction'))
    # Initialize direction type-related stuff
    $.each $('.leg-container'), (index, legElem) => @setDirection(index)

  _initLoginLink: =>
    $('.login').click (e) =>
      $('#login-modal').modal('show')
      e.preventDefault()
    $('.login-modal-form').submit (e) =>
      e.preventDefault()
      $('button', $(e.currentTarget)).button('loading')

      formData = $(".login-modal-form").serialize()
      $.post @app.url('login_modal'), formData, (data) =>
        $('button', $(e.currentTarget)).button('reset')
        alert(data.error) if data.error

  _initMultilegButtons: =>
    # If there are 2 or less legs and all legs are displayed then
    # don't show the add/remove buttons (they aren't needed)
    maxLegs = parseInt($('.page').data('max-legs'))
    if maxLegs <= 2 && maxLegs == parseInt($('#leg_count').val())
      $('.multi-leg-buttons').hide()
      return

    $('.remove-leg').click (ev) =>
      legCount = $('.leg-container:visible').length - 1
      $('.leg-container:visible').last().slideUp(200, =>
        @_setMultilegButtonStates()
        @updateSubmitStatus() )
      $('#leg_count').val(legCount)
      @app.autoResizeParent()
      ev.preventDefault()
    $('.add-leg').click (ev) =>
      legCount = $('.leg-container:visible').length + 1
      $('.leg-container:hidden').first().removeClass('hide').slideDown(200, =>
        @_setMultilegButtonStates()
        @updateSubmitStatus() )
      $('#leg_count').val(legCount)
      @app.autoResizeParent()
      @app.scrollToTheBottom()
      ev.preventDefault()
    @_setMultilegButtonStates()

  _initPassengerPanels: ->
    $('.passenger-count').change( (ev) =>
      # Update the "total passenger" count in the accordion head.
      legIndex = $(ev.target).data('leg')
      passFields = $.makeArray($(".passenger-count[data-leg='#{legIndex}']"))
      passTotal = passFields.reduce ((x,y) -> x + parseInt($(y).val())), 0
      $("#passenger-count-#{legIndex}").text(passTotal) )
    $('.passengers-header').click -> $(this).siblings().first().collapse('toggle')
    $('.passengers-form')
      .on 'show.bs.collapse', (ev) ->
          $($('i', ev.target.parentElement)[1])
            .fadeOut 150, ->
                $(this).attr('class', 'fa fa-chevron-up')
                $(this).fadeIn(300)
      .on 'hide.bs.collapse', (ev) ->
          $($('i', ev.target.parentElement)[1])
            .fadeOut 150, ->
                $(this).attr('class', 'fa fa-chevron-down')
                $(this).fadeIn(300)
      .on 'shown.bs.collapse', ->
        App.autoResizeParent()
        $(this).parents('.row').scrollIntoView()
      .on 'hidden.bs.collapse', -> App.autoResizeParent()

  _initServiceExtraPanels: =>
    $('.svc-extras-header').click -> $(this).siblings().first().collapse('toggle')
    $('.svc-extras-form')
      .on 'show.bs.collapse', (ev) ->
        $($('i', ev.target.parentElement)[1])
          .fadeOut 150, ->
              $(this).attr('class', 'fa fa-chevron-up')
              $(this).fadeIn(300)
      .on 'hide.bs.collapse', (ev) ->
        $($('i', ev.target.parentElement)[1])
          .fadeOut 150, ->
              $(this).attr('class', 'fa fa-chevron-down')
              $(this).fadeIn(300)
      .on 'shown.bs.collapse', ->
        App.autoResizeParent()
        $(this).parents('.row').scrollIntoView()
      .on 'hidden.bs.collapse', -> App.autoResizeParent()

  _initSubmit: =>
    @updateSubmitStatus()
    $('.submit').button().click -> $(this).button('loading')

  # Disable/enable multi-leg remove/add leg buttons.
  _setMultilegButtonStates: ->
    legsDisplayed = $('.leg-container:visible').length
    if legsDisplayed is $('.page').data('max-legs')
      $('.add-leg').attr('disabled', 'disabled')
    else
      $('.add-leg').removeAttr('disabled')
    if legsDisplayed is 1
      $('.remove-leg').attr('disabled', 'disabled')
    else
      $('.remove-leg').removeAttr('disabled')

  allLocationsSet: ->
    emptyLocs = $.grep $('.location-container:visible .location-field'),
        (elem, index) -> return ($(elem).val() == '')
    emptyLocs.length == 0

  changeDirection: (legIndex, dirType, direction) =>
    dirTypeField = $("#trip_#{legIndex}_direction_type")
    @setDirection(legIndex, dirType, direction) if dirType != dirTypeField.val()

  clearServiceExtras: (legIndex, show_message) =>
    if show_message
      message = $("#svc-extras-form-#{legIndex}").data('empty-msg')
    $("#svc-extras-form-#{legIndex} .panel-body").empty().html(message)

  direction: (legIndex) -> $("#trip_#{legIndex}_Direction").val()

  disableSubmit: -> $('.submit').attr('disabled', true)

  enableSubmit: -> $('.submit').removeAttr('disabled')

  loadServiceExtras: (legIndex, airport, siteId) =>
    @clearServiceExtras(legIndex)
    @app.showLoader($("#svc-extras-form-#{legIndex} .panel-body"),
                    placement: 'append', centered: true)

    $.get @app.url('svc_extras'),
        { leg: legIndex, airport, provider_site: siteId },
        (data) =>
          if data.error
            alert data.error
          else
            $("#svc-extras-form-#{legIndex} .panel-body").html(data.extras_html)
            $("#svc-extras-form-#{legIndex} label").popover()
            @app.autoResizeParent()

  setDirection: (legIndex, dirType, direction) =>
    directionField = $("#trip_#{legIndex}_Direction")
    dirTypeField = $("#trip_#{legIndex}_direction_type")
    dirType = dirType || dirTypeField.val()
    direction = direction || directionField.val()

    # Ensure that the direction set is in the list of direction types.
    # Otherwise, set it to a valid value.
    dirTypes = ($(elem).data('type') for elem in $('.dropdown-menu:first li'))
    if dirType not in dirTypes
      newDirTypeElem = $($('.dropdown-menu:first li')[1])
      dirType = newDirTypeElem.data('type')
      direction = newDirTypeElem.data('direction')
      $("#dropdown-toggle-#{legIndex} .type-label").html(newDirTypeElem.html())

    dirTypeField.val(dirType)
    if direction != directionField.val() # Direction changed.
      directionField.val(direction)
      # Reverse from/to labels
      $(".directional[data-leg='#{legIndex}']").hide()
      $(".directional[data-leg='#{legIndex}'][data-direction='#{direction}']").show()

    # Update the flight fields (ex. flight time is "arrival time" if this is a cruise)
    $(".flight-date-label[data-leg='#{legIndex}']").hide()
    if dirType[0] is 'C' # If pickup is a port
      $("#flight-type-container-#{legIndex}").hide()
      $(".flight-date-label[data-leg='#{legIndex}'][data-type='CA']").show()
    else if dirType[2] is 'C' && direction is 'D'
      $("#flight-type-container-#{legIndex}").hide()
      $(".flight-date-label[data-leg='#{legIndex}'][data-type='CD']").show()
    else
      $("#flight-type-container-#{legIndex}").show()
      $(".flight-date-label[data-leg='#{legIndex}'][data-type='A']").show()

  updateSubmitStatus: =>
    if @allLocationsSet()
      @enableSubmit()
    else
      @disableSubmit()

root = exports ? this
root.ResPage = new ResPage(root.App)
root.ResPageClass = ResPage
