// Shims needed for old browser support for React
//= require console-polyfill
//= require matchMedia
//= require es5-shim.min
//= require es5-sham.min

//= require jquery
//= require jquery_ujs
//= require jquery.color
//= require jquery.ui.autocomplete
//= require velocity
//= require chardinjs
//= require bootstrap-sprockets
//= require datepicker/bootstrap-datepicker
//= require jquery.dateFormat
//= require jquery.creditCardValidator
//= require jquery.scrollIntoView.min
//= require jquery.jscroll.min
//= require 'chosen/chosen.jquery'
//= require fastclick.min
//= require_self
//= require 'ie8_polyfills'
//= require 'app'
//= require 'res_page'
//= require_tree ./res_pages
//= require 'services'
//= require 'checkout'
//= require 'receipt'
//= require 'attractions'
//= require 'profiles'
//= require 'coupon'
// require turbolinks

//= require react
//= require react_ujs
//= require components

// Ensure console doesn't cause a problem in production.
if(typeof console === "undefined") { console = { log: function() { } }; }
