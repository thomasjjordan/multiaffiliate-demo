//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require 'app'
//= require_self


class Configuration
  constructor: (@app) ->
    ready = =>
      return unless $('body[data-controller="configuration"]').length
      @_initAirportList()

    $(document).ready ready
    $(document).on 'page:load', ready
    $(window).on 'page:restore', ready

  _updateAirports: (attractionId, updatedAirportList, actions = {}) =>
    $siteTr = $("tr[data-site-id=#{attractionId}]")
    if actions.addAirport
      $(".airport-list", $siteTr).append(actions.addAirport)
    if actions.remove
      $(".airport-list p[data-airport=#{actions.remove}]", $siteTr).remove()


    $select = $(".add-airport select", $siteTr)
    $select.empty().append "<option></option>"
    for airport in updatedAirportList
      opt = $("<option value='#{airport.Code}'>#{airport.Description}</option>")
      $select.append opt

    # Hide the "add airport" section if there are no airports available to add.
    $('.add-airport', $siteTr).toggle(updatedAirportList.length)

  _initAirportList: =>
    $('.add-airport button').click (e) =>
      $form = $(e.currentTarget).parents('form.add-airport').first()
      attractionId = $('input[name=id]', $form).val()
      airport = $('select[name=airport]', $form).val()
      @addAirport(attractionId, airport)
      e.preventDefault()

    $('.airport-list').on 'click', '.remove-airport', (e) =>
      $my = $(e.currentTarget)
      attractionId = $my.parents('tr').first().data('site-id')
      airport = $my.data('airport')
      airportCode = $my.data('code')
      if confirm("You are about to remove #{airportCode} from this attraction.")
        @deleteAirport(attractionId, airport)

  addAirport: (attractionId, airport) =>
    $.post @app.url('add_airport'),
           { id: attractionId, airport },
           (data) =>
             if data.airport
               @_updateAirports(attractionId,
                                data.available_airports,
                                addAirport: data.airport)

  deleteAirport: (attractionId, airport) =>
    $.post @app.url('delete_airport'),
           { id: attractionId, airport },
           (data) =>
             if data.available_airports
               @_updateAirports(attractionId,
                                data.available_airports,
                                remove: airport)

root = exports ? this
root.Configuration = new Configuration(root.App)
