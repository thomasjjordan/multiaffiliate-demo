class ConfigurationController < ApplicationController
  layout 'configuration'
  before_action :ensure_web10 unless Rails.env.development?
  before_action :set_login_session
  before_action :login_required, except: :logout

  display_tokens(
    'NETWORK_CONFIG_' => {
      config_logo: 'LOGO'
    },
    site_logo: 'LOGO'
  )

  def activate_site
    if params[:groundnet_site_id]
      site = Network::Site.find(params[:groundnet_site_id])
      try_to_activate_site(site)
      site.reload
    elsif params[:attraction]
      site = Network::Attraction.find(params[:attraction])
      site.update_attributes(Active: true,
                             ErrorMessage: nil,
                             ErrorOccurredAt: nil)
    end

    if site.Active
      flash[:message] = "#{site.ProviderSiteId} is now active."
    else
      flash[:error] = "Unable to activate #{site.ProviderSiteId}: #{site.ErrorMessage}"
    end

    redirect_to config_home_url(site_id: current_site_id)
  end

  def add_airport
    attraction = Network::Attraction.find(params[:id])
    airport = Network::Airport.where(Code: params[:airport],
                                     SiteID: current_network.site_id).first

    unless attraction.airports.include?(airport)
      attraction.airports << airport
    end

    airport_html = render_to_string('configuration/_airport.html',
                                    layout: false,
                                    locals: { airport: airport })
    render json: { airport: airport_html,
                   available_airports: attraction.unattached_airports }
  end

  def add_airports_to_site
    unless params[:groundnet_site].blank?
      if site = Network::Site.where(:SiteID => current_network.site_id,
            :ProviderSiteId => params[:groundnet_site]).first
        add_airports(site)
        try_to_activate_site(site)
        flash[:message] = "Added airports to #{params[:groundnet_site]}."
      end
    end
    redirect_to :action => 'index' and return

  rescue Hudson::UnknownSite
    flash[:error] = "Couldn't find a site called #{params[:groundnet_site]}."
    redirect_to :action => 'index', :groundnet_site => params[:groundnet_site] and return
  end

  def add_default_passenger_type_rows
    Network::Site.where('SiteID' => current_network.site_id).each do |site|
      Network::PassengerTypeTranslation.create(
        :SiteID => current_network.site_id,
        :ProviderSiteID => site.ProviderSiteId,
        :ProviderPassengerField => 'Passengers',
        :PassengerField => 'AltPassengers'
      )
    end

    flash[:message] = "Added passenger type default rows"
    redirect_to :action => 'index' and return
  end

  def disable_site
    if params[:site]
      site = Network::Site.for(current_network.site_id, params[:site])
      flash[:message] = "#{params[:site]} has been disabled."
    elsif params[:attraction]
      site = Network::Attraction.for(current_network.site_id, params[:attraction])
      flash[:message] = "#{params[:attraction]} has been disabled."
    end

    site.update_attributes(Active: false,
                           ErrorMessage: "Disabled through 'Disable Site' link",
                           ErrorOccurredAt: Time.now) if site

    redirect_to config_home_url(site_id: current_network.site_id)
  end

  def add_site
    if params[:groundnet_site].blank?
      redirect_to action: 'index'
      return
    end

    if params[:transportation]
      if Network::Site.for(current_network.site_id, params[:groundnet_site])
        flash[:error] = 'This site has already been added.'
        redirect_to(action: 'index') && return
      end

      site = Network::Site.create(
        SiteID: current_network.site_id,
        ProviderSiteId: params[:groundnet_site],
        Name: Tokens.for(params[:groundnet_site], 'TITLE'),
        Active: false)
      add_airports(site)

      Network::PassengerTypeTranslation.create(
        SiteID: current_network.site_id,
        ProviderSiteID: site.ProviderSiteId,
        ProviderPassengerField: 'Passengers',
        PassengerField: 'AltPassengers')

      try_to_activate_site(site)

    elsif params[:attraction]
      if Network::Attraction.for(current_network.site_id, params[:groundnet_site])
        flash[:error] = 'This site has already been added.'
        redirect_to(action: 'index') && return
      end

      site = Network::Attraction.create(
        SiteID: current_network.site_id,
        ProviderSiteId: params[:groundnet_site],
        Name: Tokens.for(params[:groundnet_site], 'TITLE'),
        Active: true)
    end

    flash[:message] = "Added #{params[:groundnet_site]} to the list."
    redirect_to action: 'index'

	rescue HudsonWebService::AccessDenied
    flash[:error] = "#{params[:groundnet_site]} has been disabled and cannot be accessed."
    redirect_to :action => 'index', :groundnet_site => params[:groundnet_site] and return

  rescue Hudson::UnknownSite, HudsonWebService::NoWebServiceError
    flash[:error] = "Couldn't find a site called #{params[:groundnet_site]}."
    redirect_to :action => 'index', :groundnet_site => params[:groundnet_site] and return
  end

  def advanced
    if request.post?
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'CurrencyOut')
        .first_or_create.update_attribute('Value', params[:currency_out])
      Network::Config.where(SiteID: current_network.site_id,
                           Name: 'PassengerTypeFilter')
        .first_or_create
        .update_attribute('Value', params[:passenger_type_filter])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'GAUniversal')
        .first_or_create.update_attribute('Value', params[:ga_universal])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'SingleSite')
        .first_or_create.update_attribute('Value', params[:single_site])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'MultiLegDisabled')
        .first_or_create.update_attribute('Value', params[:multi_leg])

      Network::Config.where(:SiteID => current_network.site_id, :Name => 'OnewayDisabled')
        .first_or_create.update_attribute('Value', params[:oneway])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'RoundtripDisabled')
        .first_or_create.update_attribute('Value', params[:roundtrip])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'CruiseEnabled')
        .first_or_create.update_attribute('Value', params[:cruise])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'CruiseSeparate')
        .first_or_create.update_attribute('Value', params[:cruise_separate])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'HideTabBar')
        .first_or_create.update_attribute('Value', params[:hide_tab_bar])

      Network::Config.where(:SiteID => current_network.site_id, :Name => 'MultiLegMaxLegs')
        .first_or_create.update_attribute('Value', params[:multi_leg_max_legs])
      Network::Config.where(:SiteID => current_network.site_id, :Name => 'MultiLegInitialLegs')
        .first_or_create.update_attribute('Value', params[:multi_leg_init_legs])
      flash[:message] = "Updated advanced config."
      redirect_to config_home_url(site_id: current_site_id)
    end
    @currency_out = Network::Config.for(current_network.site_id, 'CurrencyOut')
    @passenger_type_filter = Network::Config.for(current_network.site_id,
                                                 'PassengerTypeFilter')
    @ga_universal = Network::Config.for(current_network.site_id, 'GAUniversal')
    @single_site = Network::Config.for(current_network.site_id, 'SingleSite')
    @multi_leg = Network::Config.for(current_network.site_id, 'MultiLegDisabled')
    @multi_leg_max_legs = Network::Config.multi_leg_max_legs(current_network.site_id)
    @multi_leg_init_legs = Network::Config.multi_leg_initial_legs(current_network.site_id)
    @oneway = Network::Config.for(current_network.site_id, 'OnewayDisabled')
    @roundtrip = Network::Config.for(current_network.site_id, 'RoundtripDisabled')
    @cruise = Network::Config.for(current_network.site_id, 'CruiseEnabled')
    @cruise_separate = Network::Config.for(current_network.site_id, 'CruiseSeparate')
    @hide_tab_bar = Network::Config.for(current_network.site_id, 'HideTabBar')
  end

  def airport
    @airport = Network::Airport.where(:ID => params[:airport_id]).first
    @groups = Network::AirportGroup.where('SiteId' => current_network.site_id).order('Description ASC')

    if @airport.nil?
    	@airport = Network::Airport.new(:SiteID => current_network.site_id, :Code => 'xxx', :Description => 'New Airport')
    end
		if @airport.map_point.nil?
      @airport.map_point = Network::MapPoint.new(:SiteID => current_network.site_id, :Lat => 42.696939, :Long => -71.158326)
    end

		# Initialize map
    @map = GMap.new('locations_map_panel')
    @map.control_init(:large_map => true, :map_type => false)
    @map.set_map_type_init(GMapType::G_PHYSICAL_MAP)
    @map.center_zoom_init([@airport.map_point.Lat,@airport.map_point.Long],5)

		# icon_url = @airport.map_point.uses_alternate_icon? ? @airport.map_point.IconURL :
			# Tokens.for(current_network.site_id, 'MA_LOCATOR_ICON')
    @map.icon_global_init( GIcon.new( :image => icon_url,
    	:icon_size => GSize.new( 31,23 ), :icon_anchor => GPoint.new(15,10)),
    	"icon_#{@airport.map_point.ID}")
    icon = "icon_#{@airport.map_point.ID}".to_sym

    marker = PDMarker.new([@airport.map_point.Lat, @airport.map_point.Long], {:icon => icon, :draggable => true},
      @airport.map_point.Location)
    @map.declare_init(marker, "marker_#{@airport.map_point.id}")
    @map.overlay_global_init(marker, 'marker')
    @map.event_init(marker, 'dragend', "function() {
    	lat_lon = marker.getLatLng();
    	$('Lat').value = lat_lon.lat();
    	$('Long').value = lat_lon.lng();
    }")

    if request.post?
    	if params[:do] == 'update'
        @airport.AirportGroupID = params[:airport_group]
        @airport.map_point.AlternateIcon = params[:alternate_icon]
        @airport.map_point.Lat = params[:Lat]
        @airport.map_point.Long = params[:Long]
        @airport.map_point.IconURL = params[:IconURL]
        @airport.map_point.save
        @airport.save
        redirect_to :action => 'map' and return


      elsif params[:do] == 'delete'
        @airport.destroy
        redirect_to :action => 'map' and return
      end
    end
  end

  def airport_group
    @group = Network::AirportGroup.where(:ID => params[:group_id]).includes(:airports).first
    if @group.nil?
      @group = Network::AirportGroup.new(:SiteID => current_network.site_id)
		end
		if @group.map_point.nil?
      @group.map_point = Network::MapPoint.new(:SiteID => current_network.site_id, :Lat => 42.696939, :Long => -71.158326)
    end

		# Initialize map
    @map = GMap.new('locations_map_panel')
    request_path = request.env['REQUEST_URI'] || ''
    @map.control_init(:large_map => true, :map_type => false)
    @map.set_map_type_init(GMapType::G_PHYSICAL_MAP)
    @map.center_zoom_init([@group.map_point.Lat,@group.map_point.Long],5)

		# icon_url = @group.map_point.uses_alternate_icon? ? @group.map_point.IconURL :
			# Tokens.for(current_network.site_id, 'MA_LOCATOR_ICON')
    @map.icon_global_init( GIcon.new( :image => icon_url,
    	:icon_size => GSize.new( 31,23 ), :icon_anchor => GPoint.new(15,10)),
    	"icon_#{@group.map_point.ID}")
    icon = "icon_#{@group.map_point.ID}".to_sym

    marker = PDMarker.new([@group.map_point.Lat, @group.map_point.Long], {:icon => icon, :draggable => true,
      :id => "map_marker_#{@group.map_point.airport.Code rescue @group.map_point.airport_group.ID}"},
      @group.map_point.Location)
    @map.declare_init(marker, "marker_#{@group.map_point.id}")
    @map.overlay_global_init(marker, 'marker')
    @map.event_init(marker, 'dragend', "function() {
    	lat_lon = marker.getLatLng();
    	$('Lat').value = lat_lon.lat();
    	$('Long').value = lat_lon.lng();
    }")


    if request.post?
      if params[:do] == 'update'
        if params[:description].blank?
          flash.now[:error] = 'Description cannot be blank.'

        else
          @group.Description = params[:description]

          @group.map_point.Location = params[:description]
          @group.map_point.Lat = params[:Lat]
          @group.map_point.Long = params[:Long]
	        @group.map_point.IconURL = params[:IconURL]
          @group.map_point.save
          @group.save
          redirect_to :action => 'map' and return

        end

      elsif params[:do] == 'delete'
        @group.destroy
        redirect_to :action => 'map' and return
      end
    end
  end

  def auto_complete_for_sites
		@sites = []
    render :layout => false
  end

  def delete_airport
    attraction = Network::Attraction.find(params[:id])
    airport = attraction.airports.find(params[:airport])

    attraction.airports.delete(airport) if airport
    render json: { available_airports: attraction.unattached_airports }
  end

  def delete_site
    if params[:groundnet_site_id]
      site = Network::Site.find(params[:groundnet_site_id])
    elsif params[:attraction]
      site = Network::Attraction.find(params[:attraction])
    end
    if site
      site.destroy
      flash[:message] = "Removed #{site.ProviderSiteId} from the list."
    end

    redirect_to config_home_url(site_id: current_site_id)
  end

  def index
    @sites = Network::Site.includes(:airports)
    @sites = @sites.where(SiteID: current_network.site_id).references(:airports)
    @sites = @sites + Network::Attraction.provider_sites_for(current_network.site_id)
    @sites = @sites.sort_by &:ProviderSiteId
  end

  def logout
    network_site_id = current_site_id.dup
  	reset_session
		set_login_session
    redirect_to configuration_url(network_site_id: network_site_id)
  end
	alias logout_user logout

  def map
    @airports = Network::Airport.where('SiteID' => current_network.site_id).order('Description ASC')
    @airport_groups = Network::AirportGroup.where('SiteID' => current_network.site_id).order('Description ASC')
    @regions = Network::MapRegion.where('SiteID' => current_network.site_id).order("Name ASC")
  end

  def passenger_types

    if request.post?
      if !params[:field]
        redirect_to :action => 'index' and return
      end

      non_blank_fields = params['field'].values.delete_if {|i| i.blank?}
      if non_blank_fields.length != non_blank_fields.uniq.length
        @error = "Each provider passenger type can only be referenced once."

      else
        Network::PassengerTypeTranslation.delete_all(["SiteID = ? and ProviderSiteID = ?", current_network.site_id, params[:site]])
        params['field'].each do |k,v|
          Network::PassengerTypeTranslation.create(
            :SiteID => current_network.site_id,
            :ProviderSiteID => params[:site],
            :ProviderPassengerField => v,
            :PassengerField => k
          )
        end
        flash[:message] = "Saved passenger type translations for #{params[:site]}"
        try_to_activate_site(Network::Site.where(:SiteID => current_network.site_id, :ProviderSiteId => params[:site]).first)
        redirect_to :action => 'index' and return
      end
    end

    @site_id = params[:site]
    @provider_pt = PassengerType.all(@site_id).dup.delete_if {|pt| pt.field_name == 'Passengers'}
    @go_pt = PassengerType.all(current_network.site_id).dup.delete_if {|pt| pt.field_name == 'Passengers'}
    @translation = Network::PassengerTypeTranslation.network_to_provider_hash(current_network.site_id, @site_id)

	rescue HudsonWebService::AccessDenied
    flash[:error] = "This site has been disabled and cannot be accessed."
    redirect_to :action => 'index' and return
  rescue HudsonWebService::ObsolescenceError, HudsonWebService::ErrorWithWebServiceError
    flash[:error] = "Site must be upgraded before updating passenger translations."
    redirect_to :action => 'index' and return
  end

  def map_regions
    @region = Network::MapRegion.where(:ID => params[:region_id]).first
    if @region.nil?
      @region = Network::MapRegion.new(:SiteID => current_network.site_id,
      	:Lat => 37.7, :Long => -110.38, :ZoomLevel => 3, :Name => "New Region")
    end

		# Initialize map
    @map = GMap.new('locations_map_panel')
    @map.control_init(:large_map => true, :map_type => false)
    @map.set_map_type_init(GMapType::G_PHYSICAL_MAP)
    @map.center_zoom_init([@region.Lat,@region.Long],@region.ZoomLevel)
    @map.event_init(@map, :move, "function() {
    	lat_lon = map.getCenter();
    	$('Lat').value = lat_lon.lat();
    	$('Long').value = lat_lon.lng();
    	$('ZoomLevel').value = map.getZoom();
    }")

    if request.post?
      if params[:do] == 'update'
        if params[:Name].blank?
          flash.now[:error] = 'Name cannot be blank.'

        else
          @region.Name = params[:Name]
          @region.Lat = params[:Lat]
          @region.Long = params[:Long]
          @region.ZoomLevel = params[:ZoomLevel]
          @region.save
          redirect_to :action => 'map' and return
        end

      elsif params[:do] == 'delete'
        @region.destroy
        redirect_to :action => 'map' and return
      end
    end
  end

  def scan_all_sites
    Network::Site.where('SiteID' => current_network.site_id).each do |site|
      try_to_activate_site(site)
    end
    redirect_to :action => 'index' and return
  end

  def services
    if request.post?

      non_blank_fields = params['field'].values.delete_if {|i| i.blank?}
      if non_blank_fields.length != non_blank_fields.uniq.length
        @error = "Each provider service extra can only be referenced once."

      else
        Network::ServiceTranslation.delete_all(["SiteID = ? and ProviderSiteID = ?", current_network.site_id, params[:site]])
        params['field'].each do |k,v|
          Network::ServiceTranslation.create(
            :SiteID => current_network.site_id,
            :ProviderSiteID => params[:site],
            :ProviderServiceType => v,
            :ServiceType => k
          ) unless v.blank?
        end
        flash[:message] = "Saved service translations for #{params[:site]}"
        try_to_activate_site(Network::Site.where(:SiteID => current_network.site_id, :ProviderSiteId => params[:site]).first)
        redirect_to :action => 'index' and return
      end
    end

    @site_id = params[:site]
    @provider_s = ActiveServices.for(@site_id)
    @go_s = ActiveServices.for(current_network.site_id)
    @translation = Network::ServiceTranslation.network_to_provider_hash(current_network.site_id, @site_id)

	rescue HudsonWebService::AccessDenied
    flash[:error] = "This site has been disabled and cannot be accessed."
    redirect_to :action => 'index' and return
  rescue HudsonWebService::ObsolescenceError, HudsonWebService::ErrorWithWebServiceError
    flash[:error] = "Site must be upgraded before updating service translations."
    redirect_to :action => 'index' and return
  end

  def service_extras
    if request.post?

      non_blank_fields = params[:field].values.delete_if {|i| i.blank?}
      if non_blank_fields.length != non_blank_fields.uniq.length
        @error = "Each provider service extra can only be referenced once."

      else
        Network::ServiceExtraTranslation.delete_all(["SiteID = ? and ProviderSiteID = ?", current_network.site_id, params[:site]])
        params[:field].each do |k,v|
          Network::ServiceExtraTranslation.create(
            :SiteID => current_network.site_id,
            :ProviderSiteID => params[:site],
            :ProviderServiceExtraField => v,
            :ServiceExtraField => k
          ) unless v.blank?
        end if params[:field]
        flash[:message] = "Saved service extra translations for #{params[:site]}"
        try_to_activate_site(Network::Site.where(:SiteID => current_network.site_id, :ProviderSiteId =>  params[:site]).first)
        redirect_to :action => 'index' and return
      end
    end

    @site_id = params[:site]
    @provider_se = ServiceExtras.for(@site_id)
    @go_se = ServiceExtras.for(current_network.site_id)
    @translation = Network::ServiceExtraTranslation.network_to_provider_hash(current_network.site_id, @site_id)

	rescue HudsonWebService::AccessDenied
    flash[:error] = "This site has been disabled and cannot be accessed."
    redirect_to :action => 'index' and return
  rescue HudsonWebService::ObsolescenceError, HudsonWebService::ErrorWithWebServiceError
    flash[:error] = "Site must be upgraded before updating service extra translations."
    redirect_to :action => 'index' and return
  end

  def site_advanced
  	@site_id = params[:site]
   	site =  Network::Site.where(:SiteID => current_network.site_id, :ProviderSiteId => params[:site]).first
    if request.post?
    	site.update_attribute('DisableOneWay', params[:disable_one_way])
      site.update_attribute('EnableCruise', params[:enable_cruise])
      flash[:message] = "Saved advanced config for #{params[:site]}"
      redirect_to :action => 'index' and return
    end
    @disable_one_way = site.DisableOneWay == 1
    @enable_cruise = site.EnableCruise == 1
  end

  def update_airports
    Network::Airport.where('SiteID' => current_network.site_id).each do |airport|
      airport.update_attribute(:Description, create_airport_description(airport.Code))
    end
    render :nothing => true
  end

  def update_site_details
    site = Network::Site.find(params[:id])
    if site
      site.update_attributes(
        :ServiceArea => params[:ServiceArea],
        :ServiceAreaID => params[:ServiceAreaID] )
      try_to_activate_site(site)
      flash[:message] = "Site configuration updated."
    else
      flash[:error] = "Site could not be updated."
    end

    redirect_to configuration_url
  end

  private

  def add_airports(multiaff_site)
    airports = Airports.for(multiaff_site.ProviderSiteId)
    airports.each do |a|
      next if a.code =~ /xx.*/ or a.code == 'ptp' or a.code == '---'

      airport = Network::Airport.where(:SiteID => current_network.site_id, :Code => a.code).first
      unless airport
        airport = Network::Airport.create(:Code => a.code, :Description => create_airport_description(a), :SiteID => current_network.site_id)
      end
      multiaff_site.airports << airport
    end
  end

  def create_airport_description(airport_or_code)
    if airport_or_code.is_a? WSAirport
      code = airport_or_code.code
    else
      code = airport_or_code
    end

    iata_lookup = Network::Airport.find_by_sql(["select City, State, Country, AirportName, IATA from IATALookup where UPPER(IATA) = UPPER(?)", code])
    unless iata_lookup.empty?
      iata = iata_lookup.first
      description = iata.City
      if !iata.State.blank?
        description << ", #{iata.State}"
      elsif !iata.Country.blank?
        description << ", #{iata.Country}"
      end
      description << " (#{iata.IATA})"
    else
      description = " (#{code})"
    end
    description
  end

  # Edits must be done on web10 (the master replication server)
  def ensure_web10
    if request.host !~ /web10.hudsonltd.net/
      new_url = request.url.sub(request.host, 'web10.hudsonltd.net')
      redirect_to new_url
    end
  end

  def generate_total_site_list
    last_updated = session[:site_list][:last_updated] rescue 1.year.ago
    sites = session[:site_list][:sites] rescue []
    if last_updated < 1.day.ago or sites.empty?
      sites = SiteInfo.find_for_all(:all).collect {|s| s.SiteId rescue nil}.delete_if {|s| s =~ /v\d\d+.*$/ or s =~ /test$/}.uniq.compact.sort
      session[:site_list] = {:last_updated => Time.now, :sites => sites}
    end
    session[:site_list][:sites]
  end

	def login_required
		@user = session[:hudson_login][:user] rescue nil
		if @user.nil? || @user.site_id != current_network.site_id
			redirect_to(login_url)
			return
		elsif !@user.has_priv?(219)
			redirect_to(login_url(:error => "You are not allowed access to Multiaffiliate Configuration."))
			return
		end
	rescue
		redirect_to(login_url(site_id: current_network.site_id))
		return
	end

  def set_error(site, error_message)
    site.update_attributes(:ErrorMessage => error_message, :ErrorOccurredAt => Time.now, :Active => false)
  end

  def try_to_activate_site(groundnet_site)
    error_occurred = false
    site_id = groundnet_site.ProviderSiteId

    # Must have Service translations.
    set = Network::ServiceTranslation.for(current_network.site_id, site_id)
    if set.empty?
      set_error(groundnet_site, "No service translations.")
      error_occurred = true
    end

    # Does this site service airports that other sites service?
    # If so, flag this and other sites that now need to define a service area.
    groundnet_site.airports.each do |airport|
      next if airport.sites.count == 1
      airport.sites.each do |a_site|
        if a_site.ServiceArea.blank? and (a_site == groundnet_site or a_site.ErrorMessage.blank?)
          set_error(a_site, "Service Area is required.")
          error_occurred = true if a_site == groundnet_site
        end
      end
    end

    unless error_occurred
      groundnet_site.update_attributes(:Active => true, :ErrorMessage => nil, :ErrorOccurredAt => nil)
    end
  end

	def set_login_session
		if session[:hudson_login].nil? || session[:hudson_login][:input].nil? ||
        session[:hudson_login][:user].try(:site_id) != current_network.site_id
			session[:hudson_login] = {:input => {
				:site_id => current_network.site_id,
				:redirect_url => config_home_url(site_id: current_network.site_id),
				:app_title => "Multiaffiliate Configuration",
				:privileges => "219"
			}}
		end
	end
end
