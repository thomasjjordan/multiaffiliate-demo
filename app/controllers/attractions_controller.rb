class AttractionsController < ApplicationController
  include ActionView::Helpers::NumberHelper

  before_action :no_caching, only: [:index, :validate]
  # before_action :allow_caching, only: 'widget'
  before_action :attach_group_profile

  display_tokens(
    'MA_ATTRACT_' => {
      att_add_label: { 'CAP_ADD' => 'Add to cart' },
      att_date_label: { 'CAP_DATE' => 'Date' },
      att_details_head: { 'HEAD_DETAILS' => 'Details' },
      att_pudloc_label: { 'CAP_PUDLOC' => 'Pickup location' },
      att_instructions_head: { 'HEAD_INSTR' => 'Instructions' },
      att_load_times_label: { 'CAP_LOAD_TIMES' => 'See available times' },
      att_more_details_label: { 'CAP_MORE_DETAILS' => 'More details...' },
      att_special_info_head: { 'HEAD_SPECIALINFO' => 'Special Information' },
      att_starting_price_label: { 'CAP_START_PRICE' => 'Prices starting at' },
      att_terms_conditions_head: { 'HEAD_TERMS' => 'Terms and Conditions' },
      att_times_label: { 'CAP_TIMES' => 'Tour time' },
      att_yard_departure_label: 'CAP_YARD_TIME',
      attract_h1: { 'H1' => 'Attractions' },
      attract_header: 'HEADR',
      no_attractions_available: {
        'MSG_NO_ATTRACTIONS' =>
          'No attractions are currently available in this area.' } })

  tokens(
    'MA_ATTRACT_' => {
      att_price_label: { 'CAP_PRICE' => 'Price' },
      no_attraction_available_error: {
        'ERR_ATTRACT_UNAVAIL' =>
          'This attraction cannot be booked on your chosen date with ' \
          'this passenger count.' },
      no_times_available_error: {
        'ERR_ATTRACT_NOTIMES' =>
          'No times are available for this attraction on your chosen date ' \
          'with this passenger count.' } })

  def details
    @attraction = attraction(params, params[:global_code] || params[:key])
  end

  def index
    att_site_id = determine_attraction_site_id(params)
    @attractions = Attraction.all(att_site_id, filters: params[:filters])
    @attractions = @attractions.group_by(&:type_label).sort.to_h

    respond_to do |format|
      format.html do
        render(text: no_attractions_available) if @attractions.empty?
      end
      format.json { render json: @attractions.values.flatten }
    end
  end

  def update_info
    if no_passengers_selected
      render json: { error: no_attraction_available_error }
      return
    end

    attraction = attraction(params, params[:DropOffKey])
    params[:FlightTOD] = Chronic.parse(params[:FlightTOD].to_s).beginning_of_day
    params[:PickupTOD] = params[:FlightTOD]
    service = current_session.attraction_service(attraction,
                                                 strip_unwanted_params(params))
    unless service
      render json: { error: no_attraction_available_error }
      return
    end

    times = service.times.collect do |time|
      { time: time.time,
        yard_arrival: time.yard_arrival_time,
        yard_departure: time.yard_departure_time }
    end

    render json: {
      fare: number_to_currency(service.fare),
      times: times }
  end

  def validate
    if no_passengers_selected
      fail MultiaffiliateReservation::AttractionServiceError
    end

    attraction = attraction(params, params[:DropOffKey])
    if attraction.display_times?
      params[:FlightTOD] = Chronic.parse(params[:PickupTOD].to_s)
    else
      params[:FlightTOD] = Chronic.parse(params[:FlightTOD].to_s).beginning_of_day
    end
    params[:PickupTOD] = params[:FlightTOD]

    current_session.start_a_new_trip!
    current_trip.set_attraction_info(attraction, strip_unwanted_params(params))
    current_trip.is_ready_for_booking!
    current_session.start_a_new_trip!

    if params[:widget] && params[:host].present?
      flash[:force_redirect] = cart_url
      render inline: "<script>parent.goto('#{parent_cart_page}')</script>"
    else
      redirect_to(cart_url) && return
    end

  rescue MultiaffiliateReservation::AttractionServiceError
    flash[:error] = no_attraction_available_error
    redirect_to :back
  end

  def widget
    if request.post?
      validate
      return
    end

    @attraction = attraction(params, params[:key])
    @height_padding = 0
    render(text: no_attractions_available) unless @attraction
  end

  private

  def a_from_t(transpo_site_id)
    Network::Attraction.attraction_site_from_transpo_site(current_site_id,
                                                          transpo_site_id)
  end

  def attach_group_profile
    return unless params[:code].present?

    super

    if current_session.group_id.present?
      flash.now[:notice] = "Code #{params[:code]} has been applied."
    end
  end

  def attraction(params, attraction_code)
    att_site_id = determine_attraction_site_id(params, attraction_code)
    att_site_id = att_site_id.first if att_site_id.is_a?(Array)
    Attraction.find(att_site_id, attraction_code)
  end

  def determine_attraction_site_id(params, key = nil)
    if params[:global_code]
      params[:global_code].split(Attraction.global_code_separator)[0]
    elsif key =~ /\|/
      key.split(Attraction.global_code_separator)[0]
    elsif params[:attraction_site_id]
      params[:attraction_site_id]
    elsif params[:provider_site_id]
      a_from_t(params[:provider_site_id])
    else # All attraction sites
      Network::Attraction.provider_site_ids_for(current_site_id)
    end
  end

  def no_passengers_selected
    passenger_fields = %w(Passengers AltPassengers
                          AltPassengers2 AltPassengers3)
    total = passenger_fields.inject(0) do |sum, field|
      sum.to_i + params[field].to_i
    end
    total.zero?
  end

  def strip_unwanted_params(params)
    params.except(:authenticity_token, :provider_site_id, :utf8, :original_fare,
                  :controller, :action, :site_id, :host, :widget,
                  :attraction_site_id, :global_code)
  end
end
