class ServicesController < ApplicationController
  before_action :no_caching, :only => %i(index)
  before_action :should_user_be_here

  display_tokens 'MA_SVCS_' => {
            add_to_cart_msg: {
              'CART_ALERT' => "These reservation(s) will be added " +
                "to your shopping cart. You will " +
                "not be able to update your details without starting " +
                "over."},
            continue_btn_edit_text: {'BTN_CONT_EDIT_CAP' => 'Save'},
            continue_btn_new_text: {'BTN_CONT_CAP' => 'Continue'},
            leg_index_summary_prefix: {
              'CAP_LEG_SUM_PREFIX' => '<span class="l-kern">L</span>eg'},
            location_header_a: {'DO_TTL' => 'Drop-off location'},
            location_header_d: {'PU_TTL' => 'Pickup location'},
            pickup_header: {'PUTIME_TTL' => 'Pickup'},
            oneway_services_h1: {'OW_H1_SUFFIX' => 'Available Services'},
            roundtrip_d_services_h1: {'RT_D_SUFFIX' => 'Available Services for Your Departure'},
            roundtrip_a_services_h1: {'RT_A_SUFFIX' => 'Available Services for Your Arrival'},
            service_selection_label: {'CAP_SELECT_SVC' => 'Select'},
            show_all_services_label: {'CAP_SHOW_SVCS' => 'Select a different service'},
            show_details_label: {'CAP_SHOW_DTLS' => 'Change service details'},
            summary_pickup_label: {'CAP_SUM_PU' => 'Pickup at'},
            tip_calc_tip_label: {'CAP_TC_TIP' => 'Tip'},
            tip_calc_calculate_label: {'CAP_TC_CALCULATE' => 'Calculate'},
            tip_calc_common_tips: {'CAP_TC_COMMON_TIPS' => 'Common tips'},
            tip_calc_amount_label: {'CAP_TC_AMOUNT' => 'Tip amount:'},
            tip_calc_update_label: {'CAP_TC_UPDATE' => 'Update'},
            tip_calculator_label: {'CAP_TIP_CALC' => 'Tip calculator'}
          },
          'MA_DETAILS_' => {
            flight_info_header: {'FLIGHT_TTL' => 'Your flight'},
            network_flight_city_a_label: 'FLTCITY_ARV',
            network_flight_city_d_label: 'FLTCITY_DEP',
            other_details_header: {'OTHER_TTL' => 'Other Information'}
          },
          flight_city_a_label: 'AFLIGHTCITYDESC',
          flight_city_d_label: 'DFLIGHTCITYDESC',
          no_fare: {'FARENOTAV' => 'Fare not available'}

  tokens 'MA_DETAILS_' => {
            raw_network_flight_city_a_label: 'FLTCITY_ARV',
            raw_network_flight_city_d_label: 'FLTCITY_DEP'
          },
          raw_flight_city_a_label: 'AFLIGHTCITYDESC',
          raw_flight_city_d_label: 'DFLIGHTCITYDESC'


  def flight_cities
    leg = current_trip.legs[params[:leg_index].to_i]
    cities = filtered_flight_cities(leg, params[:term])
    render json: cities.collect {|fc| {label: fc, value: fc}}
  end

  def index
    @leg_index = params[:leg_index].to_i || 0
    @leg = current_trip.legs[@leg_index]
  end

  def update
    index = params[:leg_index].to_i
    current_trip.replace_service_info(index, params[:leg])
    if current_trip.valid_service_info?(index)
      current_trip.recalculate_fares(index)

      if current_trip.roundtrip? && index.zero? && !params[:edit]
        current_trip.dup_service_info_for_second_leg
      elsif current_trip.cruise? && [0,1].include?(index) && !params[:edit]
        current_trip.dup_ship
      end

      # Was this the last leg? Do we add it to the cart?
      if current_trip.legs.length == index+1
        current_trip.is_ready_for_booking!
        current_session.start_a_new_trip!

        render json: { redirect_to: cart_url }
        return

      else
        @leg_index = index
        @leg = current_trip.legs[@leg_index]
        previous_html = render_to_string(action: 'index', layout: false)
        @leg_index = index + 1
        @leg = current_trip.legs[@leg_index]
        new_html = render_to_string(action: 'index', layout: false)
        render json: {
          previousHtml: previous_html,
          html: new_html
        }
        return
      end

    else
      flash.now[:error] = general_form_error
      @leg_index = index
      @leg = current_trip.legs[@leg_index]
      render json: {
        errorHtml: render_to_string(action: 'index', layout: false)
      }
    end
  end

	private

  def filtered_flight_cities(res, filter_term)
    cache_key = "services_controller:filtered_flight_cities:#{current_site_id}"
    cities = Rails.cache.fetch(cache_key) do
      current_network.airports.collect &:Description
    end

    # Remove cities not matching our search terms
    search_terms = filter_term.downcase.split
    cities.delete_if do |city|
      search_terms.none? {|term| city.downcase.include?(term)}
    end

    # Make sure the currently selected flight city is in the list.
    if res.FlightCity.present? && !cities.include?(res.FlightCity)
      cities.unshift(res.FlightCity)
    end

    cities.compact
  end

  def should_user_be_here
    redirect_to(home_url) unless current_trip.ready_for_services?
  end
end
