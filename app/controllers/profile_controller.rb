class ProfileController < ApplicationController
  display_tokens 'MA_GRPLOGIN_' => {
        group_login_header: 'HEADER',
        group_login_label: {'LABEL' => 'Login' },
        group_login_submit: {'BTN_CONTINUE' => 'Continue'} },
      'MA_PRLOGIN_' => {
        login_forgot_pw_label: {'CAP_FORGOT_PW' => 'Forgot your password?'},
        login_header: 'HEADER',
        login_success: {'MSG_LOGIN_SUCCESS' => 'Welcome back, you are now logged in.'}
      }

  # Called directly (no template)
  def group_login
    if params[:code].blank? || !login_group_profile!(params[:code])
      flash[:error] = group_login_error
    end

    params[:host].present? ?
      redirect_to("#{params[:host]}" + 'reservations') :
      redirect_to(root_path)
  end

  def group_login_window
    if request.post? && params[:code].present?
      if login_group_profile!(params[:code])
        render inline: "<script>parent.goto('reservations')</script>"
      else
        flash.now[:error] = group_login_error
      end
    end
  end

  def home
    unless current_user_profile.present?
      render inline: "<script>parent.goto('preferred_riders')</script>"
    end
  end

  def login_modal
    @username = params[:username]
    unless request.post? && @username.present?
      render json: { error: login_error(no_span: true) }
      return
    end

    if login_user_profile!(@username, params[:password])
      flash[:notice] = login_success
      if params[:redirect_to]
        render json: { redirect_to: params[:redirect_to] }
      else
        render inline: "<script>parent.goto('reservations')</script>"
      end
    else
      render json: { error: login_error(no_span: true) }
    end
  end

  def login_window
    @username = params[:username]
    return unless request.post?

    unless @username.present?
      flash.now[:error] = login_error
      return
    end

    if login_user_profile!(@username, params[:password])
      flash[:notice] = login_success
      render inline: "<script>parent.goto('reservations')</script>"
    else
      flash.now[:error] = login_error
    end
  end

  def logout
    current_session.logout_profiles!

    if params[:goto]
      flash[:notice] = logged_out_msg
      redirect_to params[:goto]
    else
      redirect_to_home
    end
  end

  def logout_group
    current_session.detach_coupon

    if params[:goto]
      flash[:notice] = logged_out_msg
      redirect_to params[:goto]
    else
      redirect_to_home
    end
  end

  def pr_redirect
    if current_user_profile.present?
      redirect_to params[:host] + 'preferred_riders_home'
    else
      redirect_to params[:host] + 'preferred_riders'
    end
  end

  def reload_profile
    if params[:username].present?
      login_user_profile!(params[:username], params[:password])
    end
    render nothing: true
  end
end
