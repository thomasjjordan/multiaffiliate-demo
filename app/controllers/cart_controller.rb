require 'creditcard'

class CartController < ApplicationController
  before_action :no_caching
  before_action :check_for_trips_to_book, except: ['receipt']

  display_tokens 'MA_CART_' => {
            apply_coupon_button: {'BTN_COUPON' => 'Apply'},
            apply_coupon_label: 'CAP_COUPON',
            cart_add_res_label: {'CAP_ADD_RES' => 'Add a new reservation'},
            cart_attraction_button: {'CAP_ATTRACT_MORE' => 'Learn more...'},
            cart_attraction_pickup_time: {'CAP_ATTRACT_PUTIME' => 'Pickup '},
            cart_delete_label: {'CAP_DELETE' => 'Remove'},
            cart_delete_success: {
              'DELETE_SUCCESS' => 'Your trip was successfully deleted.'
            },
            cart_empty_label: {'CAP_EMPTY' => 'Empty cart'},
            cart_empty_success: { 'EMPTY_SUCCESS' => 'Your cart was emptied.' },
            cart_flight_city_cap_a: {
              'CAP_FLTCITY_A' => 'Book additional transportation in %FLIGHTCITY%' },
            cart_flight_city_cap_d: {
              'CAP_FLTCITY_D' => 'Book additional transportation in %FLIGHTCITY%' },
            cart_h1: {'H1' => 'Shopping cart'},
            cart_header: 'HEAD',
            cart_no_items: {'CAP_CART_EMPTY' => 'Your cart is empty.'},
            cart_total_label: {'CAP_TOTAL' => 'Total:'},
            checkout_label: {'CAP_CHECKOUT' => 'Checkout'},
            coupon_applied_suffix: {'CAP_COUPON_APPLIED_SUFFIX' => 'applied'},
            remove_coupon_label: {'CAP_REMOVE_COUPON' => 'remove' }
            },
          'MA_CHECKOUT_' => {
            book_button: {'BTN_BOOK' => 'Book your trip'},
            book_loading_msg: {'BOOK_LOADING' => 'Please wait...'},
            booking_error: {'ERR_BOOK' => 'We could not book your trip.'},
            checkout_alert_msg: 'CHECKOUT_ALERT',
            checkout_h1: {'H1' => 'Checkout' },
            checkout_header: 'HEAD',
            contact_header: {'HEAD_CONTACT' => 'Passenger information'},
            email_subs_header: {'HEAD_EMAILSUBS' => 'Email subscriptions'},
            name_label: {'CAP_NAME' => 'Name'},
            payment_header: {'HEAD_PAYMENT' => 'Payment'},
            points_description: {'CAP_POINTS' =>
              'Points will be awarded to the following programs:'},
            points_header: {'HEAD_POINTS' => 'Preferred rider points'},
            pt_direct_bill_label: {'CAP_PT_DIRECTBILL' => 'Direct bill'},
            pt_credit_card_label: {'CAP_PT_CC' => 'Credit card'},
            pt_cash_label: {'CAP_PT_CASH' => 'Cash'}
          },
          'MA_RECEIPT_' => {
            book_another_label: {'CAP_BOOK_ANOTHER' => 'Book another trip'},
            receipt_logout_label: {'CAP_LOGOUT' => 'Logout'},
            preferred_rider_button: {'CAP_PRBUTTON' => 'Join today'},
            preferred_rider_spiel: 'PREF_RIDER',
            print_label: {'CAP_PRINT' => 'Print'},
            receipt_h1: {'H1' => 'Receipt'},
            receipt_header: 'HEAD'
          },
          credit_card_label: {'CCNUMDESC' => 'Credit card number'},
          credit_card_exp_label: 'CCEXPMONDESC',
          credit_card_name_label: 'CCDETAILSDESC',
          credit_card_zip_label: 'CCZIPDESC',
          ccvv_label: 'CCVVDESC',
          email_label: 'PICKUPEMAILDESC',
          terms_and_conditions: 'TERMCONDDESC',
          telephone_label: 'TELEPHONEDESC',
          telephone2_label: 'ALTTELEPHONEDESC',
          terms_error: 'TERMCONDERR',
          validate_email_label: {'PICKUPEMAILCDESC' =>
            'Please enter your email address again to confirm'}

  tokens 'MA_CART_' => {
            bad_coupon_error: {
              'ERR_INVALID_COUPON' => 'This coupon code is not available.'},
            cart_add_res_destination: 'CAP_ADD_RES_URL',
            cart_delete_confirm: {
              'CONFIRM_DELETE' =>
                'You are about to remove this item from your cart, are you sure?'
            },
            cart_empty_confirm: {
              'CONFIRM_EMPTY' => 'You are about to empty your cart, are you sure?'
            },
            cart_hide_attractions_attract: 'HIDE_ATTATT'
         },
          'MA_RECEIPT_' => {
            hide_book_another_link: 'HIDE_BOOKANOT',
            show_preferred_rider_button: 'SHOW_PRBUTTON'
         },
         tripit_enabled: 'NETWORK_TRIPIT_ENABLED',
         bare_terms_and_conditions: 'TERMCONDDESC'


  def apply_coupon
    coupon = params[:coupon].upcase
    unless coupon.present?
      render json: {}
      return
    end

    unless current_session.valid_coupon?(coupon)
      render json: { error: bad_coupon_error }
      return
    end

    unless params[:approved]
      issues = cart.negatively_affected_items_if_profile_applied(coupon)
      if issues.present?
        render json: { issues: issues }
        return
      end
    end

    current_session.attach_coupon(params[:coupon])
    render json: { redirect_to: request.env["HTTP_REFERER"] }
  end

  def remove_coupon
    unless current_session.coupon_applied?
      render json: {}
      return
    end

    unless params[:approved]
      issues = cart.negatively_affected_items_if_profile_removed
      if issues.present?
        render json: { issues: issues }
        return
      end
    end

    current_session.detach_coupon
    render json: { redirect_to: request.env["HTTP_REFERER"] }
  end

  def book
    current_session.book!(params[:ccvv])

    current_session.copy_cart_for_receipt
    cart.empty!
    current_session.clear_group_profile!
    current_session.clear_contact_and_payment_info!
    current_session.start_a_new_trip!
    output_stats(current_session.receipt_cart)
    redirect_to receipt_url

  rescue Session::BookingError => e
    error_msg = "#{booking_error}<br>#{system_error_message}".html_safe
    redirect_to checkout_url, flash: {error: error_msg}

  rescue Exception => e
    if Rails.env.development?
      raise
    else
      logger.error e
      send_exception_report(e)
      redirect_to checkout_url, flash: {error: system_error_message}
    end
  end

  def checkout
    current_session.payment_type = default_payment_type(current_session.payment_type)
    @email_subs = EmailSubscriptions.for(current_site_id)
    @terms = flash[:terms_conditions].to_i
  end

  def delete
    trip = cart.items_ready_for_booking[params[:trip].to_i]
    cart.delete_item(trip)
    flash[:notice] = cart_delete_success
    redirect_to :back
  end

  def empty
    # If we have attached config for the most recent item in the cart.
    if current_attached_config && current_attached_config.new_url.present?
      external_url = current_attached_config.new_url
    end

    # If the customer has defined a custom link, we want to update the parent.
    if cart_add_res_destination.present?
      external_url = cart_add_res_destination
    end

    flash[:notice] = cart_empty_success
    cart.empty!
    current_session.start_a_new_trip!

    if external_url
      render inline: "<script>window.top.location.href = '#{external_url}';</script>"
      return
    end

    redirect_to :back
  end

  # Create a new trip and send the user back to the first res page.
  # When trip_index and leg_index are passed in the relevant leg will be
  # used to default some values.
  def new_trip
    base_trip = bookable_trips[params[:trip_index].to_i]
    base_leg = base_trip.legs[params[:leg_index].to_i]
    current_session.start_a_new_trip!
    leg = current_trip.legs.first

    if base_leg
      leg.Direction = base_leg.departure? ? 'A' : 'D'
      airport_key = airport_from_flight_city(base_leg.FlightCity)
      leg.set_airport_and_primary_site airport_key
      leg.airport_key = airport_key if leg.airport_key.blank?

      # Default fields.
      %i( FlightTOD FlightType Passengers AltPassengers AltPassengers2
          AltPassengers3 FlightNumber Airline ).each do |field|
        leg.send("#{field}=", base_leg.send(field))
      end

      # Default the new leg's FlightCity from the airport's description.
      airport = current_network.airports.find do |a|
        a.Code.downcase == base_leg.airport_key.downcase
      end
      leg.FlightCity = airport.Description if airport.present?
    end

    redirect_to oneway_url
  end

  def receipt
    @receipt_cart = current_session.receipt_cart
    (redirect_to_home; return) if @receipt_cart.nil?
  end

  def validate_checkout
    data = params[:cart]
    if data.blank?
      redirect_to(checkout_url) && return
    end

    # Force payment type if only one option is available.
    # This prevents people from passing in a direct bill
    # when it's not an option.
    data[:payment_type] = default_payment_type(data[:payment_type].to_i)
    data[:CCExp] = Time.local(data[:CCExp][:year], data[:CCExp][:month]) if data[:CCExp]
    data.delete(:CCNumber) if data[:CCNumber] =~ /^\*\*\*\*/
    flash[:terms_conditions] = (data[:terms_conditions] ||= 0)
    flash[:ccvv] = data[:CCVV]
    flash[:mailing_list] = params[:mailing_list] || {}

    current_session.replace_checkout_info(data)
    if current_session.invalid?
      flash[:error] = general_form_error
      redirect_to :back
      return
    end

    # Add to mailing lists.
    if params[:mailing_list]
      params[:mailing_list].each do |key, value|
        unique_email_addresses(bookable_trips).each do |email|
          add_to_mailing_list(email, key)
        end
      end
    end

    redirect_to book_url(ccvv: data[:CCVV])

  ensure
    current_session.clear_ccvv! # We cannot store CCVV
  end

  private

  # Attempt to find an airport code within the given flight city.
  # Returns nil if none found.
  def airport_from_flight_city(flight_city)
    if (flight_city =~ /\((\w\w\w)\)/) || (flight_city =~ /^(\w\w\w(?:\s|$))/)
      return $1.downcase
    end
    nil
  end
  helper_method :airport_from_flight_city

  def cc_required?
    current_session.cc_required?
  end
  helper_method :cc_required?

  def check_for_trips_to_book
    return unless bookable_trips.empty?

    # If we have attached config
    if current_attached_config && current_attached_config.new_url.present?
      external_url = current_attached_config.new_url
      render inline: "<script>window.top.location.href = '#{external_url}';</script>"
    else
      redirect_to_home
    end
  end

  def collected_payment_types
    current_session.collected_payment_types
  end
  helper_method :collected_payment_types

  def default_payment_type(type)
    type ||= 1
    unless collected_payment_types.collect(&:payment_type).include?(type)
      type = collected_payment_types.first.payment_type
    end
    type
  end

  def offer_cash?
    # Yes if all legs allow cash.
    collected_payment_types.find_all{|pt| pt.payment_type == 0}.length == total_leg_count
  end
  helper_method :offer_cash?

  def offer_cc?
    # Yes if any legs allow cc
    !!collected_payment_types.find{|pt| pt.payment_type == 1}
  end
  helper_method :offer_cc?

  def offer_db?
    # Yes if all legs allow db.
    collected_payment_types.find_all{|pt| pt.payment_type == 3}.length == total_leg_count
  end
  helper_method :offer_db?

  def only_offer_cash?
    !offer_cc? && !offer_db? && offer_cash?
  end
  helper_method :only_offer_cash?

  def only_offer_cc?
    offer_cc? && !offer_db? && !offer_cash?
  end
  helper_method :only_offer_cc?

  def only_offer_db?
    offer_db? && !offer_cc? && !offer_cash?
  end
  helper_method :only_offer_db?

  # Adds some statistics about the booked shopping cart to the log.
  # This lets us run some quick/dirty commands on our logs to determine
  # the type of bookings taking place.
  def output_stats(cart)
    logger.info "]] Booked reservation stats"

    logger.info "]] Trip types:"
    cart.items_ready_for_booking.each do |trip|
      logger.info trip.trip_type
    end

    logger.info "]] Reservation stats:"
    cart.items.collect(&:legs).flatten.compact.each do |leg|
      logger.info "LEG: site:#{leg.home_site_id} from:#{leg.PickupKey} to:#{leg.DropOffKey}"
    end
    logger.info "]] End stats"
  end

  def terms_and_conditions?
    current_session.terms_and_conditions?
  end
  helper_method :terms_and_conditions?

  def total_leg_count
    bookable_trips.inject(0) {|sum, item| sum + item.legs.length}
  end
  helper_method :total_leg_count

  def unique_email_addresses(trips)
    addresses = trips.collect {|trip| trip.legs.collect &:EmailAddr }
    addresses = addresses.flatten.uniq
    addresses
  end
  helper_method :unique_email_addresses
end
