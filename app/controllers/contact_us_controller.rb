class ContactUsController < ApplicationController
 	layout nil

	# Accept incoming form paramters that are intended to go to the addnotes web service.
	# Except for options[*] parameters, all parameters are expected to go straight to addnotes.
	# NoteType and options[on_success_url] are required parameters.
	def process_form
		# Required parameters
		if params[:NoteType].blank? || params[:FileAlias].blank? || params[:options][:on_success_url].blank?
			render(:text => "NoteType, FileAlias and options[on_success_url] are required parameters.") && return
		end

		# Store and remove from the parameters list "options[on_success_url]" and "options[on_error_url]"
		@success_url = params[:options][:on_success_url]
		@error_url = params[:options][:on_error_url]
		@error_url = @success_url if @error_url.blank?

		# Pass parameters list to the addnotes web service.
		in_params = params.dup
		in_params.delete(:controller); in_params.delete(:action); in_params.delete(:network_site_id)
		in_params.delete(:options); in_params.delete(:host)

		confirmation_id = Note.add(current_site_id, in_params)

		success_uri = URI.parse(@success_url)
		@success_url = success_uri.query ? "#{@success_url}&confirmation=#{confirmation_id}" : "#{@success_url}?confirmation=#{confirmation_id}"

		redirect_to @success_url

	rescue Exception => e
		logger.info("Exception raised: #{e.to_s}")
		send_exception_report(e)

		error_uri = URI.parse(@error_url)
		@error_url = error_uri.query ? "#{@error_url}&error_msg=#{e.to_s}" : "#{@error_url}?error_msg=#{e.to_s}"
		in_params.each {|key,value| @error_url += "&#{key}=#{value}" }

		redirect_to @error_url
	end
end
