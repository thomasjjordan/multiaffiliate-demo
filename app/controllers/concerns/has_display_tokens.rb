TOKEN_CACHE ||= []

# Usage:
#
# In controller:
#   display_tokens token_method_name: {'TOKEN' => 'default value'}
#   display_tokens 'TOKEN_PREFIX_' => {
#       token_method1: 'TOKEN1',
#       token_method2: {'TOKEN2' => 'default 2'} }

# In view:
#   Here's my token: <%= token_method1 %>
#   Here's my token, with help text positioned left: <%= token_method1 position: :left %>

module HasDisplayTokens
  include HasSite
  include HasGroupProfile
  extend ActiveSupport::Concern

  included do
    # Gather the tokens used within the application.
    # Usage:
    # symbol keys are considered methods
    # string keys are token prefixes/tokens
    # string values are default text.
    # example:
    # tokens 'BSLYT_HOME_' => {
    #         test_method: 'TEST_METHOD',
    #         second_meth: {'SEC_METH' => 'Second Default'} }
    #     normal_method: 'NORMAL_METHOD_TOKEN'
    def self.tokens(hash)
      traverse_tokens('', hash, no_span: true)
    end

    def self.display_tokens(hash)
      traverse_tokens('', hash)
    end

    private
      def self.add_token_to_precache_list(name, method_name)
        @@tokens ||= {}
        ::TOKEN_CACHE << name unless ::TOKEN_CACHE.include? name
        @@tokens[method_name] = name
      end


      def self.traverse_tokens(prefix, hash, options = {})
        hash.each do |k, v|
          if k.is_a? String
            traverse_tokens(prefix+k, v, options)
          else
            method_name = k
            if v.is_a? String
              token_name = prefix+v
              default = ''
            else
              token_def = v.find {|key, val| key.is_a? String}
              token_name = prefix+token_def[0]
              default = token_def[1]
            end

            add_token_to_precache_list(token_name, method_name)
            define_method method_name do |method_options = {}, token_options = {}|
              method_options ||= {}
              token(token_name, options.merge(method_options.merge(default: default)), token_options)
            end
            helper_method method_name.to_sym if try :helper_method
          end
        end
      end
  end


  private

    def precache_tokens
      Tokens.for(current_site_id, ::TOKEN_CACHE, :GroupAlias => current_group_id)
      @tokens_precached = true
    end

    def tokens_precached?
      @tokens_precached
    end

    # Supported options:
    # :default => Only works if a single token was passed in. Defines the default return value if the token is  undefined.
    def token(token, options = {}, token_options = {})
      precache_tokens if !tokens_precached?

      position = options[:position] || :right

      val = Tokens.for(current_site_id, token, token_options)
      val = (!val.is_a?(Array) && val.blank?) ? options[:default] : val
      if options[:no_span]
        val.to_s.html_safe
      else
        "<span class='token' data-position='#{position}' data-intro='#{token}'>#{val}</span>".html_safe
      end
    end
end
