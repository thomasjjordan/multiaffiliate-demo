require 'net/http'

class InfoController < ApplicationController
	display_tokens 'MA_MAILLIST_' => {
	      mailing_list_error: {'ERROR' =>
	      		'We were unable to add you to our mailing list.'},
	      mailing_list_header: 'HEADER',
	      mailing_list_content: {'CONTENT' => 'Email address'},
	      mailing_list_submit: {'BTN_CONTINUE' => 'Join'},
	      mailing_list_success: {'SUCCESS' =>
	      		"You've been successfully added to our mailing list."}
	      }

	def join_mailing_list_window
		@email = params[:email]
		if @email.present? && status = add_to_mailing_list(@email)
			flash.now[:notice] = mailing_list_success
		elsif @email.present?
			flash.now[:error] = mailing_list_error
		end
	end

	def blank
		render text: ''
	end

	# Client will call this URL letting us know if it thinks something is
	# blocking Google Analytics. Won't work with Universal Analytics
	def block_check
		blocked = params[:blocked].to_s == 'true' ?
			"Google Analytics is probably being blocked." :
			"Google Analytics is probably working."

		Rails.logger.info ">> GA CHECK: #{blocked}"
		render text: ''
	end

	def version
		render text: RELEASE_VERSION
	end

	def error
		@notice = params[:notice] || flash[:notice]
	end
end
