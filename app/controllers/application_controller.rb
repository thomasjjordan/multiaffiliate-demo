class ApplicationController < ActionController::Base
  protect_from_forgery
  force_ssl unless Rails.env.development?
  include HasSite
  include HasDisplayTokens

  # Allow session keys to be transmitted with GET params when necessary.
  include CookielessSessions::EnabledController

  # Increase security when using url-based sessions.
  include Frikandel::LimitSessionLifetime
  include Frikandel::BindSessionToIpAddress
  skip_before_filter :validate_session_ip_address, :validate_session_timestamp,
                     unless: proc { params[Rails.application.config.session_options[:key]].present? }

  before_action :cookies_required, except: ['cookies_test']
  before_action :check_for_bad_site
  before_action :accept_network_site_id
  before_action :attach_config
  before_action :set_time_zone
  before_action :allow_iframe_requests

  unless Rails.env.development?
    rescue_from Exception, with: :render_500
    rescue_from ActiveRecord::RecordNotFound, ActionController::RoutingError,
                ActionController::UnknownController,
                ::AbstractController::ActionNotFound, with: :render_404
  end

  display_tokens(
    'MA_GRPLOGIN_' => {
      group_login_error: { 'ERR_GRPLOGIN' => 'Your group code is invalid.' } ,
      login_error: { 'ERR_INDLOGIN' => 'Invalid username or password.' },
      login_user_label: { 'USERLBL' => 'Email address' },
      login_pw_label: { 'PASSLBL' => 'Password' },
      login_submit: { 'BTN_CONTINUE' => 'Login' }
    },
    'MA_HOME_' => {
      # Autocomplete options: 0: search (default), 1: list
      airport_autocomplete_type: 'AUTOCOMP_TYPE',
      airport_label_a: { 'CAP_AIR_A' => 'From' },
      airport_label_d: { 'CAP_AIR_D' => 'To' },
      clear_group_label: { 'CAP_CLEAR_GROUP' => 'Remove the %GROUPALIAS% code' },
      cruise_header: 'HEAD_CRUISE',
      direction_label_to: { 'TOAIR' => 'To the airport and cruise and stuff' },
      direction_label_from: { 'FROMAIR' => 'From the airport and cruise and stuff' },
      direction_type_header_arriving: { 'HEAD_DIRTYPE_A' => 'Arriving' },
      direction_type_header_departing: { 'HEAD_DIRTYPE_D' => 'Departing' },
      flight_date_label_a: {'CAP_FLIGHT_TIME' => 'Flight date'},
      flight_date_label_c_a: {'CAP_PORT_ARR_TIME' => 'Cruise arrival date'},
      flight_date_label_c_d: {'CAP_PORT_DEP_TIME' => 'Cruise departure date'},
      flight_time_label_a: 'CAP_FLIGHT_TIME2',
      flight_time_label_c_a: 'CAP_PORT_ARR_TIME2',
      flight_time_label_c_d: 'CAP_PORT_DEP_TIME2',
      home_h1: {'H1' => 'Online Reservations'},
      home_h2: {'H2' => 'Airport transportation to and from your departure and destination cities'},
      loc_label_a: {'CAP_LOC_D' => 'To'},
      loc_label_d: {'CAP_LOC_A' => 'From'},
      oneway_header: 'HEAD_OW',
      multileg_header: 'HEAD_MULT',
      profile_home_label: {'CAP_PROF_HOME' => 'Your account'},
      profile_login_label: {'CAP_PROF_LOGIN' => 'Login'},
      profile_logout_label: {'CAP_PROF_LOGOUT' => 'Logout'},
      profile_and_group_logout_label: {
        'CAP_PROF_INDGRPLOGOUT' => 'Logout and remove the %GROUPALIAS% code'
      },
      return_flight_date_label: {'CAP_RET_FLIGHT_TIME' => 'Return flight date'},
      return_flight_time_label: 'CAP_RET_FLIGHT_TIME2',
      roundtrip_header: 'HEAD_RW',
      roundtrip_only_msg: {'MSG_RT_ONLY' => 'Your reservation has been switched to round-trip. Only round-trip reservations can be made to/from %REGION%'},
      service_extras_label: {'CAP_SVC_EXTRAS' => 'Special options'},
      service_extras_info_msg: {
        'MSG_INFO_SVC_EXTRAS' => 'Select a pickup and destination for special options' },
      service_extras_missing_msg: {'MSG_NO_SVC_EXTRAS' => 'No special options are available.'},
      trip_label_cruise: {'TRIP_CRUISE' => 'Cruise'},
      trip_label_oneway: {'TRIP_OW' => 'One-way'},
      trip_label_roundtrip: {'TRIP_RT' => 'Round-trip'},
      trip_label_multi: {'TRIP_MULT' => 'Multi-leg'},
      welcome_back_msg: {'CAP_WELCOME' => 'Welcome back'}
    },
    default_profile_message: 'DEFAULT_PROFILE_NOTES',
    flight_type_label: {'FLIGHTTYPEDESC' => 'Flight type'}
  )

  tokens 'MA_GLOBAL_' => {
        back_link_caption: {'CAP_BACK' => 'Back'},
        cart_label: {'CAP_CART_LINK' => 'Cart'},
        custom_stylesheet: 'CSS',
        general_form_error: {
          'ERR_GEN_FORM' =>
            "There are some problems that need to be fixed in the form below before continuing." },
        ie_compatible_view_msg: {
          'ERR_IE_COMPAT' =>
          "<h4>This site requires that Compatibility View be turned off.</h4>Your Internet Explorer browser is currently set to display this site in Compatibility View, which has disabled much of the functionality including the ability to book a reservation.  Please press <strong>Tools</strong> from the tool bar and select <strong>Compatibility View Settings</strong> to disable it and continue." },
        leg_header_cruise_0: {'LEGHEAD_CRUISE0' => 'Arriving at the airport'},
        leg_header_cruise_1: {'LEGHEAD_CRUISE1' => 'Hotel to cruise terminal'},
        leg_header_cruise_2: {'LEGHEAD_CRUISE2' => 'Cruise ship return'},
        leg_header_cruise_3: {'LEGHEAD_CRUISE3' => 'Hotel back to airport'},
        leg_header_oneway_a: {'LEGHEAD_OWA' => 'Arriving'},
        leg_header_oneway_d: {'LEGHEAD_OWD' => 'Departing'},
        leg_header_roundtrip_a: {'LEGHEAD_RTA' => 'Arriving'},
        leg_header_roundtrip_d: {'LEGHEAD_RTD' => 'Departing'},
        leg_header_multi_a: {'LEGHEAD_MULTIA' => 'Leg'},
        leg_header_multi_d: {'LEGHEAD_MULTID' => 'Leg'},
        leg_name_multi: {'LEGNAME_MULTI' => 'Leg'},
        leg_name_oneway_A: {'LEGNAME_OWA' => 'Arriving'},
        leg_name_oneway_D: {'LEGNAME_OWD' => 'Departing'},
        leg_name_roundtrip_A: {'LEGNAME_RTA' => 'Arriving'},
        leg_name_roundtrip_D: {'LEGNAME_RTD' => 'Departing'},
        leg_name_second_leg: {'LEGNAME_RTSECOND' => 'Return Trip'},
        leg_summary_flighttod_default: {'LEGSUM_FLTTOD_DEF' => 'Flight: '},
        leg_summary_flighttod_cruise_0: {'LEGSUM_FLTTOD_CRUISE0' => 'Flight: '},
        leg_summary_flighttod_cruise_1: {'LEGSUM_FLTTOD_CRUISE1' => 'Requested Drop-off: '},
        leg_summary_flighttod_cruise_2_h: {'LEGSUM_FLTTOD_CRUISE2H' => 'Requested Pickup: '},
        leg_summary_flighttod_cruise_2_a: {'LEGSUM_FLTTOD_CRUISE2A' => 'Flight: '},
        leg_summary_flighttod_cruise_3: {'LEGSUM_FLTTOD_CRUISE3' => 'Flight: '},
        logged_out_msg: {'MSG_LOGOUT' => 'You have been logged out.'},
        google_analytics_id: 'ANALYTICS_ID',
        raw_parent_cart_page: {'PAGE_CART' => 'reservations'},
        parent_root: 'PAGE_ROOT',
        service_group_discount_label: {'MSG_GRP_DISC' =>
            'A discount of %GROUPDISC% has been applied with the %GROUPALIAS% code.'} },
      'MA_HOME_' => {
        airport_placeholder: {'PLACEHOLD_AIRPORT' => 'Airport Code or City'},
        flight_type_placeholder: {'PLACEHOLD_FLIGHTTYPE' => ''},
        loc_placeholder: {'PLACEHOLD_LOC' => 'City, Zip Code, or Place'},
        region_placeholder: {'REGION' => 'Region'} },
      system_error_message: 'ERR_CUSTMSG',
      custom_date_format: 'GEN_DATEFMT'
      # custom_date_format: {'GEN_DATEFMT' => "DD/YYYY/MM" }

  def cookies_test
    session[:cookies_checked] = true

    return_url = params[:return_to] || home_url

    if request.cookies[session_key].blank? && return_url !~ /#{session_key}/
      delimiter = return_url.include?('?') ? '&' : '?'
      return_url += "#{delimiter}#{session_key}=#{session_id}"
    end

    redirect_to return_url
  end

  private

  # For backwards compatibility, accept network_site_id
  def accept_network_site_id
    params[:site_id] = params[:network_site_id] if params[:network_site_id].present?
  end

  def add_to_mailing_list(email, key = nil)
    key = EmailSubscriptions.for(current_site_id).first.Key if key.nil?

    # If there are trips in the cart, pass the provider site id of the first trip.
    unless bookable_trips.empty?
      provider_site_id = bookable_trips[0].legs[0].home_site_id
    end

    logger.info("Adding #{email} to mailing list")
    EmailSubscriptions.add(current_site_id, key, email, request.referer,
        request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip, provider_site_id)
  end

  def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end

  # A parent frame can attach some config (ex. skin) to the session
  def attach_config
    current_session.attached_config = params[:attached_config] if params[:attached_config]
  end

  # If the :code parameter is passed in, login to the group profile.
  def attach_group_profile
    if params[:code].present?
      unless login_group_profile!(params[:code])
        flash.now[:error] = group_login_error
      end
    end
  end

  # Create the base URL to redirect
  def base_referer_url
    request.referer.blank? ? "//#{current_host}" :
        request.referer.sub(/\/[^\/]+?$/, '')
  end

  def bookable_trips
    cart.items_ready_for_booking
  end
  helper_method :bookable_trips

  def cart
    current_session.cart
  end
  helper_method :cart

  def check_for_bad_site
    SiteServer.for(current_site_id) if current_site_id
  rescue HudsonWebService::ServiceError => e
    if e.to_s =~ /Site not found/
      logger.error "Unable to find site for <#{current_site_id}>, displaying error."
      render text: 'Site not found'
    end
  end

  # force_redirect allows the ExpressionEngine plugin to redirect to
  # the reservations page at the ExpressionEngine level but actually
  # display a different page at the rails app level.
  def check_for_force_redirect
    if flash[:force_redirect]
      redirect_to flash[:force_redirect]
    end
  end

  def cookies_required
    return if request.cookies[session_key].present? || session[:cookies_checked]
    session[:init_session] = 'init_session' # Force lazy session to load
    redirect_to cookies_test_url(return_to: request.original_url)
  end

  def cruise_enabled?(current_trip_type = nil)
    if current_trip_type &&
       current_trip_type != :cruise &&
       Network::Config.treat_cruise_as_separate_app?(current_site_id)
      return false
    end

    Network::Config.cruise_enabled?(current_site_id)
  end
  helper_method :cruise_enabled?

  def current_attached_config
    current_session.attached_config
  end
  helper_method :current_attached_config

  def current_group_profile
    current_session.group_profile
  end
  helper_method :current_group_profile

  def current_group_id
    if current_group_profile.present?
      current_group_profile.WebID
    elsif current_user_profile.present?
      current_user_profile.ParentID
    else
      nil
    end
  end
  alias :current_group_alias :current_group_id
  helper_method :current_group_id, :current_group_alias

  def current_host
    host = params[:host] || session[:host] || ''
    host.gsub(/^https?:\/\//, '')
  end
  helper_method :current_host

  def current_network
    NetworkObj.new(current_site_id)
  end
  helper_method :current_network

  def current_user_profile
    current_session.user_profile
  end
  helper_method :current_user_profile

  def current_session
    session[current_site_id] ||= {}
    session[current_site_id][:session] ||= Session.new(current_site_id)
  end
  helper_method :current_session

  def current_theme
    @theme ||= Layout::Colors.new(current_site_id, current_group_id)
  end
  helper_method :current_theme

  def current_trip
    cart.current_item
  end
  helper_method :current_trip

  def current_user
    current_session.user
  end
  helper_method :current_user

  # Needed to ensure the session key isn't passed on the URL if cookies are enabled.
  # (works along with the cookieless_sessions gem)
  def default_url_options
    options ||= super.dup || {}

    # Don't pass session key on URL if cookies are enabled.
    options.delete(session_key) if cookies[session_key].present?

    options
  end

  # We support defaulting reservation fields by passing in parameters
  # (usually via an external plugin). params[:reservation_params] will be set to 1
  # if there are default fields available.
  # Currently only supported for round-trip, one-way, cruise
  # and the first 2 legs of multi-leg.
  def default_reservation_fields
    return unless params[:reservation_params].to_i == 1

    # First ensure we are on the right page.
    desired_action = params[:trip].try(:[], :trip_type)
    if desired_action
      desired_action = 'round_trip' if desired_action == 'roundtrip'
        action_to_test = desired_action == 'cruise' ? 'index' : desired_action
        if action_name != action_to_test && desired_action.in?(%w(round_trip one_way multi_leg cruise))
        redirect_to(params.merge(action: desired_action).to_unsafe_h)
        return
      end
    end

    current_trip.update_from_default_values(params[:trip]) if params[:trip].is_a?(Hash)
      current_trip.not_ready_for_services!
  end

  def home_url
    url = send("#{current_trip.trip_type}_url")
    url.blank? ? oneway_url : url
  end
  helper_method :home_url

  def load_profile(profile_id)
    Profiles.for(current_site_id, profile_id)
  end

  # Returns true if successful, false otherwise.
  # If successful the group profile will be attached to the session
  # and all trips will be cleared.
  def login_group_profile!(group_id)
    # Don't re-login a group profile if it's the same one we already have.
    return true if current_session.group_id == group_id

    profile = load_profile(group_id.upcase)
    return false unless profile

    if profile.coupon?
      current_session.attach_coupon(profile.WebID)
      return true
    elsif profile.is_group?
      cart.empty!
      current_session.attach_group_profile(profile)
      return true
    end

    false
  end

  # Returns true if successful, false otherwise.
  # If successful the user profile will be attached to the session
  # and all trips will be cleared.
  def login_user_profile!(username, password)
    profile = load_profile(username)
    if profile.try(:check_password, password)
      cart.empty!
      current_session.attach_user_profile(profile)
      return true
    end

    false
  end

  def multi_leg_enabled?
    Network::Config.multi_leg_enabled?(current_site_id)
  end
  helper_method :multi_leg_enabled?

  def allow_caching
    headers['Cache-Control'] = 'max-age=900'
  end

  def no_caching # Turn off browser caching
    headers['Cache-Control'] = 'no-store, no-cache, must-revalidate'
    headers['Expires'] = 'Mon, 26 Jul 1997 05:00:00 GMT'
    headers['Pragma'] = 'no-cache'
  end

  def oneway_enabled?
    Network::Config.oneway_enabled?(current_site_id)
  end
  helper_method :oneway_enabled?

  # The relative URL to the parent's cart page.
  def parent_cart_page
    if current_attached_config && current_attached_config.cart_page.present?
      current_attached_config.cart_page
    else
      raw_parent_cart_page
    end
  end
  helper_method :parent_cart_page

  def provider_from_airport(airport)
    Rails.cache.fetch("provider_from_airport:#{site_id}:#{airport}") do
      sites = Network::Airport.active_providers(site_id, airport)
      if !sites || sites.length > 1
        nil
      else
        sites.first.ProviderSiteId
      end
    end
  end

  def redirect_to_home
    if parent_root.present?
      render inline: "<script>window.top.location.href = '#{parent_root}';</script>"
    else
      redirect_to root_url
    end
  end

  def render_404(_e)
    redirect_to_home
  end

  def render_500(e)
    send_exception_report(e)

    flash[:error] = system_error_message
    respond_to do |type|
      type.html { redirect_to error_page_path }
      type.all  { render nothing: true, status: '500 Error' }
    end
  end

  def roundtrip_enabled?
    Network::Config.roundtrip_enabled?(current_site_id)
  end
  helper_method :roundtrip_enabled?

  def send_exception_report(exception)
    Alarm.new(current_site_id, exception, request.env).ring
  end

  def set_time_zone
    Time.zone = current_site.time_zone if current_site_id
  end

  def site_token(site_id, name, default_value = '', options = {})
    value = Tokens.for(site_id, name, options)
    (value.blank? ? default_value : value).html_safe
  end
  helper_method :site_token
end