class ReservationsController < ApplicationController
  before_action :no_caching, only: [:one_way, :round_trip, :multi_leg]
  before_action :check_for_force_redirect
  before_action :attach_group_profile
  before_action :precache_locations, only: [:one_way, :round_trip, :multi_leg]

  def cruise
    redirect_to(params.merge(
        controller: 'cruise_reservations', action: 'index').to_unsafe_h)
  end

  def iframe_test
    render layout: false
  end

  # For the locations dialog: Return enough information to
  # populate the dialog, but don't return locations for tabs that
  # support searching.
  def initial_locations
    leg = current_trip.base_legs[params[:leg].to_i]
    leg.Direction = params[:direction]

    locs = {}
    params[:provider_site].split(',').each do |site_id|
      leg.set_airport_and_primary_site(params[:airport], site_id)
      dialog = LocationDialog.new(current_site_id, leg)
      locs[site_id] = dialog.initial_locations
    end
    render json: locs
  end

  def locations
    # Get location list
    leg = current_trip.base_legs[params[:leg].to_i]
    leg.Direction = params[:direction]
    leg.set_airport_and_primary_site(params[:airport], params[:provider_site])
    loc_list = leg.available_locations(params[:loc_type], minimal_cols: true)

    # Filter with params[:term]
    locs = []
    search = params[:term].downcase.split
    loc_list.each do |loc|
      terms_match = search.all? do |term|
        (loc.Code + loc.Zip + loc.Location).downcase.include?(term)
      end
      next unless terms_match

      # Create an optimized version of the loc object to improve speed.
      stripped_loc = {}
      [:Name, :Code, :State, :City, :FareType].each do |attr|
        stripped_loc[attr] = loc.send(attr)
      end
      locs.push(stripped_loc)
    end
    render json: locs
  end

  def multi_leg
    if !multi_leg_enabled?
      redirect_to cruise_path
      return
    end

    current_session.ensure_current_trip_is_a Trip::MultiLeg
    default_reservation_fields
    current_trip.clear_roundtrip_only_legs!
    current_trip.ensure_legs_are_initialized
  end

  def one_way
    if !oneway_enabled?
      redirect_to roundtrip_path
      return
    end

    current_session.ensure_current_trip_is_a Trip::OneWay
    default_reservation_fields
    current_trip.clear_roundtrip_only_legs!
  end

  def preload_data
    AppPreloader.preload_data(current_site_id)
    render text: 'Done'
  end

  def round_trip
    if !roundtrip_enabled?
      redirect_to multi_path
      return
    end

    current_session.ensure_current_trip_is_a Trip::RoundTrip
    default_reservation_fields

    if params[:forced_roundtrip]
      airport_desc = NetworkAirport.find(current_site_id, params[:trip][:airport]).name
      flash.now[:notice] = roundtrip_only_msg({}, noreplace: true).gsub(/%REGION%/, "<strong>#{airport_desc}</strong>")
    end
  end

  def svc_extras
    primary_site = params[:provider_site].blank? ?
        provider_from_airport(params[:airport]) : params[:provider_site]
    render(json: { error: 'Missing provider site' }) unless primary_site

    render json: {
      extras_html: render_to_string('reservations/_service_extra_fields.html',
          layout: false, locals: {
            site_id: primary_site,
            leg: current_trip.base_legs[params[:leg].to_i],
            leg_index: params[:leg],
            fixed: current_network.fixed_service_extras(primary_site),
            unfixed: current_network.non_fixed_service_extras(primary_site)
          }) }
  end

  def update_legs
    current_trip.leg_count = params[:leg_count] if params[:leg_count]
    current_trip.replace_base_info(params[:trip])

    current_trip.not_ready_for_services!
    if current_trip.valid_base_info?
      current_trip.load_services

      if current_trip.has_leg_errors?
        flash[:error] = current_trip.leg_errors
      else
        current_trip.ready_for_services!
        redirect_to(services_url); return
      end
    else
      flash[:error] = general_form_error
    end
    redirect_to home_url
  end

private

  # In cases where there aren't a lot of locations to display we
  # want to ensure that all available locations are in memcache and
  # ready to be loaded quickly.
  # Don't do if there are more than 10 airports - the load time would be huge!
  def precache_locations
    airports = NetworkAirport.all(current_site_id, current_group_profile)
    return unless current_group_profile.present? && airports.length < 10

    airports.each do |airport|
      airport.service_areas.collect do |region|
        Locations.for(region.provider_site_id,
                      airport_code: airport.code,
                      group_id: current_group_id,
                      service_area: region.service_area_id)
      end
    end
  end
end
