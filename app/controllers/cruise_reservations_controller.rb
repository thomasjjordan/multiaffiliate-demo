class CruiseReservationsController < ApplicationController
  before_action :no_caching, only: [:index, :booking_container]
  before_action :check_for_force_redirect
  before_action :attach_group_profile

  display_tokens 'MA_HOME_CRS_' => {
      cruise_arrival_airport_label: {'CAP_A_AIR' => 'Airport'},
      cruise_arrival_loc_label: {'CAP_A_LOC' => 'Destination'},
      cruise_arrival_hotel_dest_label: {'CAP_A_H_DEST' => 'Destination'},
      cruise_arrival_hotel_loc_label: {'CAP_A_H_LOC' => 'Hotel'},
      cruise_arrival_hotel_date_label: {'CAP_A_H_TIME' => 'Drop-off date'},
      cruise_arrival_hotel_time_label: 'CAP_A_H_TIME2',      cruise_dep_airport_label: {'CAP_D_AIR' => 'Port'},
      cruise_dep_flight_date_label: {'CAP_D_FLTTIME' => 'Flight time'},
      cruise_dep_flight_time_label: 'CAP_D_FLTTIME2',
      cruise_dep_loc_label: {'CAP_D_LOC' => 'Destination'},
      cruise_dep_date_label: {'CAP_D_TIME' => 'Pickup date'},
      cruise_dep_time_label: 'CAP_D_TIME2',
      cruise_disable_leg0_label: {
        'CAP_DISABLE_LEG0' => "I don't need transportation from the airport"},
      cruise_disable_leg1_label: {
        'CAP_DISABLE_LEG1' => "I don't need transportation from my hotel"},
      cruise_disable_leg2_label: {
        'CAP_DISABLE_LEG2' => "I don't need return transportation"},
      cruise_disable_leg3_label: {
        'CAP_DISABLE_LEG3' => "I don't need transportation from my hotel"},
      cruise_region_label: {'CAP_REGION' => 'Region'},
      cruise_sect_head_0: 'SECT_HEAD_0',
      cruise_sect_head_1: 'SECT_HEAD_1',
      cruise_sect_head_2: 'SECT_HEAD_2',
      cruise_sect_head_3: 'SECT_HEAD_3'
    }


  def arriving_hotel_destinations
    render json: presenter.ports
  end

  def arriving_hotel_hotels
    render json: presenter.arriving_non_ports(params[:airport])
  end

  def arriving_locations
    render json: presenter.arriving_destinations(params[:airport])
  end

  def booking_container
    current_trip.cruise_site_id = params[:cruise_site_id]
    @cruise = presenter
    render json: {
      html: render_to_string(partial: 'booking_container')
    }
  end

  def departing_hotel_destinations
    render json: presenter.departing_airports(params[:airport])
  end

  def departing_locations
    render json: presenter.departing_destinations(params[:airport])
  end

  def index
    if !cruise_enabled?
      redirect_to oneway_path
      return
    end

    current_session.ensure_current_trip_is_a Trip::Cruise
    current_trip.cruise_site_id = params[:cruise_site_id] if params[:cruise_site_id]
    @cruise = presenter
    default_reservation_fields
  end

  def update_legs
    current_trip.replace_base_info(params[:trip])

    current_trip.not_ready_for_services!
    if current_trip.valid_base_info?
      current_trip.load_services

      if current_trip.has_leg_errors?
        flash[:error] = current_trip.leg_errors
      else
        current_trip.ready_for_services!
        redirect_to(services_url); return
      end
    else
      flash[:error] = general_form_error
    end
    redirect_to home_url
  end

  private

  def presenter
    CruiseReservationsPresenter.new(current_session, view_context)
  end

end
